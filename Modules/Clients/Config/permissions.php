<?php

return [
    'clients.clientdatas' => [
        'index' => 'clients::clientdatas.list resource',
        'create' => 'clients::clientdatas.create resource',
        'edit' => 'clients::clientdatas.edit resource',
        'destroy' => 'clients::clientdatas.destroy resource',
    ],
    'clients.clientoffers' => [
        'index' => 'clients::clientoffers.list resource',
        'create' => 'clients::clientoffers.create resource',
        'edit' => 'clients::clientoffers.edit resource',
        'destroy' => 'clients::clientoffers.destroy resource',
    ],
// append


];
