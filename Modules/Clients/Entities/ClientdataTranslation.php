<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientdataTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'clients__clientdata_translations';
}
