<?php

namespace Modules\Clients\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Clientoffer extends Model
{
    use Translatable;

    protected $table = 'clients__clientoffers';
    public $translatedAttributes = [];
    protected $fillable = [
        'company_id','appl_type','reg_amount','accrediation','stage1_amt','stage2_amt','stage3_amt','total_amt','designationpr','contact_second','status','user_id'
    ];

public function company()
{
    return $this->belongsTo("Modules\Clients\Entities\Clientdata","company_id");
}

public function user()
{
    return $this->belongsTo("Modules\User\Entities\Sentinel\User","user_id");
}
}