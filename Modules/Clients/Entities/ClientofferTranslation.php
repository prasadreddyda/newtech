<?php

namespace Modules\Clients\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientofferTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'clients__clientoffer_translations';
}
