<?php

namespace Modules\Clients\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Clientdata extends Model
{
    use Translatable;

    protected $table = 'clients__clientdatas';
    public $translatedAttributes = [];
    protected $fillable = [
        'user_id','location','standard_name','client_type','country_name','company_name','contact_primary','designationpr','contact_second','designationsec','telephone','mobile_no','Address','email_id','company_sector','trade_license','vat_no','site_no','branch_no','head_office','employee_no','contact_finance','email_finance','phone_no','comments','status','reports'
    ];

public function user()
{
    return $this->belongsTo("Modules\User\Entities\Sentinel\User","user_id");
}
public function standard()
{
    return $this->belongsTo("Modules\Settings\Entities\Standards","standard_name");
}
public function clienttype()
{
    return $this->belongsTo("Modules\Settings\Entities\ClientType","client_type");
}

public function countryname()
{
    return $this->belongsTo("Modules\Settings\Entities\Countries","country_name");
}

public function locationname()
{
    return $this->belongsTo("Modules\Settings\Entities\Locations","location");
}



}