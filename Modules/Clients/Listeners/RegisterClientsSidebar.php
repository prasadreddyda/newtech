<?php

namespace Modules\Clients\Listeners;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterClientsSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('Clients'), function (Item $item) {
                $item->icon('fa fa-users');
                $item->weight(0);
                $item->authorize(
                    $this->auth->hasAccess('clients.clientdatas.index')
                    // $this->auth->hasAccess('clients.clientoffers.index')
                );
                $item->item(trans('Create Client'), function (Item $item) {
                    $item->icon('fa fa-users');
                    $item->weight(0);
                    $item->append('admin.clients.clientdata.create');
                    $item->route('admin.clients.clientdata.index');
                    $item->authorize(
                        $this->auth->hasAccess('clients.clientdatas.index')
                    );
                });
                $item->item(trans('Create Offer'), function (Item $item) {
                    $item->icon('fa fa-regular fa-flag');
                    $item->weight(0);
                    $item->append('admin.clients.clientoffer.create');
                    $item->route('admin.clients.clientoffer.index');
                    $item->authorize(
                        $this->auth->hasAccess('clients.clientoffers.index')
                    );
                });
// append


            });
        });

        return $menu;
    }
}
