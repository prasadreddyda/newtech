<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/clients'], function (Router $router) {
    $router->bind('clientdata', function ($id) {
        return app('Modules\Clients\Repositories\ClientdataRepository')->find($id);
    });
    $router->get('clientdatas', [
        'as' => 'admin.clients.clientdata.index',
        'uses' => 'ClientdataController@index',
        'middleware' => 'can:clients.clientdatas.index'
    ]);
    $router->get('clientdatas/create', [
        'as' => 'admin.clients.clientdata.create',
        'uses' => 'ClientdataController@create',
        'middleware' => 'can:clients.clientdatas.create'
    ]);
    $router->post('clientdatas', [
        'as' => 'admin.clients.clientdata.store',
        'uses' => 'ClientdataController@store',
        'middleware' => 'can:clients.clientdatas.create'
    ]);
    $router->get('clientdatas/{clientdata}/edit', [
        'as' => 'admin.clients.clientdata.edit',
        'uses' => 'ClientdataController@edit',
        'middleware' => 'can:clients.clientdatas.edit'
    ]);
    $router->put('clientdatas/{clientdata}', [
        'as' => 'admin.clients.clientdata.update',
        'uses' => 'ClientdataController@update',
        'middleware' => 'can:clients.clientdatas.edit'
    ]);
    $router->delete('clientdatas/{clientdata}', [
        'as' => 'admin.clients.clientdata.destroy',
        'uses' => 'ClientdataController@destroy',
        'middleware' => 'can:clients.clientdatas.destroy'
    ]);
    $router->bind('clientoffer', function ($id) {
        return app('Modules\Clients\Repositories\ClientofferRepository')->find($id);
    });
    $router->get('clientoffers', [
        'as' => 'admin.clients.clientoffer.index',
        'uses' => 'ClientofferController@index',
        'middleware' => 'can:clients.clientoffers.index'
    ]);
    $router->get('clientoffers/create', [
        'as' => 'admin.clients.clientoffer.create',
        'uses' => 'ClientofferController@create',
        'middleware' => 'can:clients.clientoffers.create'
    ]);
    $router->post('clientoffers', [
        'as' => 'admin.clients.clientoffer.store',
        'uses' => 'ClientofferController@store',
        'middleware' => 'can:clients.clientoffers.create'
    ]);
    $router->get('clientoffers/{clientoffer}/edit', [
        'as' => 'admin.clients.clientoffer.edit',
        'uses' => 'ClientofferController@edit',
        'middleware' => 'can:clients.clientoffers.edit'
    ]);
    $router->put('clientoffers/{clientoffer}', [
        'as' => 'admin.clients.clientoffer.update',
        'uses' => 'ClientofferController@update',
        'middleware' => 'can:clients.clientoffers.edit'
    ]);
    $router->delete('clientoffers/{clientoffer}', [
        'as' => 'admin.clients.clientoffer.destroy',
        'uses' => 'ClientofferController@destroy',
        'middleware' => 'can:clients.clientoffers.destroy'
    ]);
// append


});
