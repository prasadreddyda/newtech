<?php

namespace Modules\Clients\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Clients\Entities\Clientoffer;
use Modules\Clients\Http\Requests\CreateClientofferRequest;
use Modules\Clients\Http\Requests\UpdateClientofferRequest;
use Modules\Clients\Repositories\ClientofferRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Clients\Repositories\ClientdataRepository;
use Modules\User\Contracts\Authentication;
use PDF;

class ClientofferController extends AdminBaseController
{
    /**
     * @var ClientofferRepository
     */
    private $clientoffer;

    public function __construct(ClientofferRepository $clientoffer,ClientdataRepository $clientdata,Authentication $auth)
    {
        parent::__construct();

        $this->clientoffer = $clientoffer;
        $this->clientdata = $clientdata;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $log = $this->auth->user();
        // print_r($log->roles[0]->slug);
        // die();

        if(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'admin')
        {
           
            $clientoffers = $this->clientoffer->all();
        }

        else
        {
            $clientoffers = $this->clientoffer->getByAttributes(['user_id' => $log->id ]);
        }


       // $clientoffers = $this->clientoffer->all();
        $clientdata = $this->clientdata->all();
        // $clienttype = $this->clienttype->all();
        // $countries = $this->countries->all();


        return view('clients::admin.clientoffers.index', compact('clientoffers','clientdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $log = $this->auth->user();

        if(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'admin')
        {
           
            $clientdata = $this->clientdata->all();
        }

        elseif(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'surveillance')
        {
            $clientdata = $this->clientdata->getByAttributes(['user_id' => $log->id ]);
        }
        else
        {
            $clientdata = $this->clientdata->getByAttributes(['user_id' => $log->id ]);
        }

      
        return view('clients::admin.clientoffers.create',compact('clientdata'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateClientofferRequest $request
     * @return Response
     */
    public function store(CreateClientofferRequest $request)
    {
        if(isset($request->submit) && $request->submit == "generate") {
            $log = $this->auth->user();
            // $data = array('user_id' => $log->id, 'exam_id' => $id, 'status' => 0);
            $request->merge(['user_id' => $log->id]);
            $this->clientoffer->create($request->all());
        
            $data = [
                'title' => 'Welcome to ItSolutionStuff.com',
                'date' => date('m/d/Y')
            ];
              
            $pdf = PDF::loadView('myPDF', $data);
        
            return $pdf->download('qrs_report.pdf');
        }
        
        $log = $this->auth->user();
        // $data = array('user_id' => $log->id, 'exam_id' => $id, 'status' => 0);
        $request->merge(['user_id' => $log->id]);
        $this->clientoffer->create($request->all());

        return redirect()->route('admin.clients.clientoffer.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('clients::clientoffers.title.clientoffers')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Clientoffer $clientoffer
     * @return Response
     */
    public function edit(Clientoffer $clientoffer)
    {
        if(isset($_GET['report']))
        {
            $type = "report";
            
        }
        elseif(isset($_GET['viewdata']))
        {
            $type = "viewdata";
        }
        else
        {
            $type = "edit";
        }
        $clientdata = $this->clientdata->all();

        
        return view('clients::admin.clientoffers.edit', compact('clientoffer','clientdata','type'));
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  Clientoffer $clientoffer
     * @param  UpdateClientofferRequest $request
     * @return Response
     */
    public function update(Clientoffer $clientoffer, UpdateClientofferRequest $request)
    {
        $this->clientoffer->update($clientoffer, $request->all());

        return redirect()->route('admin.clients.clientoffer.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('clients::clientoffers.title.clientoffers')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Clientoffer $clientoffer
     * @return Response
     */
    public function destroy(Clientoffer $clientoffer)
    {
        $this->clientoffer->destroy($clientoffer);

        return redirect()->route('admin.clients.clientoffer.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('clients::clientoffers.title.clientoffers')]));
    }
}
