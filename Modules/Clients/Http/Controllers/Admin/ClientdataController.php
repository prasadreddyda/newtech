<?php

namespace Modules\Clients\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Clients\Entities\Clientdata;
use Modules\Clients\Http\Requests\CreateClientdataRequest;
use Modules\Clients\Http\Requests\UpdateClientdataRequest;
use Modules\Clients\Repositories\ClientdataRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Settings\Repositories\StandardsRepository;
use Modules\Settings\Repositories\ClientTypeRepository;
use Modules\Settings\Repositories\CountriesRepository;
use Modules\Settings\Repositories\LocationsRepository;
use Modules\User\Contracts\Authentication;
use DB;
use Excel;

class ClientdataController extends AdminBaseController
{
    /**
     * @var ClientdataRepository
     */
    private $clientdata;

    public function __construct(ClientdataRepository $clientdata,StandardsRepository $standards,ClientTypeRepository $clienttype,CountriesRepository $countries,Authentication $auth,LocationsRepository $locations)
    {
        parent::__construct();

        $this->clientdata = $clientdata;
        $this->standards = $standards;
        $this->clienttype = $clienttype;
        $this->countries = $countries;
        $this->locations = $locations;
        $this->auth = $auth;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $log = $this->auth->user();
        // print_r($log->roles[0]->slug);
        // die();
        $log = $this->auth->user();
        // print_r($log->roles[0]->slug);
        // die();
      

        
        if(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'admin')
        {
           
            $clientdatas = $this->clientdata->all();
        }

        else
        {
            $clientdatas = $this->clientdata->getByAttributes(['user_id' => $log->id ]);
        }
        // $clientdatas = $this->clientdata->getByAttributes(['user_id' => $log->id ]);
        $standards = $this->standards->all();
        $clienttype = $this->clienttype->all();
        $countries = $this->countries->all();
        $locations = $this->locations->all();
     

        return view('clients::admin.clientdatas.index', compact('clientdatas','standards','clienttype','countries','locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $standards = $this->standards->all();
        $clienttype = $this->clienttype->all();
        $countries = $this->countries->all();
        $locations = $this->locations->all();
        return view('clients::admin.clientdatas.create',compact('standards','clienttype','countries','locations'));


       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateClientdataRequest $request
     * @return Response
     */
    public function store(CreateClientdataRequest $request)
    {

        if(isset($request->ajay) && $request->ajay){

            $file_name = $_FILES['ajay']['name'];
            $file_size =$_FILES['ajay']['size'];
            $file_tmp =$_FILES['ajay']['tmp_name'];
            $file_type=$_FILES['ajay']['type'];
            
            $expensions= array("jpeg","jpg","png");

            $newfile = "assets/Reports/".time().$file_name;

           

            if($file_size > 2097152){
               $errors[]='File size must be excately 2 MB';
            }
            
            if(empty($errors)==true){
               move_uploaded_file($file_tmp,$newfile);
               $request->merge(['reports' => $newfile]);
            }
      }
          $log = $this->auth->user();
        $standard_name = json_encode($request->standard_name);
        $head = 1;
        $request->merge(['user_id' => $log->id, 'standard_name' => $standard_name, 'head_office' => $head]);
     
      
  
       
        $this->clientdata->create($request->all());

        return redirect()->route('admin.clients.clientdata.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('clients::clientdatas.title.clientdatas')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Clientdata $clientdata
     * @return Response
     */
    public function edit(Clientdata $clientdata)
    {
        if(isset($_GET['report']))
        {
            $type = "report";
            
        }
        else
        {
            $type = "edit";
        }

       

        $standards = $this->standards->all();
        $clienttype = $this->clienttype->all();
        $countries = $this->countries->all();
        $locations = $this->locations->all();

        return view('clients::admin.clientdatas.edit', compact('clientdata','standards','clienttype','countries','locations','type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Clientdata $clientdata
     * @param  UpdateClientdataRequest $request
     * @return Response
     */
    public function update(Clientdata $clientdata, UpdateClientdataRequest $request)
    {
        
        // dd($request->ajay);
        if(isset($request->ajay) && $request->ajay){

            $file_name = $_FILES['ajay']['name'];
            $file_size =$_FILES['ajay']['size'];
            $file_tmp =$_FILES['ajay']['tmp_name'];
            $file_type=$_FILES['ajay']['type'];
            
            $expensions= array("jpeg","jpg","png");

            $newfile = "assets/Reports/".time().$file_name;

           

            if($file_size > 2097152){
               $errors[]='File size must be excately 2 MB';
            }
            
            if(empty($errors)==true){
               move_uploaded_file($file_tmp,$newfile);
               $request->merge(['reports' => $newfile]);
            }
      }
        
        $this->clientdata->update($clientdata, $request->all());

     

        return redirect()->route('admin.clients.clientdata.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('clients::clientdatas.title.clientdatas')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Clientdata $clientdata
     * @return Response
     */
    public function destroy(Clientdata $clientdata)
    {
        // dd($clientdata->reports);

        if(file_exists(public_path($clientdata->reports))){
            unlink(public_path($clientdata->reports));
            }else{
                $this->clientdata->destroy($clientdata);
            }
        $this->clientdata->destroy($clientdata);

        return redirect()->route('admin.clients.clientdata.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('clients::clientdatas.title.clientdatas')]));
    }
}
