<?php

namespace Modules\Clients\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;
use Illuminate\Validation\Rule;


class UpdateClientdataRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            
            // 'company_name' => 'required|unique:clients__clientdatas,company_name'.$this->id.'|max:255',
            'company_name' => [
                'required',
                Rule::unique('clients__clientdatas')->ignore($this->id),
            ],

            'trade_license' => [
                'required',
                Rule::unique('clients__clientdatas')->ignore($this->id),
            ],
        ];
       
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
