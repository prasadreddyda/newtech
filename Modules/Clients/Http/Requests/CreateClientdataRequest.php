<?php

namespace Modules\Clients\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateClientdataRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'trade_license' => 'required|unique:clients__clientdatas|max:255',
            'company_name' => 'required|unique:clients__clientdatas|max:255',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
