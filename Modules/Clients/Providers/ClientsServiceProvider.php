<?php

namespace Modules\Clients\Providers;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Clients\Listeners\RegisterClientsSidebar;
use Illuminate\Support\Arr;

class ClientsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterClientsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('clientdatas', Arr::dot(trans('clients::clientdatas')));
            $event->load('clientoffers', Arr::dot(trans('clients::clientoffers')));
            // append translations


        });


    }

    public function boot()
    {
        $this->publishConfig('clients', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Clients\Repositories\ClientdataRepository',
            function () {
                $repository = new \Modules\Clients\Repositories\Eloquent\EloquentClientdataRepository(new \Modules\Clients\Entities\Clientdata());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Clients\Repositories\Cache\CacheClientdataDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Clients\Repositories\ClientofferRepository',
            function () {
                $repository = new \Modules\Clients\Repositories\Eloquent\EloquentClientofferRepository(new \Modules\Clients\Entities\Clientoffer());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Clients\Repositories\Cache\CacheClientofferDecorator($repository);
            }
        );
// add bindings


    }


}
