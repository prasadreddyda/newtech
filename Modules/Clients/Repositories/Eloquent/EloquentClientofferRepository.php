<?php

namespace Modules\Clients\Repositories\Eloquent;

use Modules\Clients\Repositories\ClientofferRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentClientofferRepository extends EloquentBaseRepository implements ClientofferRepository
{
}
