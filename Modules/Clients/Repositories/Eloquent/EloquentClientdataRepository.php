<?php

namespace Modules\Clients\Repositories\Eloquent;

use Modules\Clients\Repositories\ClientdataRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentClientdataRepository extends EloquentBaseRepository implements ClientdataRepository
{
}
