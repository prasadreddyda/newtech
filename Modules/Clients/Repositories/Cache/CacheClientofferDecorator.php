<?php

namespace Modules\Clients\Repositories\Cache;

use Modules\Clients\Repositories\ClientofferRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheClientofferDecorator extends BaseCacheDecorator implements ClientofferRepository
{
    public function __construct(ClientofferRepository $clientoffer)
    {
        parent::__construct();
        $this->entityName = 'clients.clientoffers';
        $this->repository = $clientoffer;
    }
}
