<?php

namespace Modules\Clients\Repositories\Cache;

use Modules\Clients\Repositories\ClientdataRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheClientdataDecorator extends BaseCacheDecorator implements ClientdataRepository
{
    public function __construct(ClientdataRepository $clientdata)
    {
        parent::__construct();
        $this->entityName = 'clients.clientdatas';
        $this->repository = $clientdata;
    }
}
