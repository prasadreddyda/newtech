<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsClientdataTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients__clientdata_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('clientdata_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['clientdata_id', 'locale']);
            $table->foreign('clientdata_id')->references('id')->on('clients__clientdatas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients__clientdata_translations', function (Blueprint $table) {
            $table->dropForeign(['clientdata_id']);
        });
        Schema::dropIfExists('clients__clientdata_translations');
    }
}
