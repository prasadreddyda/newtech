<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsClientofferTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients__clientoffer_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('clientoffer_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['clientoffer_id', 'locale']);
            $table->foreign('clientoffer_id')->references('id')->on('clients__clientoffers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients__clientoffer_translations', function (Blueprint $table) {
            $table->dropForeign(['clientoffer_id']);
        });
        Schema::dropIfExists('clients__clientoffer_translations');
    }
}
