<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsClientdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients__clientdatas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
          
            $table->integer('standard_name')->unsigned();
            $table->integer('client_type')->unsigned();
            $table->integer('country_name')->unsigned();
            $table->integer('location')->unsigned();
            
            // $table->unsignedBigInteger('country_id');
            // $table->unsignedBigInteger('standard_id');
            // $table->unsignedBigInteger('client_type');
            $table->string('company_name');
            
            $table->string('contact_primary');
            $table->string('designationpr');
            $table->string('contact_second');
            $table->string('designationsec');
            $table->integer('telephone');
            $table->integer('mobile_no');
            $table->string('Address');
            $table->string('email_id');
            $table->string('company_sector');
            
            $table->string('trade_license');
            $table->integer('vat_no');
            
            $table->string('site_no');
            $table->string('branch_no');
            $table->string('head_office');
            $table->string('employee_no');
            $table->string('contact_finance');
            $table->string('email_finance');
            $table->integer('phone_no');
            $table->string('comments');
            $table->string('reports');
           

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('location')->references('id')->on('settings__locations')->onDelete('cascade');
            $table->foreign('country_name')->references('id')->on('settings__countries')->onDelete('cascade');
            $table->foreign('standard_name')->references('id')->on('settings__standards')->onDelete('cascade');
            $table->foreign('client_type')->references('id')->on('settings__clienttypes')->onDelete('cascade');
           
            // $table->foreign('country_id')->references('id')->on('settings__countries')->onDelete('cascade');
            // $table->foreign('standard_id')->references('id')->on('settings__standards')->onDelete('cascade');
            // $table->foreign('client_type')->references('id')->on('settings__clienttypes')->onDelete('cascade');
            // $table->foreign('country_id')->references('name')->on('settings__countries')->onDelete('cascade');





            // $table->integer('academic_year');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients__clientdatas');
    }
}
