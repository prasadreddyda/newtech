<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsClientoffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients__clientoffers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            // $table->unsignedBigInteger('company_id');
            // $table->unsignedBigInteger('standard_id');
            // $table->unsignedBigInteger('client_type');
            $table->integer('company_id')->unsigned();
            $table->integer('standard_id')->unsigned();
            $table->integer('client_type')->unsigned();
            $table->string('contact_primary');
            $table->string('designationpr');
            $table->string('contact_second');
            $table->string('designationsec');
            $table->integer('telephone');
            $table->integer('mobile_no');
            $table->string('Address');
            $table->string('email_id');
            $table->string('company_sector');
           
            $table->string('trade_license');
            $table->integer('vat_no');
            $table->integer('stage1_amt');
            $table->integer('stage2_amt');
            $table->integer('stage3_amt');
            $table->integer('total_amt');
            
            $table->string('country');
            $table->string('site_no');
            $table->string('branch_no');
            $table->string('head_office');
            $table->string('employee_no');
            $table->string('contact_finance');
            $table->string('email_finance');
            $table->integer('phone_no');
            $table->string('comments');
            $table->string('accrediation');
            $table->string('appl_type');
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('clients__clientdatas')->onDelete('cascade');
            $table->foreign('standard_id')->references('id')->on('settings__standards')->onDelete('cascade');
            $table->foreign('client_type')->references('id')->on('settings__clienttypes')->onDelete('cascade');

            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients__clientoffers');
    }
}
