@extends('layouts.master')
    <style>
            html {
                user-select: none;
            }
    </style>
@section('content-header')
    <h1>
        {{ trans('clients::clientoffers.title.create clientoffer') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.clients.clientoffer.index') }}">{{ trans('clients::clientoffers.title.clientoffers') }}</a></li>
        <li class="active">{{ trans('clients::clientoffers.title.create clientoffer') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.clients.clientoffer.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('clients::admin.clientoffers.partials.create-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" name="submit" value="save" class="btn btn-warning btn-flat">Save</button>
                        <button type="submit" name="submit" value="generate" class="btn btn-success btn-flat">Save & Generate Offer</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.clients.clientoffer.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')

    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.clients.clientoffer.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
<script type="text/javascript">
$(document).on('keydown', function(e) { 
    if((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
        alert("Printing is prohibited");
        e.cancelBubble = true;
        e.preventDefault();

        e.stopImmediatePropagation();
    }  
});
</script>


<script>
document.addEventListener("contextmenu", function(event){
event.preventDefault();
alert('Access Denied');    
}, false);
</script>

@endpush
