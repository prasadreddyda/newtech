@extends('layouts.master')
<style>
        html {
            user-select: none;
        }
</style>
@section('content-header')
    <h1>
        {{ trans('Client Offer') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('clients::clientoffers.title.clientoffers') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.clients.clientoffer.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('clients::clientoffers.button.create clientoffer') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead  style="background-color: #47915c; color: white;">
                            <tr >
                            <th>Company Name</th>
                            <th>Accrediation</th>
                            <th> Application Type</th>
                            <th> Reg Amount</th>
                            <!-- <th>Stage 1</th>
                            <th>Stage 2</th>
                            <th> Stage 3</th> -->
                            <th>Total </th>
                            
                           <th>Executive Name</th>

                                <!-- <th>Offer </th> -->
                                <th>Details </th>

                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($clientoffers)): ?>
                            <?php foreach ($clientoffers as $clientoffer): ?>
                            <tr  style="font-size:13px;font-weight:bold;background-color:#e1ecd8;">
                                 <td>
                                    {{ $clientoffer->company['company_name'] }}
                                </td>
                                <td>
                            <?php if($clientoffer->accrediation == 1) { ?>
                                        <b>EIAC</b> 
                                    <?php } elseif($clientoffer->accrediation == 2) { ?>
                                        <b>IAS</b>
                                    <?php }elseif($clientoffer->accrediation == 3) { ?>
                                        <b>ASCB[E]</b>
                                    <?php }elseif($clientoffer->accrediation == 4) { ?>
                                        <b>JAS-ANZ</b>
                                    <?php }else{
                                        ?>
                                         <b>GAC</b>
                                        <?php
                                    } ?>

                            </td>
                            <td>
                            <?php if($clientoffer->appl_type == 1) { ?>
                                        <b>Initial</b> 
                                    <?php } elseif($clientoffer->appl_type == 2) { ?>
                                        <b>Re-Certificatin</b>
                                    <?php }else { ?>
                                        <b>Surveillance</b>
                                    <?php } ?>

                            </td>
                            <td> {{ $clientoffer->reg_amount}}</td>
                            <!-- <td> {{ $clientoffer->stage1_amt}}</td>
                            <td> {{ $clientoffer->stage2_amt}}</td>
                            <td> {{ $clientoffer->stage3_amt}}</td> -->
                            <td> {{ $clientoffer->total_amt}}</td>
                            <td>
                                    {{ $clientoffer->user['first_name'] }}
                            </td>
                               
<!--                                
                                <td>
                                    <a href="{{ route('admin.clients.clientoffer.edit', [$clientoffer->id]) }}?report={{ $clientoffer->id}}" class="btn btn-success btn-primary"><i class="fa fa-eye"></i> Generate Offer</a>
                                </td> -->
                                <td>
                                    <a href="{{ route('admin.clients.clientoffer.edit', [$clientoffer->id]) }}?viewdata={{ $clientoffer->id}}" class="btn btn-success btn-primary"><i class="fa fa-eye"></i> View Data</a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.clients.clientoffer.edit', [$clientoffer->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.clients.clientoffer.destroy', [$clientoffer->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <!-- <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr> -->
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('clients::clientoffers.title.create clientoffer') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.clients.clientoffer.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>

<script type="text/javascript">
$(document).on('keydown', function(e) { 
    if((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
        alert("Printing is prohibited");
        e.cancelBubble = true;
        e.preventDefault();

        e.stopImmediatePropagation();
    }  
});
</script>


<script>
document.addEventListener("contextmenu", function(event){
event.preventDefault();
alert('Access Denied');    
}, false);
</script>
@endpush
