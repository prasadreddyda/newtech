@if($type == "report")


<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> APPLICATION FORM / AGREEMENT SHEET
            <small class="pull-right">Date: 2/10/2014</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          {{ $clientoffer->company['designationpr'] }}
          <address>
            <strong>Admin, Inc.</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (804) 123-5432<br>
            Email: info@almasaeedstudio.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>John Doe</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (555) 539-1037<br>
            Email: john.doe@example.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #007612</b><br>
          <br>
          <b>Order ID:</b> 4F3S8J<br>
          <b>Payment Due:</b> 2/22/2014<br>
          <b>Account:</b> 968-34567
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Qty</th>
              <th>Product</th>
              <th>Serial #</th>
              <th>Description</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>1</td>
              <td>Call of Duty</td>
              <td>455-981-221</td>
              <td>El snort testosterone trophy driving gloves handsome</td>
              <td>$64.50</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Need for Speed IV</td>
              <td>247-925-726</td>
              <td>Wes Anderson umami biodiesel</td>
              <td>$50.00</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Monsters DVD</td>
              <td>735-845-642</td>
              <td>Terry Richardson helvetica tousled street art master</td>
              <td>$10.70</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Grown Ups Blue Ray</td>
              <td>422-568-642</td>
              <td>Tousled lomo letterpress</td>
              <td>$25.99</td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <img src="../../dist/img/credit/visa.png" alt="Visa">
          <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
          <img src="../../dist/img/credit/american-express.png" alt="American Express">
          <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Amount Due 2/22/2014</p>

          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td>$250.30</td>
              </tr>
              <tr>
                <th>Tax (9.3%)</th>
                <td>$10.34</td>
              </tr>
              <tr>
                <th>Shipping:</th>
                <td>$5.80</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>$265.24</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>

    @else

    @if($type == "viewdata")

    <div class="box-body">
     <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            
                           
                        <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company_name') ? ' has-error' : '' }}">
                                    {!! Form::label('company_id', trans('Select Company *')) !!}
                                    <select class="form-control" required name="company_id" id="company_id" disabled >
                                        <option value="">--- Select Company ---</option>
                                       
                                        <?php foreach ($clientdata as $value): ?>
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientoffer->company_id) { echo "selected";} ?> >{{ $value->company_name }}</option>
                                        <?php endforeach; ?>

                                    </select>
                                    {!! $errors->first('company_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>                          

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Application type') ? ' has-error' : '' }}">
                                    {!! Form::label('appl_type', trans('Application Type *')) !!}
                                     <select class="form-control" required name="appl_type" disabled>
                                        <option value="">--- Select Application Type ---</option>
                                        <option value="1" <?php if($clientoffer->appl_type == 1) { echo "selected";} ?>>Initial</option>
                                        <option value="2" <?php if($clientoffer->appl_type == 2) { echo "selected";}?>>Re-Certification</option>
                                        <option value="3" <?php if($clientoffer->appl_type == 3) { echo "selected";}?>>Surveillance</option>

                                       
                                        
                                    </select>
                                    {!! $errors->first('appl_type', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Accreditation') ? ' has-error' : '' }}">
                                    {!! Form::label('accrediation', trans('Accreditation Type *')) !!}
                                     <select class="form-control" required name="accrediation" disabled>
                                        <option value="">--- Select Accreditation Type ---</option>
                                        <option value="1" <?php if($clientoffer->accrediation == 1) { echo "selected";} ?>>EIAC</option>
                                        <option value="2" <?php if($clientoffer->accrediation == 2) { echo "selected";}?>>IAS</option>
                                        <option value="3" <?php if($clientoffer->accrediation == 3) { echo "selected";}?>>ASCB[E]</option>
                                        <option value="4" <?php if($clientoffer->accrediation == 4) { echo "selected";}?>>JAS-ANZ</option>
                                        <option value="5" <?php if($clientoffer->accrediation == 5) { echo "selected";}?>>GAC</option>

                                      
                                        
                                    </select>
                                    {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Registration Amount ') ? ' has-error' : '' }}">
                                    {!! Form::label('reg_amount', trans('Registration Amount ')) !!}
                                    {!! Form::text('reg_amount', old('reg_amount',$clientoffer->reg_amount), ['class' => 'form-control','required', 'placeholder' => trans('Registration Amount *'),'readonly']) !!}
                                    {!! $errors->first('reg_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                           

                           
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Stage 1 Price ') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1_amt', trans('Initial Price *')) !!}
                                    {!! Form::text('stage1_amt', old('stage1_amt',$clientoffer->stage1_amt), ['class' => 'form-control','required', 'placeholder' => trans('Stage 1 Price *'),'readonly']) !!}
                                    {!! $errors->first('stage1_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Stage 2 Price') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2_amt', trans('Surveillance-1 Price *')) !!}
                                    {!! Form::text('stage2_amt', old('stage2_amt',$clientoffer->stage2_amt), ['class' => 'form-control','required' , 'placeholder' => trans('Stage 2 Price *'),'readonly']) !!}
                                    {!! $errors->first('stage2_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Stage 3 Price') ? ' has-error' : '' }}">
                                    {!! Form::label('stage3_amt', trans('Surveillance-2 Price *')) !!}
                                    {!! Form::text('stage3_amt', old('stage3_amt',$clientoffer->stage3_amt), ['class' => 'form-control','required', 'placeholder' => trans('Stage 3 Price'),'readonly']) !!}
                                    {!! $errors->first('stage3_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Total Amount') ? ' has-error' : '' }}">
                                    {!! Form::label('total_amt', trans('Total Amount *')) !!}
                                    {!! Form::text('total_amt', old('total_amt',$clientoffer->total_amt), ['class' => 'form-control','required' ,'placeholder' => trans('Total Amount'),'readonly']) !!}
                                    {!! $errors->first('total_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            

                           

                            

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    {!! Form::label('status', trans('Status')) !!}
                                    <br>
                                    {!! Form::label('status', trans('Enable')) !!}
                                    {!! Form::radio('status', 1, ['class' => 'form-control']) !!}
                                    {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    &nbsp;&nbsp;
                                    {!! Form::label('status', trans('disable')) !!}
                                    {!! Form::radio('status', 0, ['class' => 'form-control']) !!}
                                    {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @else

<div class="box-body">
     <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            
                           
                        <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company_name') ? ' has-error' : '' }}">
                                    {!! Form::label('company_id', trans('Select Company *')) !!}
                                    <select class="form-control" required name="company_id" id="company_id"  >
                                        <option value="">--- Select Company ---</option>
                                       
                                        <?php foreach ($clientdata as $value): ?>
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientoffer->company_id) { echo "selected";} ?> >{{ $value->company_name }}</option>
                                        <?php endforeach; ?>

                                    </select>
                                    {!! $errors->first('company_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>                          

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Application type') ? ' has-error' : '' }}">
                                    {!! Form::label('appl_type', trans('Application Type *')) !!}
                                     <select class="form-control" required name="appl_type">
                                        <option value="">--- Select Application Type ---</option>
                                        <option value="1" <?php if($clientoffer->appl_type == 1) { echo "selected";} ?>>Initial</option>
                                        <option value="2" <?php if($clientoffer->appl_type == 2) { echo "selected";}?>>Re-Certification</option>
                                        <option value="3" <?php if($clientoffer->appl_type == 3) { echo "selected";}?>>Surveillance</option>

                                       
                                        
                                    </select>
                                    {!! $errors->first('appl_type', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Accreditation') ? ' has-error' : '' }}">
                                    {!! Form::label('accrediation', trans('Accreditation Type *')) !!}
                                     <select class="form-control" required name="accrediation">
                                        <option value="">--- Select Accreditation Type ---</option>
                                        <option value="1" <?php if($clientoffer->accrediation == 1) { echo "selected";} ?>>EIAC</option>
                                        <option value="2" <?php if($clientoffer->accrediation == 2) { echo "selected";}?>>IAS</option>
                                        <option value="3" <?php if($clientoffer->accrediation == 3) { echo "selected";}?>>ASCB[E]</option>
                                        <option value="4" <?php if($clientoffer->accrediation == 4) { echo "selected";}?>>JAS-ANZ</option>
                                        <option value="5" <?php if($clientoffer->accrediation == 5) { echo "selected";}?>>GAC</option>

                                      
                                        
                                    </select>
                                    {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Registration Amount ') ? ' has-error' : '' }}">
                                    {!! Form::label('reg_amount', trans('Registration Amount ')) !!}
                                    {!! Form::text('reg_amount', old('reg_amount',$clientoffer->reg_amount), ['class' => 'form-control','required', 'placeholder' => trans('Registration Amount *')]) !!}
                                    {!! $errors->first('reg_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                           

                           
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Stage 1 Price ') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1_amt', trans('Initial Price *')) !!}
                                    {!! Form::text('stage1_amt', old('stage1_amt',$clientoffer->stage1_amt), ['class' => 'form-control','required', 'placeholder' => trans('Stage 1 Price *')]) !!}
                                    {!! $errors->first('stage1_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Stage 2 Price') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2_amt', trans('Surveillance-1 Price *')) !!}
                                    {!! Form::text('stage2_amt', old('stage2_amt',$clientoffer->stage2_amt), ['class' => 'form-control','required' , 'placeholder' => trans('Stage 2 Price *')]) !!}
                                    {!! $errors->first('stage2_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Stage 3 Price') ? ' has-error' : '' }}">
                                    {!! Form::label('stage3_amt', trans('Surveillance-2 Price *')) !!}
                                    {!! Form::text('stage3_amt', old('stage3_amt',$clientoffer->stage3_amt), ['class' => 'form-control','required', 'placeholder' => trans('Stage 3 Price')]) !!}
                                    {!! $errors->first('stage3_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Total Amount') ? ' has-error' : '' }}">
                                    {!! Form::label('total_amt', trans('Total Amount *')) !!}
                                    {!! Form::text('total_amt', old('total_amt',$clientoffer->total_amt), ['class' => 'form-control','required' ,'placeholder' => trans('Total Amount')]) !!}
                                    {!! $errors->first('total_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            

                           

                            

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    {!! Form::label('status', trans('Status')) !!}
                                    <br>
                                    {!! Form::label('status', trans('Enable')) !!}
                                    {!! Form::radio('status', 1, ['class' => 'form-control']) !!}
                                    {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    &nbsp;&nbsp;
                                    {!! Form::label('status', trans('disable')) !!}
                                    {!! Form::radio('status', 0, ['class' => 'form-control']) !!}
                                    {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endif
        @endif