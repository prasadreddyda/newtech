<div class="box-body">
     <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            
                           
                          

                           

                           


                           

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company_name') ? ' has-error' : '' }}">
                                    {!! Form::label('company_id', trans('Select Company *')) !!}
                                    <select class="form-control" required name="company_id" id="company_id" >
                                        <option value="">--- Select Company ---</option>
                                        <?php foreach ($clientdata as $value): ?>
                                            <option value="{{ $value->id }}">{{ $value->company_name }}</option>
                                        <?php endforeach; ?>

                                    </select>
                                    {!! $errors->first('company_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('appl_type') ? ' has-error' : '' }}">
                                    {!! Form::label('appl_type', trans('Application Type *')) !!}
                                     <select class="form-control" required name="appl_type">
                                        <option value="">--- Application Type ---</option>
                                        <option value="1">Initial</option>
                                        <option value="2">Re-Certificatin</option>
                                        <option value="3">Surveillance</option>
                                        
                                        
                                    </select>
                                    {!! $errors->first('appl_type', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                           
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('accrediation') ? ' has-error' : '' }}">
                                    {!! Form::label('accrediation', trans('Accreditation Type *')) !!}
                                     <select class="form-control" required name="accrediation">
                                        <option value="">--- Select Accreditation Type ---</option>
                                        <option value="1">EIAC</option>
                                        <option value="2">IAS</option>
                                        <option value="3">ASCB[E]</option>
                                        <option value="4">JAS-ANZ</option>
                                        <option value="5">GAC</option>
                                        
                                    </select>
                                    {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                            
                           
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Registration Amount ') ? ' has-error' : '' }}">
                                    {!! Form::label('reg_amount', trans('Registration Amount ')) !!}
                                    {!! Form::text('reg_amount', old('reg_amount'), ['class' => 'form-control', 'placeholder' => trans('Registration Amount *')]) !!}
                                    {!! $errors->first('reg_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                           
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Stage 1 Price ') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1_amt', trans('Initial Price *')) !!}
                                    {!! Form::text('stage1_amt', old('stage1_amt'), ['class' => 'form-control','required', 'placeholder' => trans('Stage 1 Price *')]) !!}
                                    {!! $errors->first('stage1_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Stage 2 Price') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2_amt', trans('Surveillance-1 Price *')) !!}
                                    {!! Form::text('stage2_amt', old('stage2_amt'), ['class' => 'form-control','required', 'placeholder' => trans('Stage 2 Price *')]) !!}
                                    {!! $errors->first('stage2_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Stage 3 Price') ? ' has-error' : '' }}">
                                    {!! Form::label('stage3_amt', trans('Surveillance-2 Price *')) !!}
                                    {!! Form::text('stage3_amt', old('stage3_amt'), ['class' => 'form-control','required', 'placeholder' => trans('Stage 3 Price')]) !!}
                                    {!! $errors->first('stage3_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Total Amount') ? ' has-error' : '' }}">
                                    {!! Form::label('total_amt', trans('Total Amount *')) !!}
                                    {!! Form::text('total_amt', old('total_amt'), ['class' => 'form-control','required', 'placeholder' => trans('Total Amount')]) !!}
                                    {!! $errors->first('total_amt', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            

                           

                            

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    {!! Form::label('status', trans('Status')) !!}
                                    <br>
                                    {!! Form::label('status', trans('Enable')) !!}
                                    {!! Form::radio('status', 1, ['class' => 'form-control']) !!}
                                    {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    &nbsp;&nbsp;
                                    {!! Form::label('status', trans('disable')) !!}
                                    {!! Form::radio('status', 0, ['class' => 'form-control']) !!}
                                    {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>