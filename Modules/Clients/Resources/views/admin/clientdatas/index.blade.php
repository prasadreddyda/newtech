@extends('layouts.master')
<style>
        html {
            user-select: none;
        }
</style>
@section('content-header')
    <h1>
        {{ trans('clients::clientdatas.title.clientdatas') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('clients::clientdatas.title.clientdatas') }}</li>
    </ol>

    
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.clients.clientdata.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('clients::clientdatas.button.create clientdata') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead  style="background-color: #47915c; color: white;">
                            <tr>
                            <!-- <th>S.No.</th> -->
                            <th>Company</th>
                            <th>Address</th>
                           
                            <th> TradeNo</th>
                            <th>Standard</th> 
                            <th> Type</th>
                            <th>Country</th>
                            <th>Executive Name</th>
                            <th>Location</th>
                            <th> Details</th>    
                            <th>Note</th> 
                          
                          
                              
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if (isset($clientdatas)): ?>
                            <?php $i =1; ?>
                            <?php foreach ($clientdatas as $clientdata): ?>
                            <tr style="font-size:13px;font-weight:bold;background-color:#e1ecd8;">
                             
                           
                            <td> {{ $clientdata->company_name}}</td>
                            <td> {{ $clientdata->Address}}</td>
                            
                            <td> {{ $clientdata->trade_license}}</td>
                            
                                 <td>
                                    @php $standard_names = json_decode($clientdata->standard_name); @endphp
                                    @if(isset($standard_names))
                                        @foreach($standard_names as $standard)
                                            @php $standard = DB::table('settings__standards')->where('id', $standard)->first(); @endphp
                                            {{ $standard->name  }} <br>
                                        @endforeach
                                    @endif
                                    <!-- @php $standards = DB::table('settings__standards')-> -->
                                </td>

                                <td>
                                    {{ $clientdata->clienttype['name'] }}
                                </td>

                                <td>
                                    {{ $clientdata->countryname['name'] }}
                                </td>

                                <td>
                                    {{ $clientdata->user['first_name'] }}
                                </td>

                                
                                <td>
                                    {{ $clientdata->locationname['name'] }}
                                </td>
                               

                              
                                <td>
                                    <a href="{{ route('admin.clients.clientdata.edit', [$clientdata->id]) }}?report={{ $clientdata->id}}" class="btn btn-success btn-primary"><i class="fa fa-eye"></i> View Data</a>
                                </td>
                            
                                <td>
                                    <a href="{{ URL::asset($clientdata->reports) }}" download><i class="fa fa-file"></i> View</a>
                                </td>

                                
                               
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.clients.clientdata.edit', [$clientdata->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.clients.clientdata.destroy', [$clientdata->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                               
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <!-- <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr> -->
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('clients::clientdatas.title.create clientdata') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.clients.clientdata.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>

<script type="text/javascript">
$(document).on('keydown', function(e) { 
    if((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
        alert("Printing is prohibited");
        e.cancelBubble = true;
        e.preventDefault();

        e.stopImmediatePropagation();
    }  
});
</script>

<script>
document.addEventListener("contextmenu", function(event){
event.preventDefault();
alert('Access Denied');    
}, false);
</script>
@endpush

