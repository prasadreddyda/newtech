@if($type == "report")

<div class="box-body">
     <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            
                           
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Company Name') ? ' has-error' : ''  }}">
                                    {!! Form::label('company_name', trans('Company Name')) !!}
                                    {!! Form::text('company_name', old('company_name',$clientdata->company_name), ['class' => 'form-control','placeholder' => trans('company name *'),'readonly']) !!}
                                    {!! $errors->first('company_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Contact Name Primary') ? ' has-error' : '' }}">
                                    {!! Form::label('contact_primary', trans('Contact Name Primary')) !!}
                                    {!! Form::text('contact_primary', old('contact_primary',$clientdata->contact_primary), ['class' => 'form-control', 'placeholder' => trans('contact name *'),'readonly']) !!}
                                    {!! $errors->first('contact_primary', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Designation') ? ' has-error' : '' }}">
                                    {!! Form::label('designationpr', trans('Designation *')) !!}
                                    {!! Form::text('designationpr', old('designationpr',$clientdata->designationpr), ['class' => 'form-control', 'placeholder' => trans('designation'),'readonly']) !!}
                                    {!! $errors->first('designationpr', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Contact Name Secondary') ? ' has-error' : '' }}">
                                    {!! Form::label('contact_second', trans('Contact Name Secondary *')) !!}
                                    {!! Form::text('contact_second', old('contact_second',$clientdata->contact_second), ['class' => 'form-control', 'placeholder' => trans('contact name'),'readonly']) !!}
                                    {!! $errors->first('contact_second', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Designation') ? ' has-error' : '' }}">
                                    {!! Form::label('designationsec', trans('Designation *')) !!}
                                    {!! Form::text('designationsec', old('designationsec',$clientdata->designationsec), ['class' => 'form-control', 'placeholder' => trans('designation'),'readonly']) !!}
                                    {!! $errors->first('designationsec', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Telephone No') ? ' has-error' : '' }}">
                                    {!! Form::label('telephone', trans('Telephone No *')) !!}
                                    {!! Form::text('telephone', old('telephone',$clientdata->telephone), ['class' => 'form-control', 'placeholder' => trans('telephone'),'readonly']) !!}
                                    {!! $errors->first('telephone', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Mobile No') ? ' has-error' : '' }}">
                                    {!! Form::label('mobile_no', trans('Mobile No *')) !!}
                                    {!! Form::text('mobile_no', old('mobile_no',$clientdata->mobile_no), ['class' => 'form-control', 'placeholder' => trans('mobile'),'readonly']) !!}
                                    {!! $errors->first('mobile_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Address') ? ' has-error' : '' }}">
                                    {!! Form::label('Address', trans('Address *')) !!}
                                    {!! Form::text('Address', old('Address',$clientdata->Address), ['class' => 'form-control', 'placeholder' => trans('address'),'readonly']) !!}
                                    {!! $errors->first('Address', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Email') ? ' has-error' : '' }}">
                                    {!! Form::label('email_id', trans('Email Id *')) !!}
                                    {!! Form::text('email_id', old('email_id',$clientdata->email_id), ['class' => 'form-control', 'placeholder' => trans('email'),'readonly']) !!}
                                    {!! $errors->first('email_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Company Sector') ? ' has-error' : '' }}">
                                    {!! Form::label('company_sector', trans('Company Sector *')) !!}
                                    {!! Form::text('company_sector', old('company_sector',$clientdata->company_sector), ['class' => 'form-control', 'placeholder' => trans('company sector'),'readonly']) !!}
                                    {!! $errors->first('company_sector', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Trade License No') ? ' has-error' : '' }}">
                                    {!! Form::label('trade_license', trans('Trade License No *')) !!}
                                    {!! Form::text('trade_license', old('trade_license',$clientdata->trade_license), ['class' => 'form-control','required','placeholder' => trans('trade license'),'readonly']) !!}
                                    {!! $errors->first('trade_license', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Vat No') ? ' has-error' : '' }}">
                                    {!! Form::label('vat_no', trans('vat no ')) !!}
                                    {!! Form::text('vat_no', old('vat_no',$clientdata->vat_no), ['class' => 'form-control', 'placeholder' => trans('vat no'),'readonly']) !!}
                                    {!! $errors->first('vat_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            



                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('standard_name') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name', trans('Select Standard *')) !!}
                                    <select  name="standard_name[]" class=" form-control " required id="standard_name" multiple disabled>
                                        <option value="">--- Select standard ---</option>
                                       
                                        <?php foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientdata->standard_name) { echo "selected";} ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('client_type') ? ' has-error' : '' }}">
                                    {!! Form::label('client_type', trans('Select Client Type *')) !!}
                                    <select class="form-control" name="client_type" id="client_type" disabled>
                                        <option value="">--- Select Client Type ---</option>
                                       
                                        <?php foreach ($clienttype as $value): ?>
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientdata->client_type) { echo "selected";} ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('country_name') ? ' has-error' : '' }}">
                                    {!! Form::label('country_name', trans('Select Country *')) !!}
                                    <select class="form-control" name="country_name" id="country_name" disabled >
                                        <option value="">--- Select Country ---</option>
                                       
                                        <?php foreach ($countries as $value): ?>
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientdata->country_name) { echo "selected";} ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('country_name') ? ' has-error' : '' }}">
                                    {!! Form::label('location', trans('Select Location *')) !!}
                                    <select class="form-control" name="location" id="location" disabled>
                                        <option value="">--- Select Location ---</option>
                                        <?php foreach ($locations as $value): ?>
                                            
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientdata->location) { echo "selected";} ?> >{{ $value->name }}</option>

                                        <?php endforeach; ?>

                                    </select>
                                    {!! $errors->first('location', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                          
                            <!-- <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('No of Head Office') ? ' has-error' : '' }}">
                                    {!! Form::label('head_office', trans('No of HeadOffice *')) !!}
                                    {!! Form::text('head_office', old('head_office',$clientdata->head_office), ['class' => 'form-control', 'placeholder' => trans('headoffice no')]) !!}
                                    {!! $errors->first('head_office', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div> -->

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('No of Branches') ? ' has-error' : '' }}">
                                    {!! Form::label('branch_no', trans('No of Branches *')) !!}
                                    {!! Form::text('branch_no', old('branch_no',$clientdata->branch_no), ['class' => 'form-control', 'placeholder' => trans('branches no'),'readonly']) !!}
                                    {!! $errors->first('branch_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('No of Sites') ? ' has-error' : '' }}">
                                    {!! Form::label('site_no', trans('No of Sites *')) !!}
                                    {!! Form::text('site_no', old('site_no',$clientdata->site_no), ['class' => 'form-control', 'placeholder' => trans('sites no'),'readonly']) !!}
                                    {!! $errors->first('site_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>



                           

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('No of Employees') ? ' has-error' : '' }}">
                                    {!! Form::label('employee_no', trans('No of Employess *')) !!}
                                    {!! Form::text('employee_no', old('employee_no',$clientdata->employee_no), ['class' => 'form-control', 'placeholder' => trans('employees no'),'readonly']) !!}
                                    {!! $errors->first('employee_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Contact Name Finance') ? ' has-error' : '' }}">
                                    {!! Form::label('contact_finance', trans('Contact Name Finance *')) !!}
                                    {!! Form::text('contact_finance', old('contact_finance',$clientdata->contact_finance), ['class' => 'form-control', 'placeholder' => trans('contact finance'),'readonly']) !!}
                                    {!! $errors->first('contact_finance', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('Comments') ? ' has-error' : '' }}">
                                    {!! Form::label('comments', trans(' Comments *')) !!}
                                    {!! Form::textarea('comments', old('comments',$clientdata->comments), ['class' => 'form-control', 'placeholder' => trans('comments'),'readonly']) !!}
                                    {!! $errors->first('comments', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                           

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Email ID') ? ' has-error' : '' }}">
                                    {!! Form::label('email_finance', trans('Email Id *')) !!}
                                    {!! Form::text('email_finance', old('email_finance',$clientdata->email_finance), ['class' => 'form-control', 'placeholder' => trans('email id '),'readonly']) !!}
                                    {!! $errors->first('email_finance', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Phone No Finance') ? ' has-error' : '' }}">
                                    {!! Form::label('phone_no', trans('Phone No Finance *')) !!}
                                    {!! Form::text('phone_no', old('phone_no',$clientdata->phone_no), ['class' => 'form-control', 'placeholder' => trans('phone no'),'readonly']) !!}
                                    {!! $errors->first('phone_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <!-- <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('User Id') ? ' has-error' : '' }}">
                                    {!! Form::label('user_id', trans('User Id *')) !!}
                                    {!! Form::text('user_id', old('user_id',$clientdata->user_id), ['class' => 'form-control', 'placeholder' => trans('user id ')]) !!}
                                    {!! $errors->first('user_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div> -->
   


                          
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
 

    @else
    
<div class="box-body">
     <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            

                           
                           
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Company Name') ? ' has-error' : '' }}">
                                    {!! Form::label('company_name', trans('Company Name *')) !!}
                                    {!! Form::text('company_name', old('company_name',$clientdata->company_name), ['class' => 'form-control','required', 'placeholder' => trans('company name *')]) !!}
                                    <!-- {!! $errors->first('company_name', '<span class="help-block">:message</span>') !!} -->
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Contact Name Primary') ? ' has-error' : '' }}">
                                    {!! Form::label('contact_primary', trans('Contact Name Primary *')) !!}
                                    {!! Form::text('contact_primary', old('contact_primary',$clientdata->contact_primary), ['class' => 'form-control','required', 'placeholder' => trans('contact name *')]) !!}
                                    {!! $errors->first('contact_primary', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Designation') ? ' has-error' : '' }}">
                                    {!! Form::label('designationpr', trans('Designation *')) !!}
                                    {!! Form::text('designationpr', old('designationpr',$clientdata->designationpr), ['class' => 'form-control','required', 'placeholder' => trans('designation')]) !!}
                                    {!! $errors->first('designationpr', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Contact Name Secondary') ? ' has-error' : '' }}">
                                    {!! Form::label('contact_second', trans('Contact Name Secondary ')) !!}
                                    {!! Form::text('contact_second', old('contact_second',$clientdata->contact_second), ['class' => 'form-control', 'placeholder' => trans('contact name')]) !!}
                                    {!! $errors->first('contact_second', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Designation') ? ' has-error' : '' }}">
                                    {!! Form::label('designationsec', trans('Designation ')) !!}
                                    {!! Form::text('designationsec', old('designationsec',$clientdata->designationsec), ['class' => 'form-control', 'placeholder' => trans('designation')]) !!}
                                    {!! $errors->first('designationsec', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Telephone No') ? ' has-error' : '' }}">
                                    {!! Form::label('telephone', trans('Telephone No *')) !!}
                                    {!! Form::text('telephone', old('telephone',$clientdata->telephone), ['class' => 'form-control','required', 'placeholder' => trans('telephone')]) !!}
                                    {!! $errors->first('telephone', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Mobile No') ? ' has-error' : '' }}">
                                    {!! Form::label('mobile_no', trans('Mobile No *')) !!}
                                    {!! Form::text('mobile_no', old('mobile_no',$clientdata->mobile_no), ['class' => 'form-control','required', 'placeholder' => trans('mobile')]) !!}
                                    {!! $errors->first('mobile_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Address') ? ' has-error' : '' }}">
                                    {!! Form::label('Address', trans('Address ')) !!}
                                    {!! Form::text('Address', old('Address',$clientdata->Address), ['class' => 'form-control', 'placeholder' => trans('address')]) !!}
                                    {!! $errors->first('Address', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Email') ? ' has-error' : '' }}">
                                    {!! Form::label('email_id', trans('Email Id ')) !!}
                                    {!! Form::text('email_id', old('email_id',$clientdata->email_id), ['class' => 'form-control', 'placeholder' => trans('email')]) !!}
                                    {!! $errors->first('email_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Company Sector') ? ' has-error' : '' }}">
                                    {!! Form::label('company_sector', trans('Company Sector *')) !!}
                                    {!! Form::text('company_sector', old('company_sector',$clientdata->company_sector), ['class' => 'form-control','required', 'placeholder' => trans('company sector')]) !!}
                                    {!! $errors->first('company_sector', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Trade License No') ? ' has-error' : '' }}">
                                    {!! Form::label('trade_license', trans('Trade License No *')) !!}
                                    {!! Form::text('trade_license', old('trade_license',$clientdata->trade_license), ['class' => 'form-control','required', 'placeholder' => trans('trade license')]) !!}
                                    <!-- {!! $errors->first('trade_license', '<span class="help-block">:message</span>') !!} -->
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Vat No') ? ' has-error' : '' }}">
                                    {!! Form::label('vat_no', trans('vat no ')) !!}
                                    {!! Form::text('vat_no', old('vat_no',$clientdata->vat_no), ['class' => 'form-control', 'placeholder' => trans('vat no')]) !!}
                                    {!! $errors->first('vat_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            



                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('standard_name') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name', trans('Select Standard *')) !!}
                                    <select  name="standard_name[]" class=" form-control " required id="standard_name" multiple>
                                        <option value="">--- Select standard ---</option>
                                     
                                        <?php foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientdata->standard_name) { echo "selected";} ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('client_type') ? ' has-error' : '' }}">
                                    {!! Form::label('client_type', trans('Select Client Type *')) !!}
                                    <select class="form-control" required name="client_type" id="client_type" >
                                        <option value="">--- Select Client Type ---</option>
                                       
                                        <?php foreach ($clienttype as $value): ?>
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientdata->client_type) { echo "selected";} ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('country_name') ? ' has-error' : '' }}">
                                    {!! Form::label('country_name', trans('Select Country *')) !!}
                                    <select class="form-control" required name="country_name" id="country_name" >
                                        <option value="">--- Select Country ---</option>
                                       
                                        <?php foreach ($countries as $value): ?>
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientdata->country_name) { echo "selected";} ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('country_name') ? ' has-error' : '' }}">
                                    {!! Form::label('location', trans('Select Location *')) !!}
                                    <select class="form-control" required name="location" id="location" >
                                        <option value="">--- Select Location ---</option>
                                        <?php foreach ($locations as $value): ?>
                                            
                                            
                                            <option value="{{ $value->id }}" <?php if($value->id == $clientdata->location) { echo "selected";} ?> >{{ $value->name }}</option>

                                        <?php endforeach; ?>

                                    </select>
                                    {!! $errors->first('location', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                          
                            <!-- <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('No of Head Office') ? ' has-error' : '' }}">
                                    {!! Form::label('head_office', trans('No of HeadOffice *')) !!}
                                    {!! Form::text('head_office', old('head_office',$clientdata->head_office), ['class' => 'form-control', 'placeholder' => trans('headoffice no')]) !!}
                                    {!! $errors->first('head_office', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div> -->

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('No of Branches') ? ' has-error' : '' }}">
                                    {!! Form::label('branch_no', trans('No of Branches ')) !!}
                                    {!! Form::text('branch_no', old('branch_no',$clientdata->branch_no), ['class' => 'form-control', 'placeholder' => trans('branches no')]) !!}
                                    {!! $errors->first('branch_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('No of Sites') ? ' has-error' : '' }}">
                                    {!! Form::label('site_no', trans('No of Sites ')) !!}
                                    {!! Form::text('site_no', old('site_no',$clientdata->site_no), ['class' => 'form-control', 'placeholder' => trans('sites no')]) !!}
                                    {!! $errors->first('site_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>



                           

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('No of Employees') ? ' has-error' : '' }}">
                                    {!! Form::label('employee_no', trans('No of Employess ')) !!}
                                    {!! Form::text('employee_no', old('employee_no',$clientdata->employee_no), ['class' => 'form-control', 'placeholder' => trans('employees no')]) !!}
                                    {!! $errors->first('employee_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Contact Name Finance') ? ' has-error' : '' }}">
                                    {!! Form::label('contact_finance', trans('Contact Name Finance ')) !!}
                                    {!! Form::text('contact_finance', old('contact_finance',$clientdata->contact_finance), ['class' => 'form-control', 'placeholder' => trans('contact finance')]) !!}
                                    {!! $errors->first('contact_finance', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('Comments') ? ' has-error' : '' }}">
                                    {!! Form::label('comments', trans(' Comments ')) !!}
                                    {!! Form::textarea('comments', old('comments',$clientdata->comments), ['class' => 'form-control', 'placeholder' => trans('comments')]) !!}
                                    {!! $errors->first('comments', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                           

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Email ID') ? ' has-error' : '' }}">
                                    {!! Form::label('email_finance', trans('Email Id ')) !!}
                                    {!! Form::text('email_finance', old('email_finance',$clientdata->email_finance), ['class' => 'form-control', 'placeholder' => trans('email id ')]) !!}
                                    {!! $errors->first('email_finance', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Phone No Finance') ? ' has-error' : '' }}">
                                    {!! Form::label('phone_no', trans('Phone No Finance ')) !!}
                                    {!! Form::text('phone_no', old('phone_no',$clientdata->phone_no), ['class' => 'form-control', 'placeholder' => trans('phone no')]) !!}
                                    {!! $errors->first('phone_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>



                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Id') ? ' has-error' : '' }} " hidden >
                                    {!! Form::label('id', trans('Company Name *')) !!}
                                    {!! Form::text('id', old('id',$clientdata->id), ['class' => 'form-control','required', 'placeholder' => trans('company name *')]) !!}
                                    {!! $errors->first('id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('ajay') ? ' has-error' : '' }}">
                                    {!! Form::label('ajay', trans('Upload Report')) !!}
                                    {!! Form::file('ajay', old('ajay'), ['class' => 'form-control', 'placeholder' => trans('Upload Report')]) !!}
                                    {!! $errors->first('ajay', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        @endif