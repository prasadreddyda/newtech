<?php

return [
    'list resource' => 'List clientdatas',
    'create resource' => 'Create clientdatas',
    'edit resource' => 'Edit clientdatas',
    'destroy resource' => 'Destroy clientdatas',
    'title' => [
        'clientdatas' => 'Clientdata',
        'create clientdata' => 'Create a clientdata',
        'edit clientdata' => 'Edit a clientdata',
    ],
    'button' => [
        'create clientdata' => 'Create a clientdata',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
