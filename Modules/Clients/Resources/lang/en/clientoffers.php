<?php

return [
    'list resource' => 'List clientoffers',
    'create resource' => 'Create clientoffers',
    'edit resource' => 'Edit clientoffers',
    'destroy resource' => 'Destroy clientoffers',
    'title' => [
        'clientoffers' => 'Clientoffer',
        'create clientoffer' => 'Create a clientoffer',
        'edit clientoffer' => 'Edit a clientoffer',
    ],
    'button' => [
        'create clientoffer' => 'Create a clientoffer',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
