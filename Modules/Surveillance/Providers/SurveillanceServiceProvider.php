<?php

namespace Modules\Surveillance\Providers;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Surveillance\Listeners\RegisterSurveillanceSidebar;
use Illuminate\Support\Arr;

class SurveillanceServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterSurveillanceSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('surdatas', Arr::dot(trans('surveillance::surdatas')));
            // append translations

        });


    }

    public function boot()
    {
        $this->publishConfig('surveillance', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Surveillance\Repositories\SurdataRepository',
            function () {
                $repository = new \Modules\Surveillance\Repositories\Eloquent\EloquentSurdataRepository(new \Modules\Surveillance\Entities\Surdata());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Surveillance\Repositories\Cache\CacheSurdataDecorator($repository);
            }
        );
// add bindings

    }


}
