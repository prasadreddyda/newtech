<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveillanceSurdataTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveillance__surdata_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('surdata_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['surdata_id', 'locale']);
            $table->foreign('surdata_id')->references('id')->on('surveillance__surdatas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveillance__surdata_translations', function (Blueprint $table) {
            $table->dropForeign(['surdata_id']);
        });
        Schema::dropIfExists('surveillance__surdata_translations');
    }
}
