<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveillanceSurdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveillance__surdatas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            // $table->integer('standard_name')->unsigned();
             
            $table->string('comp_name');
            $table->string('comp_address');
            $table->string('certificate');
            $table->integer('user_id')->unsigned();
            $table->string('accrediation');
            $table->string('validity_from');
            $table->string('validity_to');
            $table->string('total_amount');
            $table->string('amount_collected');
            $table->string('sur1_amount');
            $table->string('sur2_amount');
            $table->string('aud_name');
            $table->string('stage1_date');
            $table->string('stage2_date');

            $table->string('year1');
            $table->string('auditor1');
            $table->string('year2');
            $table->string('auditor2');
            $table->string('year3');
            $table->string('auditor3');
            $table->string('year4');
            $table->string('auditor4');
            $table->tinyint('status');
           
            $table->string('standard_name');
            $table->string('comments');


            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('standard_name')->references('id')->on('settings__standards')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveillance__surdatas');
    }
}
