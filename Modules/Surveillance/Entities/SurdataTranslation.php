<?php

namespace Modules\Surveillance\Entities;

use Illuminate\Database\Eloquent\Model;

class SurdataTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'surveillance__surdata_translations';
}
