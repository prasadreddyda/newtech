<?php

namespace Modules\Surveillance\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Surdata extends Model
{
    use Translatable;

    protected $table = 'surveillance__surdatas';
    public $translatedAttributes = [];
    protected $fillable = [
        'standard_name','comp_name','comp_address','certificate','user_id','accrediation','validity_from','validity_to','total_amount','amount_collected','sur1_amount','sur2_amount','aud_name','stage1_date','stage2_date','comments','year1','year2','year3','year4','auditor1','auditor2','auditor3','auditor4','status'
    ];
    public function standard()
{
    return $this->belongsTo("Modules\Settings\Entities\Standards","standard_name");
}
public function user()
{
    return $this->belongsTo("Modules\User\Entities\Sentinel\User","user_id");
}
}
