<?php

return [
    'list resource' => 'List surdatas',
    'create resource' => 'Create surdatas',
    'edit resource' => 'Edit surdatas',
    'destroy resource' => 'Destroy surdatas',
    'title' => [
        'surdatas' => 'Surdata',
        'create surdata' => 'Create a surdata',
        'edit surdata' => 'Edit a surdata',
    ],
    'button' => [
        'create surdata' => 'Create a surdata',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
