@if($type == "viewdata")

<div class="box-body">
     <div class="tab-content">
         <!-- <input type="button" value="Add Company Details+" class="btn btn-primary " onClick="showHideDiv('divMsg')"/> -->

                <div class="tab-pane active" id="divMsg" >
                    <div class="box-body">
                        <div class="row">



                        <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Accreditation') ? ' has-error' : '' }}">
                                    {!! Form::label('accrediation', trans('Accreditation Type *')) !!}
                                     <select class="form-control"  name="accrediation" disabled>
                                        <option value="">--- Select Accreditation Type ---</option>
                                        <option value="1" <?php if($surdata->accrediation == 1) { echo "selected";} ?>>EIAC</option>
                                        <option value="2" <?php if($surdata->accrediation == 2) { echo "selected";}?>>IAS</option>
                                        <option value="3" <?php if($surdata->accrediation == 3) { echo "selected";}?>>ASCB[E]</option>
                                        <option value="4" <?php if($surdata->accrediation == 4) { echo "selected";}?>>JAS-ANZ</option>
                                        <option value="5" <?php if($surdata->accrediation == 5) { echo "selected";}?>>GAC</option>

                                      
                                        
                                    </select>
                                    {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Name*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_name', trans('Company Name *')) !!}
                                    {!! Form::text('comp_name', old('comp_name',$surdata->comp_name), ['class' => 'form-control','required', 'placeholder' => trans('Company Name'),'readonly']) !!}
                                    {!! $errors->first('comp_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Address*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_address', trans('Company Address')) !!}
                                    {!! Form::text('comp_address', old('comp_address',$surdata->comp_address), ['class' => 'form-control','required', 'placeholder' => trans('Address'),'readonly']) !!}
                                    {!! $errors->first('comp_address', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Certificate*') ? ' has-error' : '' }}">
                                    {!! Form::label('certificate', trans('Certificate Number')) !!}
                                    {!! Form::text('certificate', old('certificate',$surdata->certificate), ['class' => 'form-control','required', 'placeholder' => trans('Certificate'),'readonly']) !!}
                                    {!! $errors->first('certificate', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name', trans('Select Standard *')) !!}
                                    <select  name="standard_name[]" class=" form-control " disabled required id="standard_name" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datas =  json_decode($surdata->standard_name); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_from', trans('Validity From *')) !!}
                                    {!! Form::date('validity_from', old('validity_from',$surdata->validity_from), ['class' => 'form-control','required', 'placeholder' => trans('Validity from'),'readonly']) !!}
                                    {!! $errors->first('validity_from', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity to*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_to', trans('Validity to *')) !!}
                                    {!! Form::date('validity_to', old('validity_to',$surdata->validity_to), ['class' => 'form-control','required','placeholder' => trans('Validity to'),'readonly']) !!}
                                    {!! $errors->first('validity_to', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Total Amount') ? ' has-error' : '' }}">
                                    {!! Form::label('total_amount', trans('Total Amount *')) !!}
                                    {!! Form::text('total_amount', old('total_amount',$surdata->total_amount), ['class' => 'form-control','required', 'placeholder' => trans('Total Amount'),'readonly']) !!}
                                    {!! $errors->first('total_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Amount Collected*') ? ' has-error' : '' }}">
                                    {!! Form::label('amount_collected', trans('Amount Collected *')) !!}
                                    {!! Form::text('amount_collected', old('amount_collected',$surdata->amount_collected), ['class' => 'form-control','required', 'placeholder' => trans('Amount Collected'),'readonly']) !!}
                                    {!! $errors->first('amount_collected', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur1 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur1_amount', trans('Sur1 Amount *')) !!}
                                    {!! Form::text('sur1_amount', old('sur1_amount',$surdata->sur1_amount), ['class' => 'form-control','required', 'placeholder' => trans('Sur1 Amount'),'readonly']) !!}
                                    {!! $errors->first('sur1_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur2 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur2_amount', trans('Sur2 Amount *')) !!}
                                    {!! Form::text('sur2_amount', old('sur2_amount',$surdata->sur2_amount), ['class' => 'form-control','required', 'placeholder' => trans('Sur2 Amount'),'readonly']) !!}
                                    {!! $errors->first('sur2_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('aud_name', trans('Auditor Name')) !!}
                                    {!! Form::text('aud_name', old('aud_name',$surdata->aud_name), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('aud_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1_date', trans('Stage1 Date')) !!}
                                    {!! Form::date('stage1_date', old('stage1_date',$surdata->stage1_date), ['class' => 'form-control', 'placeholder' => trans('Stage1 Date'),'readonly']) !!}
                                    {!! $errors->first('stage1_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2_date', trans('Stage2 Date')) !!}
                                    {!! Form::date('stage2_date', old('stage2_date',$surdata->stage2_date), ['class' => 'form-control', 'placeholder' => trans('Stage2 Date'),'readonly']) !!}
                                    {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year1', trans('Choose Year')) !!}
                                        <select class="form-control" name="year1" disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($surdata->year1 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($surdata->year1 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($surdata->year1 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($surdata->year1 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($surdata->year1 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($surdata->year1 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($surdata->year1 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($surdata->year1 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor1') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor1', trans('Select Auditor ')) !!}
                                    <select  name="auditor1[]" class=" form-control " id="auditor1" disabled multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                        
                                        <?php $datas =  json_decode($surdata->auditor1); foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } }?> >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>
                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year1', trans('Choose Year')) !!}
                                        <select class="form-control" name="year1" disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($surdata->year2 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($surdata->year2 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($surdata->year2 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($surdata->year2 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($surdata->year2 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($surdata->year2 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($surdata->year2 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($surdata->year2 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor2') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor2', trans('Select Auditor ')) !!}
                                    <select  name="auditor2[]" class=" form-control " id="auditor2" disabled multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                        
                                        <?php $datas =  json_decode($surdata->auditor2); foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } }?> >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>
                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            </div>



                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year3', trans('Choose Year')) !!}
                                        <select class="form-control" name="year3" disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($surdata->year3 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($surdata->year3 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($surdata->year3 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($surdata->year3 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($surdata->year3 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($surdata->year3 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($surdata->year3 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($surdata->year3 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor3') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor3', trans('Select Auditor ')) !!}
                                    <select  name="auditor3[]" class=" form-control "  id="auditor3" disabled multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                        
                                        <?php $datas =  json_decode($surdata->auditor3); foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } }?> >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>
                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year4', trans('Choose Year')) !!}
                                        <select class="form-control" name="year4" disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($surdata->year4 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($surdata->year4 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($surdata->year4 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($surdata->year4 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($surdata->year4 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($surdata->year4 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($surdata->year4 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($surdata->year4 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor4') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor4', trans('Select Auditor ')) !!}
                                    <select  name="auditor4[]" class=" form-control "  id="auditor4" disabled multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                        
                                        <?php $datas =  json_decode($surdata->auditor4); foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } }?> >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>
                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Status *') ? ' has-error' : '' }}">
                                        {!! Form::label('status', trans('Choose Status')) !!}
                                        <select class="form-control" name="status" required disabled>
                                            <option value="">Choose Year</option>
                                            <option value="0" <?php if($surdata->status == "0") echo "selected"; ?>>In Progress</option>
                                            <option value="1" <?php if($surdata->status == "1") echo "selected"; ?>>Signed</option>
                                            <option value="2" <?php if($surdata->status == "2") echo "selected"; ?>>Cancelled</option>
                                        </select>
                                        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Comments') ? ' has-error' : '' }}">
                                    {!! Form::label('comments', trans(' Comments ')) !!}
                                    {!! Form::textarea('comments', old('comments',$surdata->comments), ['class' => 'form-control', 'placeholder' => trans('comments'),'readonly']) !!}
                                    {!! $errors->first('comments', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                        </div>

                            
                            

                        </div>
                    </div>
                </div>

                @else

<div class="box-body">
     <div class="tab-content">
         <!-- <input type="button" value="Add Company Details+" class="btn btn-primary " onClick="showHideDiv('divMsg')"/> -->

                <div class="tab-pane active" id="divMsg" >
                    <div class="box-body">
                        <div class="row">



                        <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Accreditation') ? ' has-error' : '' }}">
                                    {!! Form::label('accrediation', trans('Accreditation Type *')) !!}
                                     <select class="form-control" required name="accrediation">
                                        <option value="">--- Select Accreditation Type ---</option>
                                        <option value="1" <?php if($surdata->accrediation == 1) { echo "selected";} ?>>EIAC</option>
                                        <option value="2" <?php if($surdata->accrediation == 2) { echo "selected";}?>>IAS</option>
                                        <option value="3" <?php if($surdata->accrediation == 3) { echo "selected";}?>>ASCB[E]</option>
                                        <option value="4" <?php if($surdata->accrediation == 4) { echo "selected";}?>>JAS-ANZ</option>
                                        <option value="5" <?php if($surdata->accrediation == 5) { echo "selected";}?>>GAC</option>

                                      
                                        
                                    </select>
                                    {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Name*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_name', trans('Company Name *')) !!}
                                    {!! Form::text('comp_name', old('comp_name',$surdata->comp_name), ['class' => 'form-control','required', 'placeholder' => trans('Company Name')]) !!}
                                    {!! $errors->first('comp_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Address*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_address', trans('Company Address')) !!}
                                    {!! Form::text('comp_address', old('comp_address',$surdata->comp_address), ['class' => 'form-control','required', 'placeholder' => trans('Address')]) !!}
                                    {!! $errors->first('comp_address', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Certificate*') ? ' has-error' : '' }}">
                                    {!! Form::label('certificate', trans('Certificate Number')) !!}
                                    {!! Form::text('certificate', old('certificate',$surdata->certificate), ['class' => 'form-control','required', 'placeholder' => trans('Certificate')]) !!}
                                    {!! $errors->first('certificate', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name', trans('Select Standard *')) !!}
                                    <select  name="standard_name[]" class=" form-control " required id="standard_name" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                        <?php $datas =  json_decode($surdata->standard_name); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_from', trans('Validity From *')) !!}
                                    {!! Form::date('validity_from', old('validity_from',$surdata->validity_from), ['class' => 'form-control','required', 'placeholder' => trans('Validity from')]) !!}
                                    {!! $errors->first('validity_from', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity to*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_to', trans('Validity to *')) !!}
                                    {!! Form::date('validity_to', old('validity_to',$surdata->validity_to), ['class' => 'form-control','required','placeholder' => trans('Validity to')]) !!}
                                    {!! $errors->first('validity_to', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Total Amount') ? ' has-error' : '' }}">
                                    {!! Form::label('total_amount', trans('Total Amount *')) !!}
                                    {!! Form::text('total_amount', old('total_amount',$surdata->total_amount), ['class' => 'form-control','required', 'placeholder' => trans('Total Amount')]) !!}
                                    {!! $errors->first('total_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Amount Collected*') ? ' has-error' : '' }}">
                                    {!! Form::label('amount_collected', trans('Amount Collected *')) !!}
                                    {!! Form::text('amount_collected', old('amount_collected',$surdata->amount_collected), ['class' => 'form-control','required', 'placeholder' => trans('Amount Collected')]) !!}
                                    {!! $errors->first('amount_collected', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur1 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur1_amount', trans('Sur1 Amount *')) !!}
                                    {!! Form::text('sur1_amount', old('sur1_amount',$surdata->sur1_amount), ['class' => 'form-control','required', 'placeholder' => trans('Sur1 Amount')]) !!}
                                    {!! $errors->first('sur1_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur2 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur2_amount', trans('Sur2 Amount *')) !!}
                                    {!! Form::text('sur2_amount', old('sur2_amount',$surdata->sur2_amount), ['class' => 'form-control','required', 'placeholder' => trans('Sur2 Amount')]) !!}
                                    {!! $errors->first('sur2_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('aud_name', trans('Auditor Name')) !!}
                                    {!! Form::text('aud_name', old('aud_name',$surdata->aud_name), ['class' => 'form-control', 'placeholder' => trans('Auditor Name')]) !!}
                                    {!! $errors->first('aud_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1_date', trans('Stage1 Date')) !!}
                                    {!! Form::date('stage1_date', old('stage1_date',$surdata->stage1_date), ['class' => 'form-control', 'placeholder' => trans('Stage1 Date')]) !!}
                                    {!! $errors->first('stage1_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2_date', trans('Stage2 Date')) !!}
                                    {!! Form::date('stage2_date', old('stage2_date',$surdata->stage2_date), ['class' => 'form-control', 'placeholder' => trans('Stage2 Date')]) !!}
                                    {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                          
                        </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year1', trans('Choose Year')) !!}
                                        <select class="form-control" name="year1">
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($surdata->year1 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($surdata->year1 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($surdata->year1 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($surdata->year1 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($surdata->year1 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($surdata->year1 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($surdata->year1 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($surdata->year1 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor1') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor1', trans('Select Auditor ')) !!}
                                    <select  name="auditor1[]" class=" form-control " id="auditor1" multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                        
                                        <?php $datas =  json_decode($surdata->auditor1); foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } }?> >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>
                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year1', trans('Choose Year')) !!}
                                        <select class="form-control" name="year1">
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($surdata->year2 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($surdata->year2 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($surdata->year2 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($surdata->year2 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($surdata->year2 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($surdata->year2 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($surdata->year2 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($surdata->year2 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor2') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor2', trans('Select Auditor ')) !!}
                                    <select  name="auditor2[]" class=" form-control " id="auditor2" multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                        
                                        <?php $datas =  json_decode($surdata->auditor2); foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } }?> >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>
                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            </div>



                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year3', trans('Choose Year')) !!}
                                        <select class="form-control" name="year3">
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($surdata->year3 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($surdata->year3 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($surdata->year3 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($surdata->year3 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($surdata->year3 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($surdata->year3 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($surdata->year3 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($surdata->year3 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor3') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor3', trans('Select Auditor ')) !!}
                                    <select  name="auditor3[]" class=" form-control "  id="auditor3" multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                        
                                        <?php $datas =  json_decode($surdata->auditor3); foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } }?> >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>
                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                      
                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year4', trans('Choose Year')) !!}
                                        <select class="form-control" name="year4">
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($surdata->year4 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($surdata->year4 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($surdata->year4 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($surdata->year4 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($surdata->year4 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($surdata->year4 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($surdata->year4 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($surdata->year4 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor4') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor4', trans('Select Auditor ')) !!}
                                    <select  name="auditor4[]" class=" form-control "  id="auditor4" multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                        
                                        <?php $datas =  json_decode($surdata->auditor4); foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datas)) { foreach($datas as $data) { if($value->id == $data) { echo "selected";} } }?> >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>
                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Status *') ? ' has-error' : '' }}">
                                        {!! Form::label('status', trans('Choose Status')) !!}
                                        <select class="form-control" name="status" required>
                                            <option value="">Choose Year</option>
                                            <option value="0" <?php if($surdata->status == "0") echo "selected"; ?>>In Progress</option>
                                            <option value="1" <?php if($surdata->status == "1") echo "selected"; ?>>Signed</option>
                                            <option value="2" <?php if($surdata->status == "2") echo "selected"; ?>>Cancelled</option>
                                        </select>
                                        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Comments') ? ' has-error' : '' }}">
                                    {!! Form::label('comments', trans(' Comments ')) !!}
                                    {!! Form::textarea('comments', old('comments',$surdata->comments), ['class' => 'form-control', 'placeholder' => trans('comments')]) !!}
                                    {!! $errors->first('comments', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                        </div>

                            

                        </div>
                    </div>
                </div>  @endif