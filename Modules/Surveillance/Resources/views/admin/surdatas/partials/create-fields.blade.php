
<div class="box-body">
     <div class="tab-content">
         <!-- <input type="button" value="Add Company Details+" class="btn btn-primary " onClick="showHideDiv('divMsg')"/> -->

                <div class="tab-pane active" id="divMsg" >
                    <div class="box-body">
                        <div class="row">



                        <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('accrediation') ? ' has-error' : '' }}">
                                    {!! Form::label('accrediation', trans('Accreditation Type *')) !!}
                                     <select class="form-control" required name="accrediation">
                                        <option value="">--- Select Accreditation Type ---</option>
                                        <option value="1">EIAC</option>
                                        <option value="2">IAS</option>
                                        <option value="3">ASCB[E]</option>
                                        <option value="4">JAS-ANZ</option>
                                        <option value="5">GAC</option>
                                        
                                    </select>
                                    {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Name*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_name', trans('Company Name *')) !!}
                                    {!! Form::text('comp_name', old('comp_name'), ['class' => 'form-control','required', 'placeholder' => trans('Company Name')]) !!}
                                    {!! $errors->first('comp_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Address*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_address', trans('Company Address *')) !!}
                                    {!! Form::text('comp_address', old('comp_address'), ['class' => 'form-control','required', 'placeholder' => trans('Address')]) !!}
                                    {!! $errors->first('comp_address', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Certificate*') ? ' has-error' : '' }}">
                                    {!! Form::label('certificate', trans('Certificate Number *')) !!}
                                    {!! Form::text('certificate', old('certificate'), ['class' => 'form-control','required', 'placeholder' => trans('Certificate')]) !!}
                                    {!! $errors->first('certificate', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name', trans('Select Standard *')) !!}
                                    <select  name="standard_name[]" class=" form-control " required id="standard_name" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                        <?php foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        <?php endforeach; ?>

                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_from', trans('Validity From *')) !!}
                                    {!! Form::date('validity_from', old('validity_from'), ['class' => 'form-control','required', 'placeholder' => trans('Validity from')]) !!}
                                    {!! $errors->first('validity_from', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity to*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_to', trans('Validity to *')) !!}
                                    {!! Form::date('validity_to', old('validity_to'), ['class' => 'form-control','required', 'placeholder' => trans('Validity to')]) !!}
                                    {!! $errors->first('validity_to', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Total Amount') ? ' has-error' : '' }}">
                                    {!! Form::label('total_amount', trans('Total Amount •')) !!}
                                    {!! Form::text('total_amount', old('total_amount'), ['class' => 'form-control','required', 'placeholder' => trans('Total Amount')]) !!}
                                    {!! $errors->first('total_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('amount_collected', trans('Amount Collected ')) !!}
                                    {!! Form::text('amount_collected', old('amount_collected'), ['class' => 'form-control','required', 'placeholder' => trans('Amount Collected')]) !!}
                                    {!! $errors->first('amount_collected', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur1_amount', trans('Sur1 Amount *')) !!}
                                    {!! Form::text('sur1_amount', old('sur1_amount'), ['class' => 'form-control','required', 'placeholder' => trans('Sur1 Amount')]) !!}
                                    {!! $errors->first('sur1_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur2_amount', trans('Sur2 Amount *')) !!}
                                    {!! Form::text('sur2_amount', old('sur2_amount'), ['class' => 'form-control','required', 'placeholder' => trans('Sur2 Amount')]) !!}
                                    {!! $errors->first('sur2_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('aud_name', trans('Auditor Name')) !!}
                                    {!! Form::text('aud_name', old('aud_name'), ['class' => 'form-control', 'placeholder' => trans('Auditor Name')]) !!}
                                    {!! $errors->first('aud_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1_date', trans('Stage1 Date')) !!}
                                    {!! Form::date('stage1_date', old('stage1_date'), ['class' => 'form-control', 'placeholder' => trans('Stage1 Date')]) !!}
                                    {!! $errors->first('stage1_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2_date', trans('Stage2 Date')) !!}
                                    {!! Form::date('stage2_date', old('stage2_date'), ['class' => 'form-control', 'placeholder' => trans('Stage2 Date')]) !!}
                                    {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                          <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year1', trans('Choose Year')) !!}
                                        <select class="form-control" name="year1">
                                            <option value="">Choose Year</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                        </select>
                                        {!! $errors->first('year1', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                               <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor1') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor1', trans('Select Auditor ')) !!}
                                    <select  name="auditor1[]" class=" form-control " id="auditor1" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                        <?php foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('auditor1', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year2', trans('Choose Year')) !!}
                                        <select class="form-control" name="year2">
                                            <option value="">Choose Year</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                        </select>
                                        {!! $errors->first('year2', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                               <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor2') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor2', trans('Select Auditor ')) !!}
                                    <select  name="auditor2[]" class=" form-control " id="auditor2" multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                       
                                        <?php foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('auditor2', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            </div>



                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year3', trans('Choose Year')) !!}
                                        <select class="form-control" name="year3">
                                            <option value="">Choose Year</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                        </select>
                                        {!! $errors->first('year3', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                               <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor3') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor3', trans('Select Auditor ')) !!}
                                    <select  name="auditor3[]" class=" form-control " id="auditor3" multiple>
                                        <!-- <option value="">--- Select Auditor ---</option> -->
                                       
                                        <?php foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('auditor3', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                      
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year4', trans('Choose Year')) !!}
                                        <select class="form-control" name="year4">
                                            <option value="">Choose Year</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                        </select>
                                        {!! $errors->first('year4', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                               <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('auditor4') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor4', trans('Select Auditor ')) !!}
                                    <select  name="auditor4[]" class=" form-control " id="auditor4" multiple>
                                         <!-- <option value="">--- Select Auditor ---</option> -->
                                       
                                        <?php foreach ($users as $value): ?>
                                            <option value="{{ $value->id }}" >{{ $value->first_name }} {{ $value->last_name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('auditor4', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            </div>
                            
                            <div class="row">
                               <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('Comments') ? ' has-error' : '' }}">
                                        {!! Form::label('comments', trans(' Comments ')) !!}
                                        {!! Form::textarea('comments', old('comments'), ['class' => 'form-control', 'placeholder' => trans('comments')]) !!}
                                        {!! $errors->first('comments', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                              
                               
                        </div>
                         <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Status *') ? ' has-error' : '' }}">
                                        {!! Form::label('status', trans('Choose Status * ')) !!}
                                        <select class="form-control" name="status" required>
                                            <option value="">Choose Status</option>
                                            <option value="0">In Progress</option>
                                            <option value="1">Signed</option>
                                            <option value="2">Cancelled</option>
                                        </select>
                                        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <!-- <embed src="{{ URL::asset('reportnew.pdf')}}#toolbar=0&navpanes=0&scrollbar=0" width="425" height="425">
                                    <embed src="{{ URL::asset('report.pdf')}}#toolbar=0&navpanes=0&scrollbar=0" width="425" height="425"> -->

                                    <a href="{{ URL::asset('reportnew.pdf')}}" target="_Blank" class="btn btn-success">View Audit Report</a>
                                    <br/><br/>
                                    <a href="{{ URL::asset('report.pdf')}}" target="_Blank" class="btn btn-success">View Certificate</a>

                                </div>
                                <div class="col-sm-1">
                                </div>
                            </div>
                    </div>

                </div>

