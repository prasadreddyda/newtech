<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/surveillance'], function (Router $router) {
    $router->bind('surdata', function ($id) {
        return app('Modules\Surveillance\Repositories\SurdataRepository')->find($id);
    });
    $router->get('surdatas', [
        'as' => 'admin.surveillance.surdata.index',
        'uses' => 'SurdataController@index',
        'middleware' => 'can:surveillance.surdatas.index'
    ]);
    $router->get('surdatas/create', [
        'as' => 'admin.surveillance.surdata.create',
        'uses' => 'SurdataController@create',
        'middleware' => 'can:surveillance.surdatas.create'
    ]);
    $router->post('surdatas', [
        'as' => 'admin.surveillance.surdata.store',
        'uses' => 'SurdataController@store',
        'middleware' => 'can:surveillance.surdatas.create'
    ]);
    $router->get('surdatas/{surdata}/edit', [
        'as' => 'admin.surveillance.surdata.edit',
        'uses' => 'SurdataController@edit',
        'middleware' => 'can:surveillance.surdatas.edit'
    ]);
    $router->put('surdatas/{surdata}', [
        'as' => 'admin.surveillance.surdata.update',
        'uses' => 'SurdataController@update',
        'middleware' => 'can:surveillance.surdatas.edit'
    ]);
    $router->delete('surdatas/{surdata}', [
        'as' => 'admin.surveillance.surdata.destroy',
        'uses' => 'SurdataController@destroy',
        'middleware' => 'can:surveillance.surdatas.destroy'
    ]);

    $router->post('surdatas/search', [
        'as' => 'admin.surveillance.surdata.search',
        'uses' => 'SurdataController@search',
        'middleware' => 'can:surveillance.surdatas.create'
    ]);

// append

});
