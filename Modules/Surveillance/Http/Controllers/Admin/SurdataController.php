<?php

namespace Modules\Surveillance\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Surveillance\Entities\Surdata;
use Modules\Surveillance\Http\Requests\CreateSurdataRequest;
use Modules\Surveillance\Http\Requests\UpdateSurdataRequest;
use Modules\Surveillance\Repositories\SurdataRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Settings\Repositories\StandardsRepository;
use Modules\Scheme\Repositories\DataRepository;
use Modules\User\Contracts\Authentication;
use DB;
use phpDocumentor\Reflection\Types\Void_;
use Modules\User\Repositories\UserRepository;

class SurdataController extends AdminBaseController
{
    /**
     * @var SurdataRepository
     */
    private $surdata;

    public function __construct(SurdataRepository $surdata,StandardsRepository $standards,Authentication $auth,UserRepository $user,DataRepository $data)
    {
        parent::__construct();

        $this->surdata = $surdata;
        $this->standards = $standards;
        $this->auth = $auth;
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $log = $this->auth->user();
        $datas = $this->data->all();

        if(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'admin') {
            $surdatas = $this->surdata->all();
        } else {
            $surdatas = $this->surdata->getByAttributes(['user_id' => $log->id ]);
        }
        
        $standards = $this->standards->all();

        return view('surveillance::admin.surdatas.index', compact('surdatas','standards','datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $standards = $this->standards->all();

        $users =  DB::table('roles')
                        ->select('users.id','users.first_name','users.last_name')
                        ->join('role_users', 'role_users.role_id', 'roles.id')
                        ->join('users','users.id','role_users.user_id')
                        ->where('roles.slug', "auditor")
                        ->get();

        return view('surveillance::admin.surdatas.create',compact('standards','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSurdataRequest $request
     * @return Response
     */
    public function store(CreateSurdataRequest $request)
    {
        $log = $this->auth->user();
        $standard_name = json_encode($request->standard_name);
        $auditor1 = json_encode($request->auditor1);
        $auditor2 = json_encode($request->auditor2);
        $auditor3 = json_encode($request->auditor3);
        $auditor4 = json_encode($request->auditor4);
        // $data = array('user_id' => $log->id, 'exam_id' => $id, 'status' => 0);
        $request->merge(['user_id' => $log->id, 'standard_name' => $standard_name, 'auditor1' => $auditor1, 'auditor2' => $auditor2, 'auditor3'=> $auditor3, 'auditor4' => $auditor4]);
      

        $this->surdata->create($request->all());

        return redirect()->route('admin.surveillance.surdata.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('surveillance::surdatas.title.surdatas')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Surdata $surdata
     * @return Response
     */
    public function edit(Surdata $surdata)
    {
        if(isset($_GET['viewdata']))
        {
            $type = "viewdata";
            
        }
        else
        {
            $type = "edit";
        }
        $standards = $this->standards->all();

        $users =  DB::table('roles')
                        ->select('users.id','users.first_name','users.last_name')
                        ->join('role_users', 'role_users.role_id', 'roles.id')
                        ->join('users','users.id','role_users.user_id')
                        ->where('roles.slug', "auditor")
                        ->get();

        return view('surveillance::admin.surdatas.edit', compact('surdata','standards','type','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Surdata $surdata
     * @param  UpdateSurdataRequest $request
     * @return Response
     */
    public function update(Surdata $surdata, UpdateSurdataRequest $request)
    {
        $standard_name = json_encode($request->standard_name);
        $auditor1 = json_encode($request->auditor1);
        $auditor2 = json_encode($request->auditor2);
        $auditor3 = json_encode($request->auditor3);
        $auditor4 = json_encode($request->auditor4);

        $request->merge(['standard_name' => $standard_name, 'auditor1' => $auditor1, 'auditor2' => $auditor2, 'auditor3'=> $auditor3, 'auditor4' => $auditor4]);

        $this->surdata->update($surdata, $request->all());

        return redirect()->route('admin.surveillance.surdata.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('surveillance::surdatas.title.surdatas')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Surdata $surdata
     * @return Response
     */
    public function destroy(Surdata $surdata)
    {
        $this->surdata->destroy($surdata);

        return redirect()->route('admin.surveillance.surdata.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('surveillance::surdatas.title.surdatas')]));
    }

    public function search(Request $request) {
        if(isset($request->from) && isset($request->to)) {
            $datas = DB::table('scheme__data')
                            ->whereYear('validity_from', '>=', $request->from);
                            if(isset($request->from_month)) {
                             $datas = $datas->whereMonth('validity_from', '>=', $request->from_month);
                            }
                            
                            $datas =  $datas->whereYear('validity_to', '<=', $request->to);
                            if(isset($request->to_month)) {
                                $datas = $datas->whereMonth('validity_to', '<=', $request->to_month);
                            }
                            $datas = $datas->get();
        } elseif(isset($request->from)) {
            $datas = DB::table('scheme__data')
                            ->whereYear('validity_from', '=', $request->from);
                            if(isset($request->from_month)) {
                                $datas = $datas->whereMonth('validity_from', '=', $request->from_month);
                            }
                            $datas = $datas->get();
        }  elseif(isset($request->to)) {
            $datas = DB::table('scheme__data')
                            ->whereYear('validity_to', '=', $request->to);
                            if(isset($request->to_month)) {
                                $datas = $datas->whereMonth('validity_to', '=', $request->to_month);
                            }
                            $datas = $datas->get();
        } else {
            $datas = $this->data->all();
            $standards = $this->standards->all();
            return view('surveillance::admin.surdatas.index',compact('datas','standards'));
        }
    
            foreach($datas as $surdata) {
                $surdata->user = DB::table('users')->where('id', $surdata->user_id)->first();
            }
    
            $type = "search";
    
            return view('surveillance::admin.surdatas.index', compact('datas','type'));
        }
    }