<?php

namespace Modules\Surveillance\Repositories\Eloquent;

use Modules\Surveillance\Repositories\SurdataRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentSurdataRepository extends EloquentBaseRepository implements SurdataRepository
{
}
