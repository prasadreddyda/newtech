<?php

namespace Modules\Surveillance\Repositories\Cache;

use Modules\Surveillance\Repositories\SurdataRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheSurdataDecorator extends BaseCacheDecorator implements SurdataRepository
{
    public function __construct(SurdataRepository $surdata)
    {
        parent::__construct();
        $this->entityName = 'surveillance.surdatas';
        $this->repository = $surdata;
    }
}
