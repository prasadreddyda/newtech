<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeDataTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme__data_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('data_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['data_id', 'locale']);
            $table->foreign('data_id')->references('id')->on('scheme__data')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheme__data_translations', function (Blueprint $table) {
            $table->dropForeign(['data_id']);
        });
        Schema::dropIfExists('scheme__data_translations');
    }
}
