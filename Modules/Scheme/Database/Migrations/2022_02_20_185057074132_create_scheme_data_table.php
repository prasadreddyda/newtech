<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme__data', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('standard_name')->unsigned();
             
            $table->string('comp_name');
            $table->string('comp_address');
            $table->string('certificate');
            $table->integer('user_id')->unsigned();
            $table->string('accrediation');
            $table->string('validity_from');
            $table->string('validity_to');
            $table->string('total_amount');
            $table->string('amount_collected');
            $table->string('sur1_amount');
            $table->string('sur2_amount');
            $table->string('aud_name');
            $table->string('stage1_date');
            $table->string('stage2_date');

            $table->string('mobile');
            $table->string('landline');
            $table->string('sur_company');
            $table->string('reportdoc');
            $table->string('certificatedoc');
            $table->string('reportname');
            $table->string('certificatename');


            $table->string('standard_name1');
            $table->string('standard_name2');
            $table->string('standard_name3');
            $table->string('standard_name4');
            $table->string('stage1to_date');
            $table->string('stage2to_date');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('standard_name')->references('id')->on('settings__standards')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheme__data');
    }
}
