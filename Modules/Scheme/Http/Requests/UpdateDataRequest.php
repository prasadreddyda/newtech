<?php

namespace Modules\Scheme\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;
use Illuminate\Validation\Rule;

class UpdateDataRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            // 'amount_collected' => 'lte:total_amount',
            
            // 'comp_name' => [
            //     'required',
            //     Rule::unique('scheme__data')->ignore($this->id),
           
            // ],

        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
