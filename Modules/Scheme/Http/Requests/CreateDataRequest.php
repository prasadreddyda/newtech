<?php

namespace Modules\Scheme\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateDataRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            // 'certificate' => 'unique:scheme__data|max:255',
            // 'repo' => 'required',
            // 'certi' => 'required',
            // 'comp_name' => 'unique:scheme__data|max:255',
            // 'amount_collected' => 'lte:total_amount',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
