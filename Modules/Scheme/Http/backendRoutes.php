<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/scheme'], function (Router $router) {
    $router->bind('data', function ($id) {
        return app('Modules\Scheme\Repositories\DataRepository')->find($id);
    });
    $router->get('data', [
        'as' => 'admin.scheme.data.index',
        'uses' => 'DataController@index',
        'middleware' => 'can:scheme.data.index'
    ]);
    $router->get('data/create', [
        'as' => 'admin.scheme.data.create',
        'uses' => 'DataController@create',
        'middleware' => 'can:scheme.data.create'
    ]);
    $router->post('data', [
        'as' => 'admin.scheme.data.store',
        'uses' => 'DataController@store',
        'middleware' => 'can:scheme.data.create'
    ]);
    $router->get('data/{data}/edit', [
        'as' => 'admin.scheme.data.edit',
        'uses' => 'DataController@edit',
        'middleware' => 'can:scheme.data.edit'
    ]);
    $router->put('data/{data}', [
        'as' => 'admin.scheme.data.update',
        'uses' => 'DataController@update',
        'middleware' => 'can:scheme.data.edit'
    ]);
    $router->delete('data/{data}', [
        'as' => 'admin.scheme.data.destroy',
        'uses' => 'DataController@destroy',
        'middleware' => 'can:scheme.data.destroy'
    ]);

    $router->post('data/search', [
        'as' => 'admin.scheme.data.search',
        'uses' => 'DataController@search',
        'middleware' => 'can:scheme.data.create'
    ]);
// append

});
