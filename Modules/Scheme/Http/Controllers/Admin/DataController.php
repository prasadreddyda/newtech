<?php

namespace Modules\Scheme\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Scheme\Entities\Data;
use Modules\Scheme\Http\Requests\CreateDataRequest;
use Modules\Scheme\Http\Requests\UpdateDataRequest;
use Modules\Scheme\Repositories\DataRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Settings\Repositories\StandardsRepository;
use Modules\User\Contracts\Authentication;
use DB;
use phpDocumentor\Reflection\Types\Void_;
use Modules\User\Repositories\UserRepository;

class DataController extends AdminBaseController
{
    /**
     * @var DataRepository
     */
    private $data;

    public function __construct(DataRepository $data,StandardsRepository $standards,Authentication $auth,UserRepository $user)
    {
        parent::__construct();

        $this->data = $data;
        $this->standards = $standards;
        $this->auth = $auth;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $log = $this->auth->user();

        if(isset($log->roles[0]->slug)) {
            $role = $log->roles[0]->slug;
        } else {
            $role = "";
        }
       

        if(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'admin')
        {
           
            $datas = $this->data->all();
            
            
      
         

        }
        elseif(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'schemeadm')
        {
            $datas = $this->data->all();

        }
        else
        {
        
            $datas = $this->data->getByAttributes(['user_id' => $log->id ],'id','desc');
           
        //    dd($datas);
        }
       // $surdatas = $this->surdata->all();
        $standards = $this->standards->all();

       

        return view('scheme::admin.data.index', compact('datas','standards','role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $standards = $this->standards->all();

        $users =  DB::table('roles')
                        ->select('users.id','users.first_name','users.last_name')
                        ->join('role_users', 'role_users.role_id', 'roles.id')
                        ->join('users','users.id','role_users.user_id')
                        ->where('roles.slug', "auditor")
                        ->get();
        return view('scheme::admin.data.create',compact('standards','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateDataRequest $request
     * @return Response
     */
    public function store(CreateDataRequest $request)
    {
     

        if(isset($request->repo,$request->certi) && $request->repo && $request->certi )
        {
           

            $file_name = $_FILES['repo']['name'];
            $file_size =$_FILES['repo']['size'];
            $file_tmp =$_FILES['repo']['tmp_name'];
            $file_type=$_FILES['repo']['type'];

            $file_name1 = $_FILES['certi']['name'];
            $file_size1 = $_FILES['certi']['size'];
            $file_tmp1 = $_FILES['certi']['tmp_name'];
            $file_type1 = $_FILES['certi']['type'];
            
            $expensions= array("jpeg","jpg","png");

            $newfile = "assets/Reports/".time().$file_name;
            $newfile1 = "assets/Certificates/".time().$file_name1;

           

            if($file_size > 2097152){
               $errors[]='File size must be excately 2 MB';
            }
            
            
            if(empty($errors)==true){
               move_uploaded_file($file_tmp,$newfile);
               move_uploaded_file($file_tmp1,$newfile1);
               $request->merge(['reportdoc' => $newfile]);
               $request->merge(['certificatedoc' => $newfile1]);
               $request->merge(['reportname' => $file_name]);
               $request->merge(['certificatename' => $file_name1]);
            }
      }


      
      
        $log = $this->auth->user();
        $standard_name = json_encode($request->standard_name);
        $standard_name1 = json_encode($request->standard_name1);
        $standard_name2 = json_encode($request->standard_name2);
        $standard_name3 = json_encode($request->standard_name3);
        $standard_name4 = json_encode($request->standard_name4);
        // $aud_name = json_encode($request->aud_name);
        // $auditor1 = json_encode($request->auditor1);
        // $auditor2 = json_encode($request->auditor2);
        // $auditor3 = json_encode($request->auditor3);
        // $auditor4 = json_encode($request->auditor4);


    //     if($request->comp_address == NULL || $request->certificate == NULL || $request->standard_name == NULL || $request->validity_from == NULL || $request->validity_to == NULL || $request->total_amount == NULL || $request->amount_collected == NULL || $request->aud_name == NULL || $request->stage1_date == NULL || $request->stage1to_date == NULL || $request->stage2_date == NULL || $request->stage2to_date == NULL || $request->repo == NULL || $request->certi == NULL)
    //     {
    //         $filled = 0;
    //     }
    //     elseif($request->comp_address == "NIL" || $request->certificate == "NIL" || $request->standard_name == "NIL" || $request->validity_from == "NIL" || $request->validity_to == "NIL" || $request->total_amount == "NIL" || $request->amount_collected == "NIL" || $request->aud_name == "NIL" || $request->stage1_date == "NIL" || $request->stage1to_date == "NIL" || $request->stage2_date == "NIL" || $request->stage2to_date == "NIL" || $request->repo == "NIL" || $request->certi == "NIL")
    //     {
    //         $filled = 0;    
    //     }
    //    else
    //    {
    //        $filled = 1;
    //    }
       
        // $data = array('user_id' => $log->id, 'exam_id' => $id, 'status' => 0);
        $request->merge(['user_id' => $log->id, 'standard_name' => $standard_name, 'standard_name1' => $standard_name1, 'standard_name2' => $standard_name2, 'standard_name3' => $standard_name3, 'standard_name4' => $standard_name4]);
      
// dd($request);

        $this->data->create($request->all());

        return redirect()->route('admin.scheme.data.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('scheme::data.title.data')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Data $data
     * @return Response
     */
    public function edit(Data $data)
    {

        

        $log = $this->auth->user();
        if(isset($log->roles[0]->slug)) {
            $role = $log->roles[0]->slug;
        } else {
            $role = "";
        }

        if(isset($_GET['viewdata'])) {
            $type = "viewdata";
            
        } else if(isset($_GET['viewdata1'])) {
            $type = "viewdata1";
        }else
        {
            $type = "edit";
        }
        $standards = $this->standards->all();

      
    


        $users =  DB::table('roles')
                        ->select('users.id','users.first_name','users.last_name')
                        ->join('role_users', 'role_users.role_id', 'roles.id')
                        ->join('users','users.id','role_users.user_id')
                        ->where('roles.slug', "auditor")
                        ->get();

        return view('scheme::admin.data.edit', compact('data','standards','type','users','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Data $data
     * @param  UpdateDataRequest $request
     * @return Response
     */
    public function update(Data $data, UpdateDataRequest $request)
    {
        
        //  dd($request);

        $log = $this->auth->user();
        if(isset($log->roles[0]->slug) && $log->roles[0]->slug == "surveillance" ) {
           
            $this->data->update($data, ['status' => $request->status]);

            return redirect()->route('admin.surveillance.surdata.index')
                ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('surveillance::surdatas.title.surdatas')]));
        }
      
         else 
         {
              
            if(isset($request->repo,$request->certi) || $request->repo || $request->certi ) 
            {
                if($request->repo && $request->certi)
                {

                    // dd($request);
                    
                    $file_name = $_FILES['repo']['name'];
                    $file_size =$_FILES['repo']['size'];
                    $file_tmp =$_FILES['repo']['tmp_name'];
                    $file_type=$_FILES['repo']['type'];

                    $file_name1 = $_FILES['certi']['name'];
                    $file_size1 = $_FILES['certi']['size'];
                    $file_tmp1 = $_FILES['certi']['tmp_name'];
                    $file_type1 = $_FILES['certi']['type'];
            
                    $expensions= array("jpeg","jpg","png");

                    $newfile = "assets/Reports/".time().$file_name;
                    $newfile1 = "assets/Certificates/".time().$file_name1;

           

                        if($file_size > 2097152){
                            $errors[]='File size must be excately 2 MB';
                        }
            
            
                        if(empty($errors)==true){
                        move_uploaded_file($file_tmp,$newfile);
                        move_uploaded_file($file_tmp1,$newfile1);
                        $request->merge(['reportdoc' => $newfile]);
                        $request->merge(['certificatedoc' => $newfile1]);
                        $request->merge(['reportname' => $file_name]);
                        $request->merge(['certificatename' => $file_name1]);
                         }

                }
                elseif($request->repo)
                {
                $file_name = $_FILES['repo']['name'];
                $file_size =$_FILES['repo']['size'];
                $file_tmp =$_FILES['repo']['tmp_name'];
                $file_type=$_FILES['repo']['type'];
                $expensions= array("jpeg","jpg","png");

                $newfile = "assets/Reports/".time().$file_name;

                         if($file_size > 2097152)
                            {
                                 $errors[]='File size must be excately 2 MB';
                            }

                            if(empty($errors)==true){
                                move_uploaded_file($file_tmp,$newfile);
                              
                                $request->merge(['reportdoc' => $newfile]);
                                
                                $request->merge(['reportname' => $file_name]);
                                
                             }

                }
                elseif($request->certi)
                {
                    $file_name1 = $_FILES['certi']['name'];
                    $file_size1 = $_FILES['certi']['size'];
                    $file_tmp1 = $_FILES['certi']['tmp_name'];
                    $file_type1 = $_FILES['certi']['type'];

                    $expensions= array("jpeg","jpg","png");

                    $newfile1 = "assets/Certificates/".time().$file_name1;
    
                             if($file_size1 > 2097152)
                                {
                                     $errors[]='File size must be excately 2 MB';
                                }
    
                                if(empty($errors)==true){
                                    move_uploaded_file($file_tmp1,$newfile1);
                                  
                                    $request->merge(['certificatedoc' => $newfile1]);
                                    
                                    $request->merge(['certificatename' => $file_name1]);
                                    
                                 }
    
    
                }
               

               

               
                
                
               
            }
           
            // dd($request);

            $standard_name = json_encode($request->standard_name);
            $standard_name1 = json_encode($request->standard_name1);
            $standard_name2 = json_encode($request->standard_name2);
            $standard_name3 = json_encode($request->standard_name3);
            $standard_name4 = json_encode($request->standard_name4);


          

           $request->merge([ 'standard_name' => $standard_name, 'standard_name1' => $standard_name1, 'standard_name2' => $standard_name2, 'standard_name3' => $standard_name3, 'standard_name4' => $standard_name4]);



      

        //    $request->merge(['user_id' => $log->id, 'standard_name' => $standard_name, 'standard_name1' => $standard_name1, 'standard_name2' => $standard_name2, 'standard_name3' => $standard_name3, 'standard_name4' => $standard_name4]);



            $this->data->update($data, $request->all());

            
            

            return redirect()->route('admin.scheme.data.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('scheme::data.title.data')]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Data $data
     * @return Response
     */
    public function destroy(Data $data)
    {
        // if(file_exists(public_path($data->reportdoc)))
        // {
        //     unlink(public_path($data->reportdoc));
        //     }else{
        //     dd('File does not exists.');
        //     }

        //     if(file_exists(public_path($data->certificatedoc)))
        //     {
        //         unlink(public_path($data->certificatedoc));
        //         }else{
        //         dd('File does not exists.');
        //         }
        $this->data->destroy($data);

        return redirect()->route('admin.scheme.data.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('scheme::data.title.data')]));
    }



    public function search(Request $request) {
        $log = $this->auth->user();

        if(isset($log->roles[0]->slug)) 
        {
            $role = $log->roles[0]->slug;
        } else {
            $role = "";
        }



    if(($role == "admin") || ($role == "schemeadm")) 
      {
         
        if(isset($request->from) && isset($request->to))
            {
                             $datas = DB::table('scheme__data')
                            ->whereYear('validity_from', '>=', $request->from);
                            
                            if(isset($request->from_month))
                                {
                                     $datas = $datas->whereMonth('validity_from', '>=', $request->from_month);
                                }
                            
                            $datas =  $datas->whereYear('validity_to', '<=', $request->to);
                            if(isset($request->to_month)) 
                                {
                                     $datas = $datas->whereMonth('validity_to', '<=', $request->to_month);
                                }
                            $datas = $datas->get();
                            
             } 
        elseif(isset($request->from)) 
             {
                             $datas = DB::table('scheme__data')
                            ->whereYear('validity_from', '=', $request->from);
                            if(isset($request->from_month)) 
                                 {
                                    $datas = $datas->whereMonth('validity_from', '=', $request->from_month);
                                 }   
                                 
                            $datas = $datas->get();
                        


                            // dd($datas);

            }  
        elseif(isset($request->to)) 
            {
                             $datas = DB::table('scheme__data')
                            ->whereYear('validity_to', '=', $request->to);
                            if(isset($request->to_month)) 
                                {
                                    $datas = $datas->whereMonth('validity_to', '=', $request->to_month);
                                }
                            $datas = $datas->get();
            } 
        else 
            {
                            //  $datas = $this->data->getByAttributes(['user_id' => $log->id ]);
                             $datas = $this->data->all();

                             $standards = $this->standards->all();
                             return view('scheme::admin.data.index',compact('datas','standards','role'));
                            //  return view('scheme::admin.data.index', compact('datas','type','role'));

            }
    
        }
        else
        {
            // dd($role);
            if(isset($request->from) && isset($request->to))
            {
                             $datas = DB::table('scheme__data')
                            ->whereYear('validity_from', '>=', $request->from)
                            ->where('user_id', '=', $log->id);
                            
                            if(isset($request->from_month))
                                {
                                     $datas = $datas->whereMonth('validity_from', '>=', $request->from_month);
                                }
                            
                            $datas =  $datas->whereYear('validity_to', '<=', $request->to);
                            if(isset($request->to_month)) 
                                {
                                     $datas = $datas->whereMonth('validity_to', '<=', $request->to_month);
                                }
                            $datas = $datas->get();
             } 
        elseif(isset($request->from)) 
             {
                             $datas = DB::table('scheme__data')
                            ->whereYear('validity_from', '=', $request->from)
                            ->where('user_id', '=', $log->id);
                            if(isset($request->from_month)) 
                                 {
                                    $datas = $datas->whereMonth('validity_from', '=', $request->from_month);
                                 }   
                            $datas = $datas->get();
            }  
        elseif(isset($request->to)) 
            {
                             $datas = DB::table('scheme__data')
                            ->whereYear('validity_to', '=', $request->to)
                            ->where('user_id', '=', $log->id);
                            if(isset($request->to_month)) 
                                {
                                    $datas = $datas->whereMonth('validity_to', '=', $request->to_month);
                                }
                            $datas = $datas->get();
            } 
        else 
            {
                             $datas = $this->data->getByAttributes(['user_id' => $log->id ]);
                            //  $datas = $this->data->all();
                             $standards = $this->standards->all();
                             return view('scheme::admin.data.index',compact('datas','standards','role'));
            }
    

        }





     foreach($datas as $surdata) 
     {
        $surdata->user = DB::table('users')->where('id', $surdata->user_id)->first();
     }
    
    //  dd($datas);
            $type = "search";
    
            return view('scheme::admin.data.index', compact('datas','type','role'));
        }
}