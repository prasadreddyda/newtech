

@if($type == "viewdata")

<div class="box-body">
     <div class="tab-content">
         <!-- <input type="button" value="Add Company Details+" class="btn btn-primary " onClick="showHideDiv('divMsg')"/> -->

                <div class="tab-pane active" id="divMsg" >
                    <div class="box-body">
                        <div class="row">



                        <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Accreditation') ? ' has-error' : '' }}">
                                    {!! Form::label('accrediation', trans('Accreditation Type *')) !!}
                                     <select class="form-control"  name="accrediation" disabled>
                                        <option value="">--- Select Accreditation Type ---</option>
                                        <option value="1" <?php if($data->accrediation == 1) { echo "selected";} ?>>EIAC</option>
                                        <option value="2" <?php if($data->accrediation == 2) { echo "selected";}?>>IAS</option>
                                        <option value="3" <?php if($data->accrediation == 3) { echo "selected";}?>>ASCB[E]</option>
                                        <option value="4" <?php if($data->accrediation == 4) { echo "selected";}?>>JAS-ANZ</option>
                                        <option value="5" <?php if($data->accrediation == 5) { echo "selected";}?>>GAC</option>

                                      
                                        
                                    </select>
                                    {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Name*') ? ' has-error' : '' }}">
                                {!! Form::label('comp_name', trans('Company Name')) !!}
                                    <span style="color:red;">
                                  
                                    {!! Form::label('comp_name', trans(' *')) !!}

                                    </span>
                                    {!! Form::text('comp_name', old('comp_name',$data->comp_name), ['class' => 'form-control','required', 'placeholder' => trans('Company Name'),'readonly']) !!}
                                    {!! $errors->first('comp_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Address*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_address', trans('Company Address')) !!}
                                     <span style="color:red;">
                                  
                                     {!! Form::label('comp_address', trans(' *')) !!}

                                     </span>
                                    {!! Form::text('comp_address', old('comp_address',$data->comp_address), ['class' => 'form-control','required', 'placeholder' => trans('Address'),'readonly']) !!}
                                    {!! $errors->first('comp_address', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Mobile No') ? ' has-error' : '' }}">
                                    {!! Form::label('mobile', trans('Mobile No')) !!}
                                    {!! Form::text('mobile', old('mobile',$data->mobile), ['class' => 'form-control', 'placeholder' => trans('Mobile No'),'readonly']) !!}
                                    {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Landline No ') ? ' has-error' : '' }}">
                                    {!! Form::label('landline', trans('Landline No ')) !!}
                                    {!! Form::text('landline', old('landline',$data->landline), ['class' => 'form-control', 'placeholder' => trans('Landline No'),'readonly']) !!}
                                    {!! $errors->first('landline', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Email ID ') ? ' has-error' : '' }}">
                                    {!! Form::label('email_id', trans('Email Id ')) !!}
                                    <span style="color:red;">
                                  
                                     {!! Form::label('email_id', trans(' *')) !!}

                                  </span>
                                    {!! Form::email('email_id', old('email_id',$data->email_id), ['class' => 'form-control', 'placeholder' => trans('Email Id'),'readonly']) !!}
                                    {!! $errors->first('email_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Certificate*') ? ' has-error' : '' }}">
                                    {!! Form::label('certificate', trans('Certificate Number')) !!}
                                    <span style="color:red;">
                                  
                                    {!! Form::label('certificate', trans(' *')) !!}

                                    </span>
                                    {!! Form::text('certificate', old('certificate',$data->certificate), ['class' => 'form-control','required', 'placeholder' => trans('Certificate'),'readonly']) !!}
                                    {!! $errors->first('certificate', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name', trans('Select Standard ')) !!}
                                    <span style="color:red;">
                                  
                                     {!! Form::label('standard_name', trans(' *')) !!}

                                    </span>
                                    <select  name="standard_name[]" class=" form-control " disabled required id="standard_name" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_from', trans('Validity From ')) !!}
                                    <span style="color:red;">
                                  
                                    {!! Form::label('validity_from', trans(' *')) !!}

                                    </span>
                                    {!! Form::date('validity_from', old('validity_from',$data->validity_from), ['class' => 'form-control','required', 'placeholder' => trans('Validity from'),'readonly']) !!}
                                    {!! $errors->first('validity_from', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity to*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_to', trans('Validity to ')) !!}
                                    <span style="color:red;">
                                  
                                    {!! Form::label('validity_to', trans(' *')) !!}

                                    </span>
                                    {!! Form::date('validity_to', old('validity_to',$data->validity_to), ['class' => 'form-control','required','placeholder' => trans('Validity to'),'readonly']) !!}
                                    {!! $errors->first('validity_to', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Total Amount') ? ' has-error' : '' }}">
                                    {!! Form::label('total_amount', trans('Total Amount ')) !!}
                                    <span style="color:red;">
                                    
                                    {!! Form::label('total_amount', trans(' *')) !!}

                                    </span>
                                    {!! Form::text('total_amount', old('total_amount',$data->total_amount), ['class' => 'form-control','required', 'placeholder' => trans('Total Amount'),'readonly']) !!}
                                    {!! $errors->first('total_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Amount Collected*') ? ' has-error' : '' }}">
                                    {!! Form::label('amount_collected', trans('Amount Collected ')) !!}
                                    <span style="color:red;">
                                    
                                    {!! Form::label('amount_collected', trans(' *')) !!}

                                    </span>
                                    {!! Form::text('amount_collected', old('amount_collected',$data->amount_collected), ['class' => 'form-control','required', 'placeholder' => trans('Amount Collected'),'readonly']) !!}
                                    {!! $errors->first('amount_collected', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur1 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur1_amount', trans('Sur1 Amount ')) !!}
                                    {!! Form::text('sur1_amount', old('sur1_amount',$data->sur1_amount), ['class' => 'form-control','required', 'placeholder' => trans('Sur1 Amount'),'readonly']) !!}
                                    {!! $errors->first('sur1_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur2 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur2_amount', trans('Sur2 Amount ')) !!}
                                    {!! Form::text('sur2_amount', old('sur2_amount',$data->sur2_amount), ['class' => 'form-control','required', 'placeholder' => trans('Sur2 Amount'),'readonly']) !!}
                                    {!! $errors->first('sur2_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('aud_name', trans('Auditor Name')) !!}
                                    <span style="color:red;">
                                    
                                    {!! Form::label('aud_name', trans(' *')) !!}

                                    </span>
                                    {!! Form::text('aud_name', old('aud_name',$data->aud_name), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('aud_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            

                            <!-- <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('aud_name', trans('Auditor Name')) !!}
                                    {!! Form::text('aud_name', old('aud_name',$data->aud_name), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('aud_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div> -->
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1_date', trans('Stage1 From Date ')) !!}
                                    <span style="color:red;">
                                    
                                    {!! Form::label('stage1_date', trans(' *')) !!}

                                    </span>
                                    {!! Form::date('stage1_date', old('stage1_date',$data->stage1_date), ['class' => 'form-control','required', 'placeholder' => trans('Stage1 Date'),'readonly']) !!}
                                    {!! $errors->first('stage1_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1to_date', trans('Stage1 To Date ')) !!}
                                    <span style="color:red;">
                                    
                                    {!! Form::label('stage1to_date', trans(' *')) !!}

                                    </span>
                                    {!! Form::date('stage1to_date', old('stage1to_date',$data->stage1to_date), ['class' => 'form-control','required', 'placeholder' => trans('Stage1 To Date'),'readonly']) !!}
                                    {!! $errors->first('stage1to_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                           

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2_date', trans('Stage2 From Date ')) !!}
                                    <span style="color:red;">
                                    
                                    {!! Form::label('stage2_date', trans(' *')) !!}

                                    </span>
                                    {!! Form::date('stage2_date', old('stage2_date',$data->stage2_date), ['class' => 'form-control','required', 'placeholder' => trans('Stage2 Date'),'readonly']) !!}
                                    {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2to_date', trans('Stage2 To Date ')) !!}
                                    <span style="color:red;">
                                    
                                    {!! Form::label('stage2to_date', trans(' *')) !!}

                                    </span>
                                    {!! Form::date('stage2to_date', old('stage2to_date',$data->stage2to_date), ['class' => 'form-control','required', 'placeholder' => trans('Stage2 To Date'),'readonly']) !!}
                                    {!! $errors->first('stage2to_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                         </div>
                         <input type="button" style="font-weight:bold;font-size:17px;text-align:center;"value=" Auditor And Standard History" class="btn  btn-success btn-block" onClick="showHideDiv('divMsg')"/>
<br/>
                    <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year1', trans('Choose Year')) !!}
                                        <select class="form-control" name="year1" disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year1 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year1 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year1 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year1 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year1 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year1 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year1 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year1 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor1', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor1', old('auditor1',$data->auditor1), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('auditor1', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>



                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name1') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name1', trans('Select Standard *')) !!}
                                    <select  name="standard_name1[]" class=" form-control "  disabled id="standard_name1" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name1); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name1', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                    </div>
                    

                    <div class="row">
                             <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year1', trans('Choose Year')) !!}
                                        <select class="form-control" name="year1"  disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year2 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year2 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year2 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year2 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year2 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year2 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year2 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year2 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor2', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor2', old('auditor2',$data->auditor2), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('auditor2', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name2') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name2', trans('Select Standard *')) !!}
                                    <select  name="standard_name2[]" class=" form-control "  disabled id="standard_name2" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name2); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name2', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        
                    </div>


                    <div class="row">
                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year3', trans('Choose Year')) !!}
                                        <select class="form-control" name="year3" disabled >
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year3 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year3 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year3 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year3 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year3 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year3 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year3 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year3 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor3', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor3', old('auditor3',$data->auditor3), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('auditor3', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name3') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name3', trans('Select Standard *')) !!}
                                    <select  name="standard_name3[]" class=" form-control "  disabled id="standard_name3" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name3); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name3', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                    </div>
                        
                    <div class="row">
                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year4', trans('Choose Year')) !!}
                                        <select class="form-control" name="year4" disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year4 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year4 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year4 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year4 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year4 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year4 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year4 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year4 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor4', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor4', old('auditor1',$data->auditor4), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('auditor4', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                           
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name4') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name4', trans('Select Standard *')) !!}
                                    <select  name="standard_name4[]" class=" form-control "  disabled id="standard_name4" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name4); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name4', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                </div>
                          



                           

                            <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('Comments') ? ' has-error' : '' }}">
                                    {!! Form::label('comments', trans(' Comments ')) !!}
                                    {!! Form::textarea('comments', old('comments',$data->comments), ['class' => 'form-control', 'placeholder' => trans('comments'),'readonly']) !!}
                                    {!! $errors->first('comments', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                         
                          

                             <div class="col-sm-2">
                               
                                @if(isset($role) && $role == "surveillance") 
                                        <div class="form-group{{ $errors->has('Status *') ? ' has-error' : '' }}">
                                            {!! Form::label('status', trans('Choose Status')) !!}
                                            <select class="form-control" name="status">
                                                <option value="">Choose Team</option>
                                                <option value="0" <?php if($data->status == "0") echo "selected"; ?>>In Progress</option>
                                                <option value="1" <?php if($data->status == "1") echo "selected"; ?>>Signed</option>
                                                <option value="2" <?php if($data->status == "2") echo "selected"; ?>>Cancelled</option>
                                                <option value="3" <?php if($data->status == "3") echo "selected"; ?>>Suspended</option>

                                            </select>
                                            {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                        </div>

                                @endif

                               @if(isset($role) && $role != "surveillance")         
                                   <td>
                                   <span style="color:red;">
                                    
                                    {!! Form::label('reportdoc', trans(' *')) !!}

                                    </span>
                                       <a href="{{ URL::asset($data->reportdoc) }}" target="_Blank" class="btn btn-success" class="fa fa-eye" >View Audit Report</a>
                                   </td>
                                @endif
                               <br/><br/>
                               <td>
                               <span style="color:red;">
                                    
                                    {!! Form::label('certificatedoc', trans(' *')) !!}

                                    </span>
                                   <a href="{{ URL::asset($data->certificatedoc) }}" target="_Blank" class="btn btn-success"> View Certificate</a>
                               </td>
                            </div>

                            <div class="col-sm-3">
                            
                           </div>


                           
                        </div>

                            
                            

                        </div>
                    </div>
                </div>

                @elseif($type == "viewdata1")

                <div class="box-body">
     <div class="tab-content">
         <!-- <input type="button" value="Add Company Details+" class="btn btn-primary " onClick="showHideDiv('divMsg')"/> -->

                <div class="tab-pane active" id="divMsg" >
                    <div class="box-body">
                        <div class="row">



                        <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Accreditation') ? ' has-error' : '' }}">
                                    {!! Form::label('accrediation', trans('Accreditation Type *')) !!}
                                     <select class="form-control"  name="accrediation" disabled>
                                        <option value="">--- Select Accreditation Type ---</option>
                                        <option value="1" <?php if($data->accrediation == 1) { echo "selected";} ?>>EIAC</option>
                                        <option value="2" <?php if($data->accrediation == 2) { echo "selected";}?>>IAS</option>
                                        <option value="3" <?php if($data->accrediation == 3) { echo "selected";}?>>ASCB[E]</option>
                                        <option value="4" <?php if($data->accrediation == 4) { echo "selected";}?>>JAS-ANZ</option>
                                        <option value="5" <?php if($data->accrediation == 5) { echo "selected";}?>>GAC</option>

                                      
                                        
                                    </select>
                                    {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Name*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_name', trans('Company Name *')) !!}
                                    {!! Form::text('comp_name', old('comp_name',$data->comp_name), ['class' => 'form-control','required', 'placeholder' => trans('Company Name'),'readonly']) !!}
                                    {!! $errors->first('comp_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Address*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_address', trans('Company Address')) !!}
                                    {!! Form::text('comp_address', old('comp_address',$data->comp_address), ['class' => 'form-control','required', 'placeholder' => trans('Address'),'readonly']) !!}
                                    {!! $errors->first('comp_address', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Mobile No') ? ' has-error' : '' }}">
                                    {!! Form::label('mobile', trans('Mobile No')) !!}
                                    {!! Form::text('mobile', old('mobile',$data->mobile), ['class' => 'form-control', 'placeholder' => trans('Mobile No'),'readonly']) !!}
                                    {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Landline No ') ? ' has-error' : '' }}">
                                    {!! Form::label('landline', trans('Landline No ')) !!}
                                    {!! Form::text('landline', old('landline',$data->landline), ['class' => 'form-control', 'placeholder' => trans('Landline No'),'readonly']) !!}
                                    {!! $errors->first('landline', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Email ID ') ? ' has-error' : '' }}">
                                    {!! Form::label('email_id', trans('Email Id ')) !!}
                                    {!! Form::email('email_id', old('email_id',$data->email_id), ['class' => 'form-control', 'placeholder' => trans('Email Id'),'readonly']) !!}
                                    {!! $errors->first('email_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Certificate*') ? ' has-error' : '' }}">
                                    {!! Form::label('certificate', trans('Certificate Number')) !!}
                                    {!! Form::text('certificate', old('certificate',$data->certificate), ['class' => 'form-control', 'placeholder' => trans('Certificate'),'readonly']) !!}
                                    
                                    <!-- {!! $errors->first('certificate', '<span class="help-block">:message</span>') !!} -->
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name', trans('Select Standard *')) !!}
                                    <select  name="standard_name[]" class=" form-control " disabled required id="standard_name" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_from', trans('Validity From *')) !!}
                                    {!! Form::date('validity_from', old('validity_from',$data->validity_from), ['class' => 'form-control','required', 'placeholder' => trans('Validity from'),'readonly']) !!}
                                    {!! $errors->first('validity_from', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity to*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_to', trans('Validity to *')) !!}
                                    {!! Form::date('validity_to', old('validity_to',$data->validity_to), ['class' => 'form-control','required','placeholder' => trans('Validity to'),'readonly']) !!}
                                    {!! $errors->first('validity_to', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Total Amount') ? ' has-error' : '' }}">
                                    {!! Form::label('total_amount', trans('Total Amount *')) !!}
                                    {!! Form::text('total_amount', old('total_amount',$data->total_amount), ['class' => 'form-control','required', 'placeholder' => trans('Total Amount'),'readonly']) !!}
                                    {!! $errors->first('total_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Amount Collected*') ? ' has-error' : '' }}">
                                    {!! Form::label('amount_collected', trans('Amount Collected *')) !!}
                                    {!! Form::text('amount_collected', old('amount_collected',$data->amount_collected), ['class' => 'form-control','required', 'placeholder' => trans('Amount Collected'),'readonly']) !!}
                                    {!! $errors->first('amount_collected', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur1 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur1_amount', trans('Sur1 Amount *')) !!}
                                    {!! Form::text('sur1_amount', old('sur1_amount',$data->sur1_amount), ['class' => 'form-control','required', 'placeholder' => trans('Sur1 Amount'),'readonly']) !!}
                                    {!! $errors->first('sur1_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur2 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur2_amount', trans('Sur2 Amount *')) !!}
                                    {!! Form::text('sur2_amount', old('sur2_amount',$data->sur2_amount), ['class' => 'form-control','required', 'placeholder' => trans('Sur2 Amount'),'readonly']) !!}
                                    {!! $errors->first('sur2_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('aud_name', trans('Auditor Name')) !!}
                                    {!! Form::text('aud_name', old('aud_name',$data->aud_name), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('aud_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <!-- <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('aud_name', trans('Auditor Name')) !!}
                                    {!! Form::text('aud_name', old('aud_name',$data->aud_name), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('aud_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div> -->
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1_date', trans('Stage1 From Date *')) !!}
                                    {!! Form::date('stage1_date', old('stage1_date',$data->stage1_date), ['class' => 'form-control','required', 'placeholder' => trans('Stage1 Date'),'readonly']) !!}
                                    {!! $errors->first('stage1_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1to_date', trans('Stage1 To Date *')) !!}
                                    {!! Form::date('stage1to_date', old('stage1to_date',$data->stage1to_date), ['class' => 'form-control','required', 'placeholder' => trans('Stage1 To Date'),'readonly']) !!}
                                    {!! $errors->first('stage1to_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2_date', trans('Stage2 From Date *')) !!}
                                    {!! Form::date('stage2_date', old('stage2_date',$data->stage2_date), ['class' => 'form-control','required', 'placeholder' => trans('Stage2 Date'),'readonly']) !!}
                                    {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2to_date', trans('Stage2 To Date *')) !!}
                                    {!! Form::date('stage2to_date', old('stage2to_date',$data->stage2to_date), ['class' => 'form-control','required', 'placeholder' => trans('Stage2 To Date'),'readonly']) !!}
                                    {!! $errors->first('stage2to_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                        <input type="button" style="font-weight:bold;font-size:17px;text-align:center;"value=" Auditor And Standard History" class="btn  btn-success btn-block" onClick="showHideDiv('divMsg')"/>
<br/>
                    <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year1', trans('Choose Year')) !!}
                                        <select class="form-control" name="year1" disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year1 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year1 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year1 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year1 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year1 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year1 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year1 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year1 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor1', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor1', old('auditor1',$data->auditor1), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('auditor1', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name1') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name1', trans('Select Standard *')) !!}
                                    <select  name="standard_name1[]" class=" form-control "  disabled id="standard_name1" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name1); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name1', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                    </div>
                    

                    <div class="row">
                             <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year1', trans('Choose Year')) !!}
                                        <select class="form-control" name="year1"  disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year2 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year2 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year2 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year2 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year2 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year2 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year2 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year2 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor2', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor2', old('auditor2',$data->auditor2), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('auditor2', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                            

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name2') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name2', trans('Select Standard *')) !!}
                                    <select  name="standard_name2[]" class=" form-control "  disabled id="standard_name2" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name2); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name2', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        
                    </div>


                    <div class="row">
                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year3', trans('Choose Year')) !!}
                                        <select class="form-control" name="year3" disabled >
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year3 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year3 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year3 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year3 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year3 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year3 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year3 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year3 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                           
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor3', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor3', old('auditor3',$data->auditor3), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('auditor3', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name3') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name3', trans('Select Standard *')) !!}
                                    <select  name="standard_name3[]" class=" form-control "  disabled id="standard_name3" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name3); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name3', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                    </div>
                        
                    <div class="row">
                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year4', trans('Choose Year')) !!}
                                        <select class="form-control" name="year4" disabled>
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year4 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year4 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year4 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year4 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year4 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year4 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year4 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year4 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>

                          
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor4', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor4', old('auditor1',$data->auditor4), ['class' => 'form-control', 'placeholder' => trans('Auditor Name'),'readonly']) !!}
                                    {!! $errors->first('auditor4', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name4') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name4', trans('Select Standard *')) !!}
                                    <select  name="standard_name4[]" class=" form-control "  disabled id="standard_name4" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name4); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name4', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                </div>
                          



                           

                            <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('Comments') ? ' has-error' : '' }}">
                                    {!! Form::label('comments', trans(' Comments ')) !!}
                                    {!! Form::textarea('comments', old('comments',$data->comments), ['class' => 'form-control', 'placeholder' => trans('comments'),'readonly']) !!}
                                    {!! $errors->first('comments', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                         
                          

                             <div class="col-sm-2">
                               
                                @if(isset($role) && $role == "surveillance") 
                                        <div class="form-group{{ $errors->has('Status *') ? ' has-error' : '' }}">
                                            {!! Form::label('status', trans('Choose Status')) !!}
                                            <select class="form-control" name="status">
                                                <option value="">Choose Status</option>
                                                <option value="0" <?php if($data->status == "0") echo "selected"; ?>>In Progress</option>
                                                <option value="1" <?php if($data->status == "1") echo "selected"; ?>>Signed</option>
                                                <option value="2" <?php if($data->status == "2") echo "selected"; ?>>Cancelled</option>
                                                <option value="3" <?php if($data->status == "3") echo "selected"; ?>>Suspended</option>

                                            </select>
                                            {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                        </div>

                                @endif

                               @if(isset($role) && $role != "surveillance")         
                                   <td>
                                       <a href="{{ URL::asset($data->reportdoc) }}" target="_Blank" class="btn btn-success" class="fa fa-eye" >View Audit Report</a>
                                   </td>
                                @endif
                               <br/><br/>
                               <td>
                                   <a href="{{ URL::asset($data->certificatedoc) }}" target="_Blank" class="btn btn-success"> View Certificate</a>
                               </td>
                            </div>
                            <div class="form-group{{ $errors->has('Id') ? ' has-error' : '' }} " hidden >
                                 {!! Form::label('id', trans('Company Name *')) !!}
                                  {!! Form::text('id', old('id',$data->id), ['class' => 'form-control','required', 'placeholder' => trans('company name *')]) !!}
                                  {!! $errors->first('id', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="col-sm-3">
                            
                           </div>


                           
                        </div>

                            
                            

                        </div>
                    </div>
                </div>

                @else

<div class="box-body">
     <div class="tab-content">
         <!-- <input type="button" value="Add Company Details+" class="btn btn-primary " onClick="showHideDiv('divMsg')"/> -->

                <div class="tab-pane active" id="divMsg" >
                    <div class="box-body">
                        <div class="row">



                      
                        <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Accreditation') ? ' has-error' : '' }}">
                                    {!! Form::label('accrediation', trans('Accreditation Type *')) !!}
                                     <select class="form-control"  name="accrediation" >
                                        <option value="">--- Select Accreditation Type ---</option>
                                        <option value="1" <?php if($data->accrediation == 1) { echo "selected";} ?>>EIAC</option>
                                        <option value="2" <?php if($data->accrediation == 2) { echo "selected";}?>>IAS</option>
                                        <option value="3" <?php if($data->accrediation == 3) { echo "selected";}?>>ASCB[E]</option>
                                        <option value="4" <?php if($data->accrediation == 4) { echo "selected";}?>>JAS-ANZ</option>
                                        <option value="5" <?php if($data->accrediation == 5) { echo "selected";}?>>GAC</option>

                                      
                                        
                                    </select>
                                    {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Name*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_name', trans('Company Name *')) !!}
                                    {!! Form::text('comp_name', old('comp_name',$data->comp_name), ['class' => 'form-control', 'placeholder' => trans('Company Name')]) !!}
                                    {!! $errors->first('comp_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Company Address*') ? ' has-error' : '' }}">
                                    {!! Form::label('comp_address', trans('Company Address')) !!}
                                    {!! Form::text('comp_address', old('comp_address',$data->comp_address), ['class' => 'form-control', 'placeholder' => trans('Address')]) !!}
                                    {!! $errors->first('comp_address', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Mobile No') ? ' has-error' : '' }}">
                                    {!! Form::label('mobile', trans('Mobile No')) !!}
                                    {!! Form::text('mobile', old('mobile',$data->mobile), ['class' => 'form-control', 'placeholder' => trans('Mobile No')]) !!}
                                    {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Landline No ') ? ' has-error' : '' }}">
                                    {!! Form::label('landline', trans('Landline No ')) !!}
                                    {!! Form::text('landline', old('landline',$data->landline), ['class' => 'form-control', 'placeholder' => trans('Landline No')]) !!}
                                    {!! $errors->first('landline', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Email ID ') ? ' has-error' : '' }}">
                                    {!! Form::label('email_id', trans('Email Id ')) !!}
                                    {!! Form::email('email_id', old('email_id',$data->email_id), ['class' => 'form-control', 'placeholder' => trans('Email Id')]) !!}
                                    {!! $errors->first('email_id', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Certificate*') ? ' has-error' : '' }}">
                                    {!! Form::label('certificate', trans('Certificate Number')) !!}
                                    {!! Form::text('certificate', old('certificate',$data->certificate), ['class' => 'form-control', 'placeholder' => trans('Certificate')]) !!}
                                    <!-- {!! $errors->first('certificate', '<span class="help-block">:message</span>') !!} -->
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name', trans('Select Standard *')) !!}
                                    <select  name="standard_name[]" class=" form-control "   id="standard_name" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_from', trans('Validity From *')) !!}
                                    {!! Form::date('validity_from', old('validity_from',$data->validity_from), ['class' => 'form-control', 'placeholder' => trans('Validity from')]) !!}
                                    {!! $errors->first('validity_from', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity to*') ? ' has-error' : '' }}">
                                    {!! Form::label('validity_to', trans('Validity to *')) !!}
                                    {!! Form::date('validity_to', old('validity_to',$data->validity_to), ['class' => 'form-control','placeholder' => trans('Validity to')]) !!}
                                    {!! $errors->first('validity_to', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Total Amount') ? ' has-error' : '' }}">
                                    {!! Form::label('total_amount', trans('Total Amount *')) !!}
                                    {!! Form::text('total_amount', old('total_amount',$data->total_amount), ['class' => 'form-control', 'placeholder' => trans('Total Amount')]) !!}
                                    {!! $errors->first('total_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Amount Collected*') ? ' has-error' : '' }}">
                                    {!! Form::label('amount_collected', trans('Amount Collected *')) !!}
                                    {!! Form::text('amount_collected', old('amount_collected',$data->amount_collected), ['class' => 'form-control', 'placeholder' => trans('Amount Collected')]) !!}
                                    {!! $errors->first('amount_collected', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur1 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur1_amount', trans('Sur1 Amount ')) !!}
                                    {!! Form::text('sur1_amount', old('sur1_amount',$data->sur1_amount), ['class' => 'form-control', 'placeholder' => trans('Sur1 Amount')]) !!}
                                    {!! $errors->first('sur1_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Sur2 Amount*') ? ' has-error' : '' }}">
                                    {!! Form::label('sur2_amount', trans('Sur2 Amount ')) !!}
                                    {!! Form::text('sur2_amount', old('sur2_amount',$data->sur2_amount), ['class' => 'form-control', 'placeholder' => trans('Sur2 Amount')]) !!}
                                    {!! $errors->first('sur2_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                          
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('aud_name', trans('Auditor Name *')) !!}
                                    {!! Form::text('aud_name', old('aud_name',$data->aud_name), ['class' => 'form-control', 'placeholder' => trans('Auditor Name')]) !!}
                                    {!! $errors->first('aud_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1_date', trans('Stage1 From Date *')) !!}
                                    {!! Form::date('stage1_date', old('stage1_date',$data->stage1_date), ['class' => 'form-control','placeholder' => trans('Stage1 Date ')]) !!}
                                    {!! $errors->first('stage1_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage1to_date', trans('Stage1 To Date *')) !!}
                                    {!! Form::date('stage1to_date', old('stage1to_date',$data->stage1to_date), ['class' => 'form-control', 'placeholder' => trans('Stage1 To Date ')]) !!}
                                    {!! $errors->first('stage1to_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2_date', trans('Stage2 From Date *')) !!}
                                    {!! Form::date('stage2_date', old('stage2_date',$data->stage2_date), ['class' => 'form-control', 'placeholder' => trans('Stage2 Date ')]) !!}
                                    {!! $errors->first('stage2_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Validity from*') ? ' has-error' : '' }}">
                                    {!! Form::label('stage2to_date', trans('Stage2 To Date *')) !!}
                                    {!! Form::date('stage2to_date', old('stage2to_date',$data->stage2to_date), ['class' => 'form-control', 'placeholder' => trans('Stage2 To Date')]) !!}
                                    {!! $errors->first('stage2to_date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                        <input type="button" style="font-weight:bold;font-size:17px;text-align:center;"value=" Auditor And Standard History" class="btn  btn-success btn-block" onClick="showHideDiv('divMsg')"/>
<br/>


                    <div class="row">
                               
                            <div class="col-sm-2">
                                <div class="form-group{{ $errors->has('Accreditation') ? ' has-error' : '' }}">
                                    {!! Form::label('year1', trans('Choose Year')) !!}
                                     <select class="form-control"  name="year1" >
                                        <option value="">--- Choose Year ---</option>
                                        <option value="2015" <?php if($data->year1 == 2015) { echo "selected";} ?>>2015</option>
                                        <option value="2016" <?php if($data->year1 == 2016) { echo "selected";}?>>2016</option>
                                        <option value="2017" <?php if($data->year1 == 2017) { echo "selected";}?>>2017</option>
                                        <option value="2018" <?php if($data->year1 == 2018) { echo "selected";}?>>2018</option>
                                        <option value="2019" <?php if($data->year1 == 2019) { echo "selected";}?>>2019</option>
                                        <option value="2020" <?php if($data->year1 == 2020) { echo "selected";}?>>2020</option>
                                        <option value="2021" <?php if($data->year1 == 2021) { echo "selected";}?>>2021</option>
                                        <option value="2022" <?php if($data->year1 == 2022) { echo "selected";}?>>2022</option>

                                      
                                        
                                    </select>
                                    {!! $errors->first('year1', '<span class="help-block">:message</span>') !!}
                                </div>
                          </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor1', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor1', old('auditor1',$data->auditor1), ['class' => 'form-control', 'placeholder' => trans('Auditor Name')]) !!}
                                    {!! $errors->first('auditor1', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name1') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name1', trans('Select Standard ')) !!}
                                    <select  name="standard_name1[]" class=" form-control "   id="standard_name1" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name1); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name1', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            </div>
                    
                    <div class="row">
                             <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year2', trans('Choose Year')) !!}
                                        <select class="form-control" name="year2" >
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year2 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year2 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year2 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year2 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year2 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year2 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year2 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year2 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('year2', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor2', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor2', old('auditor2',$data->auditor2), ['class' => 'form-control', 'placeholder' => trans('Auditor Name')]) !!}
                                    {!! $errors->first('auditor2', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name2') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name2', trans('Select Standard ')) !!}
                                    <select  name="standard_name2[]" class=" form-control "   id="standard_name2" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name2); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name2', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        
                    </div>


                    <div class="row">
                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year3', trans('Choose Year')) !!}
                                        <select class="form-control" name="year3" >
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year3 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year3 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year3 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year3 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year3 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year3 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year3 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year3 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('year3', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor3', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor3', old('auditor3',$data->auditor3), ['class' => 'form-control', 'placeholder' => trans('Auditor Name')]) !!}
                                    {!! $errors->first('auditor3', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name3') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name3', trans('Select Standard ')) !!}
                                    <select  name="standard_name3[]" class=" form-control "   id="standard_name3" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name3); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name3', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                    </div>
                        
                    <div class="row">
                            <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('Year *') ? ' has-error' : '' }}">
                                        {!! Form::label('year4', trans('Choose Year')) !!}
                                        <select class="form-control" name="year4" >
                                            <option value="">Choose Year</option>
                                            <option value="2015" <?php if($data->year4 == "2015") echo "selected"; ?>>2015</option>
                                            <option value="2016" <?php if($data->year4 == "2016") echo "selected"; ?>>2016</option>
                                            <option value="2017" <?php if($data->year4 == "2017") echo "selected"; ?>>2017</option>
                                            <option value="2018" <?php if($data->year4 == "2018") echo "selected"; ?>>2018</option>
                                            <option value="2019" <?php if($data->year4 == "2019") echo "selected"; ?>>2019</option>
                                            <option value="2020" <?php if($data->year4 == "2020") echo "selected"; ?>>2020</option>
                                            <option value="2021" <?php if($data->year4 == "2021") echo "selected"; ?>>2021</option>
                                            <option value="2022" <?php if($data->year4 == "2022") echo "selected"; ?>>2022</option>
                                        </select>
                                        {!! $errors->first('year4', '<span class="help-block">:message</span>') !!}
                                    </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Select Auditor') ? ' has-error' : '' }}">
                                    {!! Form::label('auditor4', trans('Select Auditor ')) !!}
                                    {!! Form::text('auditor4', old('auditor4',$data->auditor4), ['class' => 'form-control', 'placeholder' => trans('Auditor Name')]) !!}
                                    {!! $errors->first('auditor4', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                           
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('standard_name4') ? ' has-error' : '' }}">
                                    {!! Form::label('standard_name4', trans('Select Standard ')) !!}
                                    <select  name="standard_name4[]" class=" form-control "   id="standard_name4" multiple>
                                        <!-- <option value="">--- Select standard ---</option> -->
                                       
                                          
                                        <?php $datasc =  json_decode($data->standard_name4); foreach ($standards as $value): ?>
                                            <option value="{{ $value->id }}" <?php if(isset($datasc)) { foreach($datasc as $datan) { if($value->id == $datan) { echo "selected";} } } ?> >{{ $value->name }}</option>
                                        <?php endforeach; ?>


                                    </select>
                                    {!! $errors->first('standard_name4', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                </div>

                                
                         <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('repo') ? ' has-error' : '' }}">
                                    {!! Form::label('repo', trans('Upload Audit Report *')) !!}
                                    {!! Form::file('repo', old('repo'), ['class' => 'form-control','required', 'placeholder' => trans('Upload Report')]) !!}
                                    <!-- {!! Form::label('repo', old('repo',$data->reportname), ['class' => 'form-control', 'placeholder' => trans('Report Name') ,'readonly']) !!} -->
                                    <br/><br/>
                                    
                                    {!! Form::label('repo', trans('Uploaded Audit Report:')) !!}
                                    <span style="color:green">
                                    {!! Form::label( old('repo',$data->reportname)) !!}
                                    </span>
                                    {!! $errors->first('repo', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('certi') ? ' has-error' : '' }}">
                                    {!! Form::label('certi', trans('Upload Certificate *')) !!}
                                    {!! Form::file('certi', old('certi'), ['class' => 'form-control','required', 'placeholder' => trans('Upload Certificate')]) !!}
                                    <!-- {!! Form::label('certi', old('certi',$data->certificatename), ['class' => 'form-control', 'placeholder' => trans('Certificate Name'),'readonly']) !!} -->
                                    <br/><br/>
                                    {!! Form::label('repo', trans('Uploaded Certificate:')) !!}
                                    <span style="color:green">
                                    {!! Form::label( old('repo',$data->certificatename)) !!}
                                  </span>
                                    {!! $errors->first('certi', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            


                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('Comments') ? ' has-error' : '' }}">
                                    {!! Form::label('comments', trans(' Comments ')) !!}
                                    {!! Form::textarea('comments', old('comments',$data->comments), ['class' => 'form-control', 'placeholder' => trans('comments')]) !!}
                                    {!! $errors->first('comments', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('Id') ? ' has-error' : '' }} " hidden >
                                 {!! Form::label('id', trans('Company Name *')) !!}
                                  {!! Form::text('id', old('id',$data->id), ['class' => 'form-control','required', 'placeholder' => trans('company name *')]) !!}
                                  {!! $errors->first('id', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        </div>

                            

                        </div>
                    </div>
                </div>  
                
            @endif