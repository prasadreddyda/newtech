@extends('layouts.master')

<style>
        html {
            user-select: none;
        }

        .fa-check-square {
        color: green;
        }

        .fa-times {
  color: red;
  
}
</style>


@section('content-header')
    <h1>
        {{ trans('Scheme Data') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('scheme data') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">

                <div class="col-xs-12">
                    <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                        {!! Form::open(['route' => ['admin.scheme.data.search'], 'method' => 'post']) !!}
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('from') ? ' has-error' : '' }}">
                                        <!-- {!! Form::label('from', trans('Select From Date *')) !!} -->
                                        <input type="text" name="from" class="form-control", placeholder="Valid From Year" style="border-color:green; border-width: 2px;">
                                        {!! $errors->first('from', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('from_month') ? ' has-error' : '' }}">
                                        <select class="form-control" name="from_month" style="border-color:green; border-width: 2px;">
                                            <option value="">Select Month</option>
                                            <option value="1">Jan</option>
                                            <option value="2">Feb</option>
                                            <option value="3">Mar</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">Jun</option>
                                            <option value="7">July</option>
                                            <option value="8">Aug</option>
                                            <option value="9">Sep</option>
                                            <option value="10">Oct</option>
                                            <option value="11">Nov</option>
                                            <option value="12">Dec</option>
                                        </select>
                                        {!! $errors->first('from_month', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                                        <!-- {!! Form::label('from', trans('Select To Date *')) !!} -->
                                        <input type="text" name="to" class="form-control", placeholder="Valid To Year" style="border-color:green; border-width: 2px;">
                                        {!! $errors->first('from', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('to_month') ? ' has-error' : '' }}">
                                        <select class="form-control" name="to_month" style="border-color:green; border-width: 2px;">
                                            <option value="">Select Month</option>
                                            <option value="1">Jan</option>
                                            <option value="2">Feb</option>
                                            <option value="3">Mar</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">Jun</option>
                                            <option value="7">July</option>
                                            <option value="8">Aug</option>
                                            <option value="9">Sep</option>
                                            <option value="10">Oct</option>
                                            <option value="11">Nov</option>
                                            <option value="12">Dec</option>
                                        </select>
                                        {!! $errors->first('to_month', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <!-- <div class="col-sm-2">
                                    <div class="form-group{{ $errors->has('accrediation') ? ' has-error' : '' }}">
                                        <select class="form-control" name="accrediation" style="border-color:green; border-width: 2px;">
                                            <option value="">Select Accreditation</option>
                                            <option value="1">EIAC</option>
                                            <option value="2">IAS</option>
                                            <option value="3">ASCB[E]</option>
                                            <option value="4">JAS-ANZ</option>
                                           <option value="5">GAC</option>
                                        </select>
                                        {!! $errors->first('accrediation', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div> -->
                               
                                <div class="col-sm-2">
                                    <button type="submit" name="submit" class="btn btn-success btn-primary"><i class="fa fa-search "></i> {{ trans('Search') }}</button>
                                </div>
                                @if(($role == "admin" ) || ($role == "schemeadm"))
                                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                                   
                                </div>
                                @else
                                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                                <a href="{{ route('admin.scheme.data.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                                        <i class="fa fa-pencil"></i> {{ trans('Create Scheme Data') }}
                                    </a>
                                 </div>
                                @endif
                        {!! Form::close() !!}
                    </div>
                </div>
               
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                        <thead  style="background-color: #47915c; color: white;">
                            <tr>
                                 <th>S.No.</th>
                                <th>Company Name</th>
                                <th> Address</th>
                                <th>Certificate</th>
                                <th>Standard</th>
                                
                                <th>Valid From</th>
                                <th>Valid To</th>
                                <th>Accreditation</th>
                               
                                @if(($role == "admin" ) || ($role == "schemeadm"))
                                <th>Executive </th>
                                @endif
                                <!-- <th>Status</th> -->
                                @if(($role == "admin" ) || ($role == "schemeadm"))
                                <th>Finish</th>
                                @endif
                                <th>Status</th>

                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                           
                           

                            <?php if (isset($datas)): ?>
                                <?php $i = 1; ?>
                            <?php foreach ($datas as $data): ?>
                              
                            <tr style="font-size:13px;font-weight:bold;background-color:#e1ecd8;">
                            <td> {{ $i }}</td>
                            <td> {{ $data->comp_name}}</td>
                            <td> {{ $data->comp_address}}</td>
                            <td> {{ $data->certificate}}</td>


                            <td>
                                    @php $standard_names = json_decode($data->standard_name); @endphp
                                    @if(isset($standard_names))
                                        @foreach($standard_names as $standard)
                                            @php $standard = DB::table('settings__standards')->where('id', $standard)->first(); @endphp
                                            {{ $standard->name  }} <br>
                                        @endforeach
                                    @endif
                                    <!-- @php $standards = DB::table('settings__standards')-> -->
                                </td>

                              <td>  {{ $data->validity_from }} </td>
                              <td>  {{ ($data->validity_to) }} </td>
                          
                             
                            <!-- <td> {{ $data->aud_name}}</td> -->
                            <td>
                                 @if($data->accrediation == 1) 
                                        <b>EIAC</b>
                                    @elseif($data->accrediation == 2)
                                        <b>IAS</b>
                                    @elseif($data->accrediation == 3)
                                        <b>ASCB[E]</b>
                                    @elseif($data->accrediation == 4)
                                        <b>JAS-ANZ</b>
                                    @else
                                        <b>GAC</b>
                                    @endif
                            </td>

                            @if(($role == "admin" ) || ($role == "schemeadm"))
                                    <td>
                                        {{ $data->user->first_name}}
                                    </td>
                            @endif


                            @if(($role == "admin" ) || ($role == "schemeadm"))
                            <td>
                              
                                 @if($data->comp_address == NULL || $data->certificate == NULL || $data->standard_name == NULL || $data->validity_from == NULL || $data->validity_to == NULL || $data->total_amount == NULL || $data->amount_collected == NULL || $data->aud_name == NULL || $data->stage1_date == NULL || $data->stage1to_date == NULL || $data->stage2_date == NULL || $data->stage2to_date == NULL || $data->reportdoc == NULL || $data->certificatedoc == NULL || $data->email_id == NULL)

                                 <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                                 @elseif($data->comp_address == "NIL" || $data->certificate == "NIL" || $data->standard_name == "NIL" || $data->validity_from == "NIL" || $data->validity_to == "NIL" || $data->total_amount == "NIL" || $data->amount_collected == "NIL" || $data->aud_name == "NIL" || $data->stage1_date == "NIL" || $data->stage1to_date == "NIL" || $data->stage2_date == "NIL" || $data->stage2to_date == "NIL" || $data->reportdoc == "NIL" || $data->certificatedoc == "NIL" || $data->email_id == "NIL")
                                 <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                                 @elseif($data->comp_address == "0" || $data->certificate == "0" || $data->standard_name == "0" || $data->validity_from == "0" || $data->validity_to == "0" || $data->total_amount == "0" || $data->amount_collected == "0" || $data->aud_name == "0" || $data->stage1_date == "0" || $data->stage1to_date == "0" || $data->stage2_date == "0" || $data->stage2to_date == "0" || $data->reportdoc == "0" || $data->certificatedoc == "0" || $data->email_id == "0" )
                                 <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                                 @else
                                 <i class="fa fa-check-square fa-2x" aria-hidden="true"></i>
                                   
                                @endif
                            </td>
                            @endif
                                <!-- {{ $data->aud_name}} -->
                                <!--   <table border="1">
                                    <tr>
                                        <th>
                                            Year
                                        </th>
                                        <th>
                                            Auditor
                                        </th>
                                    </tr>
                                    @if(isset($data->year1))
                                    <tr>
                                        <td>
                                            {{ $data->year1 }}
                                        </td>
                                        <td>
                                        
                                        </td>
                                    </tr>

                                    @endif

                                    @if(isset($data->year2))
                                    <tr>
                                        <td>
                                            {{ $data->year2 }}
                                        </td>
                                        <td>
                                         
                                        </td>
                                    </tr>

                                    @endif
                                    @if(isset($data->year3))
                                    <tr>
                                        <td>
                                            {{ $data->year3 }}
                                        </td>
                                        <td>
                                          
                                        </td>
                                    </tr>

                                    @endif
                                    @if(isset($data->year4))
                                    <tr>
                                        <td>
                                            {{ $data->year4 }}
                                        </td>
                                        <td>
                                          
                                        </td>
                                    </tr>

                                    @endif
                                </table>  -->
                            <!-- </td> -->
                            
                            <!-- <td>   
                              
                      
                                @if(isset($data->user->first_name))
                                    {{ $data->user->first_name }}
                                @else 
                                   {{ $data->user['first_name'] }}
                                @endif
                            </td> -->
                            <td>
                                   @if($data->status == 0) 
                                        <a href="" class="btn btn-warning btn-flat">In Progress</a>
                                    @elseif($data->status == 1)
                                        <a href="" class="btn btn-success btn-flat">Signed</a>
                                    @elseif($data->status == 2)
                                        <a href="" class="btn btn-danger btn-flat">Cancelled</a>
                                    @else
                                        <a href="" class="btn btn-info btn-flat">Suspended</a>
                                    @endif
                                </td>
                            <!-- <td>
                                   @if($data->status == 0) 
                                        <a href="#" class="btn btn-warning btn-flat">In Progress</a>
                                    @elseif($data->status == 1)
                                        <a href="#" class="btn btn-success btn-flat">Signed</a>
                                    @elseif($data->status == 2)
                                        <a href="#" class="btn btn-danger btn-flat">Cancelled</a>
                                    @else

                                    @endif
                                </td> -->
                                <td>
                                @if(($role == "admin" ) )
                                    <div class="btn-group">
                                         <a href="{{ route('admin.scheme.data.edit', [$data->id]) }}?viewdata={{ $data->id}}" class="btn btn-success btn-primary"><i class="fa fa-eye"></i> View Data</a>
                                        <!-- <a href="{{ route('admin.scheme.data.edit', [$data->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.scheme.data.destroy', [$data->id]) }}"><i class="fa fa-trash"></i></button> -->
                                    </div>
                                @elseif( ($role == "schemeadm"))
                                    <div class="btn-group">
                                         <a href="{{ route('admin.scheme.data.edit', [$data->id]) }}?viewdata={{ $data->id}}" class="btn btn-success btn-primary"><i class="fa fa-eye"></i> View Data</a>
                                        <a href="{{ route('admin.scheme.data.edit', [$data->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <!-- <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.scheme.data.destroy', [$data->id]) }}"><i class="fa fa-trash"></i></button> -->
                                    </div>
                                @else
                                    <div class="btn-group">
                                         <a href="{{ route('admin.scheme.data.edit', [$data->id]) }}?viewdata={{ $data->id}}" class="btn btn-success btn-primary"><i class="fa fa-eye"></i> View Data</a>
                                        <a href="{{ route('admin.scheme.data.edit', [$data->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.scheme.data.destroy', [$data->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                @endif
                                </td>
                            </tr>
                            <?php $i++; ?>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <!-- <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr> -->
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('scheme::data.title.create data') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.scheme.data.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": false,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>

    

<script>
document.addEventListener("contextmenu", function(event){
event.preventDefault();
alert('Access Denied');    
}, false);
</script>

<script type="text/javascript">
$(document).on('keydown', function(e) { 
    if((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
        alert("Printing is prohibited");
        e.cancelBubble = true;
        e.preventDefault();

        e.stopImmediatePropagation();
    }  
});
</script>
@endpush
