@extends('layouts.master')
<link rel="stylesheet" href="{{URL::asset('css/select2.min.css')}}" />
<style>
        html {
            user-select: none;
        }
</style>
@section('content-header')
    <h1>
        {{ trans('scheme::data.title.edit data') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.scheme.data.index') }}">{{ trans('scheme::data.title.data') }}</a></li>
        <li class="active">{{ trans('scheme::data.title.edit data') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.scheme.data.update', $data->id], 'method' => 'put','files' => 'true']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                @if ($errors->any())
                        <div class="alert alert-danger">
                                  <ul>
                                       @foreach ($errors->all() as $error)
                                     <li>{{ $error }}</li>
                                         @endforeach
                                  </ul>
                        </div>
                @endif
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('scheme::admin.data.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach
                    @if($type == "viewdata" )
                    
                    <div class="box-footer">
                        <!-- <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button> -->
                        <a class="btn btn-success  btn-flat" href="{{ route('admin.scheme.data.index')}}"><i class="fa fa-times"></i> {{ trans('Close') }}</a>
                    </div>
                    @elseif($type == "viewdata1" )
                    <div class="box-footer">
                    @if(($role == "admin" ) )
                        <!-- <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button> -->
                        <a class="btn btn-success  btn-flat" href="{{ route('admin.scheme.data.index')}}"><i class="fa fa-times"></i> {{ trans('Close') }}</a>

                        <!-- <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.surveillance.surdata.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a> -->
                    @else
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.surveillance.surdata.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>

                        <!-- <a class="btn btn-success  btn-flat" href="{{ route('admin.surveillance.surdata.index')}}"><i class="fa fa-times"></i> {{ trans('Close') }}</a> -->
                    </div>
                    @endif
                    @else
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.scheme.data.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                    @endif
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{URL::asset('js/select2.min.js')}}"></script>

    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.scheme.data.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
    <script type="text/javascript">
        $("#standard_name").select2( {
            placeholder: "Select Standard *",
            allowClear: true
            } );
    </script>

<script type="text/javascript">
        $("#standard_name1").select2( {
            placeholder: "Select Standard *",
            allowClear: true
            } );
    </script>

<script type="text/javascript">
        $("#standard_name2").select2( {
            placeholder: "Select Standard *",
            allowClear: true
            } );
    </script>

<script type="text/javascript">
        $("#standard_name3").select2( {
            placeholder: "Select Standard *",
            allowClear: true
            } );
    </script>

<script type="text/javascript">
        $("#standard_name4").select2( {
            placeholder: "Select Standard *",
            allowClear: true
            } );
    </script>
  <script type="text/javascript">
        $("#aud_name").select2( {
            placeholder: "Select Auditor *",
            allowClear: true
            } );
    </script>
     <script type="text/javascript">
        $("#auditor1").select2( {
            placeholder: "Select Auditor *",
            allowClear: true
            } );
    </script>
    <script type="text/javascript">
        $("#auditor2").select2( {
            placeholder: "Select Auditor *",
            allowClear: true
            } );
    </script>
    <script type="text/javascript">
        $("#auditor3").select2( {
            placeholder: "Select Auditor *",
            allowClear: true
            } );
    </script>
    <script type="text/javascript">
        $("#auditor4").select2( {
            placeholder: "Select Auditor *",
            allowClear: true
            } );
    </script>

<script type="text/javascript">
$(document).on('keydown', function(e) { 
    if((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
        alert("Printing is prohibited");
        e.cancelBubble = true;
        e.preventDefault();

        e.stopImmediatePropagation();
    }  
});
</script>

<script>
document.addEventListener("contextmenu", function(event){
event.preventDefault();
alert('Access Denied');    
}, false);
</script>
@endpush
