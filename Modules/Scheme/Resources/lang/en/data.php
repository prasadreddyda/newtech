<?php

return [
    'list resource' => 'List data',
    'create resource' => 'Create data',
    'edit resource' => 'Edit data',
    'destroy resource' => 'Destroy data',
    'title' => [
        'data' => 'Data',
        'create data' => 'Create a data',
        'edit data' => 'Edit a data',
    ],
    'button' => [
        'create data' => 'Create a data',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
