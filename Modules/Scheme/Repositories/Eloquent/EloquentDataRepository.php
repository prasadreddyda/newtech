<?php

namespace Modules\Scheme\Repositories\Eloquent;

use Modules\Scheme\Repositories\DataRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDataRepository extends EloquentBaseRepository implements DataRepository
{
}
