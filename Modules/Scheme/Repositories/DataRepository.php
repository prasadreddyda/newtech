<?php

namespace Modules\Scheme\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface DataRepository extends BaseRepository
{
}
