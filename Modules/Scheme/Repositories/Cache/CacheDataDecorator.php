<?php

namespace Modules\Scheme\Repositories\Cache;

use Modules\Scheme\Repositories\DataRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDataDecorator extends BaseCacheDecorator implements DataRepository
{
    public function __construct(DataRepository $data)
    {
        parent::__construct();
        $this->entityName = 'scheme.data';
        $this->repository = $data;
    }
}
