<?php

namespace Modules\Scheme\Entities;

use Illuminate\Database\Eloquent\Model;

class DataTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'scheme__data_translations';
}
