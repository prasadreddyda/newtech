<?php

namespace Modules\Scheme\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    use Translatable;

    protected $table = 'scheme__data';
    public $translatedAttributes = [];
    protected $fillable = [
        'standard_name','comp_name','comp_address','certificate','user_id','accrediation','validity_from','validity_to','total_amount','amount_collected','sur1_amount','sur2_amount','aud_name','stage1_date','stage2_date','comments','year1','year2','year3','year4','auditor1','auditor2','auditor3','auditor4','status','mobile','landline','sur_company','reportdoc','certificatedoc','reportname','certificatename','standard_name1','standard_name2','standard_name3','standard_name4','stage1to_date','stage2to_date','status1','email_id'
    ];
    public function standard()
{
    return $this->belongsTo("Modules\Settings\Entities\Standards","standard_name");
}
public function user()
{
    return $this->belongsTo("Modules\User\Entities\Sentinel\User","user_id");
}
    
}
