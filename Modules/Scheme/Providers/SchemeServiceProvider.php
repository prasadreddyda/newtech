<?php

namespace Modules\Scheme\Providers;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Scheme\Listeners\RegisterSchemeSidebar;
use Illuminate\Support\Arr;

class SchemeServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterSchemeSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('data', Arr::dot(trans('scheme::data')));
            // append translations

        });


    }

    public function boot()
    {
        $this->publishConfig('scheme', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Scheme\Repositories\DataRepository',
            function () {
                $repository = new \Modules\Scheme\Repositories\Eloquent\EloquentDataRepository(new \Modules\Scheme\Entities\Data());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Scheme\Repositories\Cache\CacheDataDecorator($repository);
            }
        );
// add bindings

    }


}
