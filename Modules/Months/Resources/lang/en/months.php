<?php

return [
    'list resource' => 'List months',
    'create resource' => 'Create months',
    'edit resource' => 'Edit months',
    'destroy resource' => 'Destroy months',
    'title' => [
        'months' => 'Months',
        'create months' => 'Create a months',
        'edit months' => 'Edit a months',
    ],
    'button' => [
        'create months' => 'Create a months',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
