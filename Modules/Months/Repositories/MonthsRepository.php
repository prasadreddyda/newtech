<?php

namespace Modules\Months\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface MonthsRepository extends BaseRepository
{
}
