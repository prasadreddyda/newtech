<?php

namespace Modules\Months\Repositories\Cache;

use Modules\Months\Repositories\MonthsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheMonthsDecorator extends BaseCacheDecorator implements MonthsRepository
{
    public function __construct(MonthsRepository $months)
    {
        parent::__construct();
        $this->entityName = 'months.months';
        $this->repository = $months;
    }
}
