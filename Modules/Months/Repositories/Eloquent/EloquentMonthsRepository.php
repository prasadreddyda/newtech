<?php

namespace Modules\Months\Repositories\Eloquent;

use Modules\Months\Repositories\MonthsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentMonthsRepository extends EloquentBaseRepository implements MonthsRepository
{
}
