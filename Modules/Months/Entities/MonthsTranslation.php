<?php

namespace Modules\Months\Entities;

use Illuminate\Database\Eloquent\Model;

class MonthsTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'months__months_translations';
}
