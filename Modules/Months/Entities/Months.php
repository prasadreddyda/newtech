<?php

namespace Modules\Months\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Months extends Model
{
    use Translatable;

    protected $table = 'months__months';
    public $translatedAttributes = [];
    protected $fillable = ['months'];
}
