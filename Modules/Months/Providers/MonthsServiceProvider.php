<?php

namespace Modules\Months\Providers;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Months\Listeners\RegisterMonthsSidebar;
use Illuminate\Support\Arr;

class MonthsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterMonthsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('months', Arr::dot(trans('months::months')));
            // append translations

        });


    }

    public function boot()
    {
        $this->publishConfig('months', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Months\Repositories\MonthsRepository',
            function () {
                $repository = new \Modules\Months\Repositories\Eloquent\EloquentMonthsRepository(new \Modules\Months\Entities\Months());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Months\Repositories\Cache\CacheMonthsDecorator($repository);
            }
        );
// add bindings

    }


}
