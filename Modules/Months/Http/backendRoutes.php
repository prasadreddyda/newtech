<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/months'], function (Router $router) {
    $router->bind('months', function ($id) {
        return app('Modules\Months\Repositories\MonthsRepository')->find($id);
    });
    $router->get('months', [
        'as' => 'admin.months.months.index',
        'uses' => 'MonthsController@index',
        'middleware' => 'can:months.months.index'
    ]);
    $router->get('months/create', [
        'as' => 'admin.months.months.create',
        'uses' => 'MonthsController@create',
        'middleware' => 'can:months.months.create'
    ]);
    $router->post('months', [
        'as' => 'admin.months.months.store',
        'uses' => 'MonthsController@store',
        'middleware' => 'can:months.months.create'
    ]);
    $router->get('months/{months}/edit', [
        'as' => 'admin.months.months.edit',
        'uses' => 'MonthsController@edit',
        'middleware' => 'can:months.months.edit'
    ]);
    $router->put('months/{months}', [
        'as' => 'admin.months.months.update',
        'uses' => 'MonthsController@update',
        'middleware' => 'can:months.months.edit'
    ]);
    $router->delete('months/{months}', [
        'as' => 'admin.months.months.destroy',
        'uses' => 'MonthsController@destroy',
        'middleware' => 'can:months.months.destroy'
    ]);
// append

});
