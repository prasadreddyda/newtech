<?php

namespace Modules\Months\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Months\Entities\Months;
use Modules\Months\Http\Requests\CreateMonthsRequest;
use Modules\Months\Http\Requests\UpdateMonthsRequest;
use Modules\Months\Repositories\MonthsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class MonthsController extends AdminBaseController
{
    /**
     * @var MonthsRepository
     */
    private $months;

    public function __construct(MonthsRepository $months)
    {
        parent::__construct();

        $this->months = $months;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $months = $this->months->all();

        return view('months::admin.months.index', compact('months'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('months::admin.months.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateMonthsRequest $request
     * @return Response
     */
    public function store(CreateMonthsRequest $request)
    {
        $this->months->create($request->all());

        return redirect()->route('admin.months.months.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('months::months.title.months')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Months $months
     * @return Response
     */
    public function edit(Months $months)
    {
        return view('months::admin.months.edit', compact('months'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Months $months
     * @param  UpdateMonthsRequest $request
     * @return Response
     */
    public function update(Months $months, UpdateMonthsRequest $request)
    {
        $this->months->update($months, $request->all());

        return redirect()->route('admin.months.months.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('months::months.title.months')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Months $months
     * @return Response
     */
    public function destroy(Months $months)
    {
        $this->months->destroy($months);

        return redirect()->route('admin.months.months.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('months::months.title.months')]));
    }
}
