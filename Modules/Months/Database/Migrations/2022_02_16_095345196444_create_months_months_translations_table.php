<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthsMonthsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('months__months_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('months_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['months_id', 'locale']);
            $table->foreign('months_id')->references('id')->on('months__months')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('months__months_translations', function (Blueprint $table) {
            $table->dropForeign(['months_id']);
        });
        Schema::dropIfExists('months__months_translations');
    }
}
