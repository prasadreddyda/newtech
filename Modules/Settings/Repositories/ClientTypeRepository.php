<?php

namespace Modules\Settings\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface ClientTypeRepository extends BaseRepository
{
}
