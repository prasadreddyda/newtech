<?php

namespace Modules\Settings\Repositories\Cache;

use Modules\Settings\Repositories\LocationsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheLocationsDecorator extends BaseCacheDecorator implements LocationsRepository
{
    public function __construct(LocationsRepository $locations)
    {
        parent::__construct();
        $this->entityName = 'settings.locations';
        $this->repository = $locations;
    }
}
