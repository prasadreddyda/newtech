<?php

namespace Modules\Settings\Repositories\Cache;

use Modules\Settings\Repositories\NaceRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheNaceDecorator extends BaseCacheDecorator implements NaceRepository
{
    public function __construct(NaceRepository $nace)
    {
        parent::__construct();
        $this->entityName = 'settings.naces';
        $this->repository = $nace;
    }
}
