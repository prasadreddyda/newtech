<?php

namespace Modules\Settings\Repositories\Cache;

use Modules\Settings\Repositories\CountriesRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCountriesDecorator extends BaseCacheDecorator implements CountriesRepository
{
    public function __construct(CountriesRepository $countries)
    {
        parent::__construct();
        $this->entityName = 'settings.countries';
        $this->repository = $countries;
    }
}
