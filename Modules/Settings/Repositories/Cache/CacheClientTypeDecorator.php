<?php

namespace Modules\Settings\Repositories\Cache;

use Modules\Settings\Repositories\ClientTypeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheClientTypeDecorator extends BaseCacheDecorator implements ClientTypeRepository
{
    public function __construct(ClientTypeRepository $clienttype)
    {
        parent::__construct();
        $this->entityName = 'settings.clienttypes';
        $this->repository = $clienttype;
    }
}
