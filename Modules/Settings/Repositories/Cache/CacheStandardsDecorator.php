<?php

namespace Modules\Settings\Repositories\Cache;

use Modules\Settings\Repositories\StandardsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheStandardsDecorator extends BaseCacheDecorator implements StandardsRepository
{
    public function __construct(StandardsRepository $standards)
    {
        parent::__construct();
        $this->entityName = 'settings.standards';
        $this->repository = $standards;
    }
}
