<?php

namespace Modules\Settings\Repositories\Eloquent;

use Modules\Settings\Repositories\NaceRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentNaceRepository extends EloquentBaseRepository implements NaceRepository
{
}
