<?php

namespace Modules\Settings\Repositories\Eloquent;

use Modules\Settings\Repositories\CountriesRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCountriesRepository extends EloquentBaseRepository implements CountriesRepository
{
}
