<?php

namespace Modules\Settings\Repositories\Eloquent;

use Modules\Settings\Repositories\StandardsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentStandardsRepository extends EloquentBaseRepository implements StandardsRepository
{
}
