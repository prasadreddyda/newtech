<?php

namespace Modules\Settings\Repositories\Eloquent;

use Modules\Settings\Repositories\LocationsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentLocationsRepository extends EloquentBaseRepository implements LocationsRepository
{
}
