<?php

namespace Modules\Settings\Repositories\Eloquent;

use Modules\Settings\Repositories\ClientTypeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentClientTypeRepository extends EloquentBaseRepository implements ClientTypeRepository
{
}
