<?php

namespace Modules\Settings\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface LocationsRepository extends BaseRepository
{
}
