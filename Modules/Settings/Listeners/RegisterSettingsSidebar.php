<?php

namespace Modules\Settings\Listeners;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterSettingsSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('Settings'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(3);
                $item->authorize(
                    $this->auth->hasAccess('settings.standards.index')
                );
                $item->item(trans('settings::standards.title.standards'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.settings.standards.create');
                    $item->route('admin.settings.standards.index');
                    $item->authorize(
                        $this->auth->hasAccess('settings.standards.index')
                    );
                });
                // $item->item(trans('settings::naces.title.naces'), function (Item $item) {
                //     $item->icon('fa fa-copy');
                //     $item->weight(0);
                //     $item->append('admin.settings.nace.create');
                //     $item->route('admin.settings.nace.index');
                //     $item->authorize(
                //         $this->auth->hasAccess('settings.naces.index')
                //     );
                // });
                $item->item(trans('settings::clienttypes.title.clienttypes'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.settings.clienttype.create');
                    $item->route('admin.settings.clienttype.index');
                    $item->authorize(
                        $this->auth->hasAccess('settings.clienttypes.index')
                    );
                });
                $item->item(trans('settings::countries.title.countries'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.settings.countries.create');
                    $item->route('admin.settings.countries.index');
                    $item->authorize(
                        $this->auth->hasAccess('settings.countries.index')
                    );
                });
                $item->item(trans('settings::locations.title.locations'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.settings.locations.create');
                    $item->route('admin.settings.locations.index');
                    $item->authorize(
                        $this->auth->hasAccess('settings.locations.index')
                    );
                });
// append





            });
        });

        return $menu;
    }
}
