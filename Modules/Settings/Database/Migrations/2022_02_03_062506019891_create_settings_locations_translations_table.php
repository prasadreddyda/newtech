<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsLocationsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings__locations_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('locations_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['locations_id', 'locale']);
            $table->foreign('locations_id')->references('id')->on('settings__locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings__locations_translations', function (Blueprint $table) {
            $table->dropForeign(['locations_id']);
        });
        Schema::dropIfExists('settings__locations_translations');
    }
}
