<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsClientTypeTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings__clienttype_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('clienttype_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['clienttype_id', 'locale']);
            $table->foreign('clienttype_id')->references('id')->on('settings__clienttypes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings__clienttype_translations', function (Blueprint $table) {
            $table->dropForeign(['clienttype_id']);
        });
        Schema::dropIfExists('settings__clienttype_translations');
    }
}
