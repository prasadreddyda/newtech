<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsNaceTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings__nace_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('nace_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['nace_id', 'locale']);
            $table->foreign('nace_id')->references('id')->on('settings__naces')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings__nace_translations', function (Blueprint $table) {
            $table->dropForeign(['nace_id']);
        });
        Schema::dropIfExists('settings__nace_translations');
    }
}
