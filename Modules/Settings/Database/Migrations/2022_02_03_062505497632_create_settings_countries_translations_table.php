<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsCountriesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings__countries_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('countries_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['countries_id', 'locale']);
            $table->foreign('countries_id')->references('id')->on('settings__countries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings__countries_translations', function (Blueprint $table) {
            $table->dropForeign(['countries_id']);
        });
        Schema::dropIfExists('settings__countries_translations');
    }
}
