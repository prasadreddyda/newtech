<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsStandardsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings__standards_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('standards_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['standards_id', 'locale']);
            $table->foreign('standards_id')->references('id')->on('settings__standards')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings__standards_translations', function (Blueprint $table) {
            $table->dropForeign(['standards_id']);
        });
        Schema::dropIfExists('settings__standards_translations');
    }
}
