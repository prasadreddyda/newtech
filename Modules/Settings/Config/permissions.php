<?php

return [
    'settings.standards' => [
        'index' => 'settings::standards.list resource',
        'create' => 'settings::standards.create resource',
        'edit' => 'settings::standards.edit resource',
        'destroy' => 'settings::standards.destroy resource',
    ],
    'settings.naces' => [
        'index' => 'settings::naces.list resource',
        'create' => 'settings::naces.create resource',
        'edit' => 'settings::naces.edit resource',
        'destroy' => 'settings::naces.destroy resource',
    ],
    'settings.clienttypes' => [
        'index' => 'settings::clienttypes.list resource',
        'create' => 'settings::clienttypes.create resource',
        'edit' => 'settings::clienttypes.edit resource',
        'destroy' => 'settings::clienttypes.destroy resource',
    ],
    'settings.countries' => [
        'index' => 'settings::countries.list resource',
        'create' => 'settings::countries.create resource',
        'edit' => 'settings::countries.edit resource',
        'destroy' => 'settings::countries.destroy resource',
    ],
    'settings.locations' => [
        'index' => 'settings::locations.list resource',
        'create' => 'settings::locations.create resource',
        'edit' => 'settings::locations.edit resource',
        'destroy' => 'settings::locations.destroy resource',
    ],
// append





];
