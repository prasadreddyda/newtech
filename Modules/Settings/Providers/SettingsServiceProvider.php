<?php

namespace Modules\Settings\Providers;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Settings\Listeners\RegisterSettingsSidebar;
use Illuminate\Support\Arr;

class SettingsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterSettingsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('standards', Arr::dot(trans('settings::standards')));
            $event->load('naces', Arr::dot(trans('settings::naces')));
            $event->load('clienttypes', Arr::dot(trans('settings::clienttypes')));
            $event->load('countries', Arr::dot(trans('settings::countries')));
            $event->load('locations', Arr::dot(trans('settings::locations')));
            // append translations





        });


    }

    public function boot()
    {
        $this->publishConfig('settings', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Settings\Repositories\StandardsRepository',
            function () {
                $repository = new \Modules\Settings\Repositories\Eloquent\EloquentStandardsRepository(new \Modules\Settings\Entities\Standards());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Settings\Repositories\Cache\CacheStandardsDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Settings\Repositories\NaceRepository',
            function () {
                $repository = new \Modules\Settings\Repositories\Eloquent\EloquentNaceRepository(new \Modules\Settings\Entities\Nace());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Settings\Repositories\Cache\CacheNaceDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Settings\Repositories\ClientTypeRepository',
            function () {
                $repository = new \Modules\Settings\Repositories\Eloquent\EloquentClientTypeRepository(new \Modules\Settings\Entities\ClientType());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Settings\Repositories\Cache\CacheClientTypeDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Settings\Repositories\CountriesRepository',
            function () {
                $repository = new \Modules\Settings\Repositories\Eloquent\EloquentCountriesRepository(new \Modules\Settings\Entities\Countries());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Settings\Repositories\Cache\CacheCountriesDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Settings\Repositories\LocationsRepository',
            function () {
                $repository = new \Modules\Settings\Repositories\Eloquent\EloquentLocationsRepository(new \Modules\Settings\Entities\Locations());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Settings\Repositories\Cache\CacheLocationsDecorator($repository);
            }
        );
// add bindings





    }


}
