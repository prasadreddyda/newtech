<?php

return [
    'list resource' => 'List naces',
    'create resource' => 'Create naces',
    'edit resource' => 'Edit naces',
    'destroy resource' => 'Destroy naces',
    'title' => [
        'naces' => 'Nace',
        'create nace' => 'Create a nace',
        'edit nace' => 'Edit a nace',
    ],
    'button' => [
        'create nace' => 'Create a nace',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
