<?php

return [
    'list resource' => 'List standards',
    'create resource' => 'Create standards',
    'edit resource' => 'Edit standards',
    'destroy resource' => 'Destroy standards',
    'title' => [
        'standards' => 'Standards',
        'create standards' => 'Create a standards',
        'edit standards' => 'Edit a standards',
    ],
    'button' => [
        'create standards' => 'Create a standards',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
