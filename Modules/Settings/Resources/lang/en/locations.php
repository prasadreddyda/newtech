<?php

return [
    'list resource' => 'List locations',
    'create resource' => 'Create locations',
    'edit resource' => 'Edit locations',
    'destroy resource' => 'Destroy locations',
    'title' => [
        'locations' => 'Locations',
        'create locations' => 'Create a locations',
        'edit locations' => 'Edit a locations',
    ],
    'button' => [
        'create locations' => 'Create a locations',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
