<?php

return [
    'list resource' => 'List clienttypes',
    'create resource' => 'Create clienttypes',
    'edit resource' => 'Edit clienttypes',
    'destroy resource' => 'Destroy clienttypes',
    'title' => [
        'clienttypes' => 'ClientType',
        'create clienttype' => 'Create a clienttype',
        'edit clienttype' => 'Edit a clienttype',
    ],
    'button' => [
        'create clienttype' => 'Create a clienttype',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
