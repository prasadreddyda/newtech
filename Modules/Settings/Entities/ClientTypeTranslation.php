<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientTypeTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'settings__clienttype_translations';
}
