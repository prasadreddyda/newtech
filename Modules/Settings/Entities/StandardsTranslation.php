<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class StandardsTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'settings__standards_translations';
}
