<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class LocationsTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'settings__locations_translations';
}
