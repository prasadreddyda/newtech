<?php

namespace Modules\Settings\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Standards extends Model
{
    use Translatable;

    protected $table = 'settings__standards';
    public $translatedAttributes = [];
    protected $fillable = [
        'name'
    ];
}
