<?php

namespace Modules\Settings\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ClientType extends Model
{
    use Translatable;

    protected $table = 'settings__clienttypes';
    public $translatedAttributes = [];
    protected $fillable = [
        'name'
    ];
}
