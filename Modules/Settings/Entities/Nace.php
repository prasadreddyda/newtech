<?php

namespace Modules\Settings\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Nace extends Model
{
    use Translatable;

    protected $table = 'settings__naces';
    public $translatedAttributes = [];
    protected $fillable = [
        'name'
    ];
}
