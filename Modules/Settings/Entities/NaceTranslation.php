<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class NaceTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'settings__nace_translations';
}
