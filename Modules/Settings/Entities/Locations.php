<?php

namespace Modules\Settings\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    use Translatable;

    protected $table = 'settings__locations';
    public $translatedAttributes = [];
    protected $fillable = [
        'name'
    ];
}
