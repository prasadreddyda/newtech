<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class CountriesTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'settings__countries_translations';
}
