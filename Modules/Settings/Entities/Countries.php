<?php

namespace Modules\Settings\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    use Translatable;

    protected $table = 'settings__countries';
    public $translatedAttributes = [];
    protected $fillable = [
        'name'
    ];
}
