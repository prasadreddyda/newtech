<?php

namespace Modules\Settings\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Settings\Entities\Nace;
use Modules\Settings\Http\Requests\CreateNaceRequest;
use Modules\Settings\Http\Requests\UpdateNaceRequest;
use Modules\Settings\Repositories\NaceRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class NaceController extends AdminBaseController
{
    /**
     * @var NaceRepository
     */
    private $nace;

    public function __construct(NaceRepository $nace)
    {
        parent::__construct();

        $this->nace = $nace;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $naces = $this->nace->all();

        return view('settings::admin.naces.index', compact('naces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('settings::admin.naces.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateNaceRequest $request
     * @return Response
     */
    public function store(CreateNaceRequest $request)
    {
        $this->nace->create($request->all());

        return redirect()->route('admin.settings.nace.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('settings::naces.title.naces')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Nace $nace
     * @return Response
     */
    public function edit(Nace $nace)
    {
        return view('settings::admin.naces.edit', compact('nace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Nace $nace
     * @param  UpdateNaceRequest $request
     * @return Response
     */
    public function update(Nace $nace, UpdateNaceRequest $request)
    {
        $this->nace->update($nace, $request->all());

        return redirect()->route('admin.settings.nace.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('settings::naces.title.naces')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Nace $nace
     * @return Response
     */
    public function destroy(Nace $nace)
    {
        $this->nace->destroy($nace);

        return redirect()->route('admin.settings.nace.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('settings::naces.title.naces')]));
    }
}
