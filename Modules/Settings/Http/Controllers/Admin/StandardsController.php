<?php

namespace Modules\Settings\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Settings\Entities\Standards;
use Modules\Settings\Http\Requests\CreateStandardsRequest;
use Modules\Settings\Http\Requests\UpdateStandardsRequest;
use Modules\Settings\Repositories\StandardsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class StandardsController extends AdminBaseController
{
    /**
     * @var StandardsRepository
     */
    private $standards;

    public function __construct(StandardsRepository $standards)
    {
        parent::__construct();

        $this->standards = $standards;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $standards = $this->standards->all();

        return view('settings::admin.standards.index', compact('standards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('settings::admin.standards.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateStandardsRequest $request
     * @return Response
     */
    public function store(CreateStandardsRequest $request)
    {
        $this->standards->create($request->all());

        return redirect()->route('admin.settings.standards.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('settings::standards.title.standards')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Standards $standards
     * @return Response
     */
    public function edit(Standards $standards)
    {
        return view('settings::admin.standards.edit', compact('standards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Standards $standards
     * @param  UpdateStandardsRequest $request
     * @return Response
     */
    public function update(Standards $standards, UpdateStandardsRequest $request)
    {
        $this->standards->update($standards, $request->all());

        return redirect()->route('admin.settings.standards.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('settings::standards.title.standards')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Standards $standards
     * @return Response
     */
    public function destroy(Standards $standards)
    {
        $this->standards->destroy($standards);

        return redirect()->route('admin.settings.standards.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('settings::standards.title.standards')]));
    }
}
