<?php

namespace Modules\Settings\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Settings\Entities\Locations;
use Modules\Settings\Http\Requests\CreateLocationsRequest;
use Modules\Settings\Http\Requests\UpdateLocationsRequest;
use Modules\Settings\Repositories\LocationsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class LocationsController extends AdminBaseController
{
    /**
     * @var LocationsRepository
     */
    private $locations;

    public function __construct(LocationsRepository $locations)
    {
        parent::__construct();

        $this->locations = $locations;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $locations = $this->locations->all();

        return view('settings::admin.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('settings::admin.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateLocationsRequest $request
     * @return Response
     */
    public function store(CreateLocationsRequest $request)
    {
        $this->locations->create($request->all());

        return redirect()->route('admin.settings.locations.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('settings::locations.title.locations')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Locations $locations
     * @return Response
     */
    public function edit(Locations $locations)
    {
        return view('settings::admin.locations.edit', compact('locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Locations $locations
     * @param  UpdateLocationsRequest $request
     * @return Response
     */
    public function update(Locations $locations, UpdateLocationsRequest $request)
    {
        $this->locations->update($locations, $request->all());

        return redirect()->route('admin.settings.locations.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('settings::locations.title.locations')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Locations $locations
     * @return Response
     */
    public function destroy(Locations $locations)
    {
        $this->locations->destroy($locations);

        return redirect()->route('admin.settings.locations.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('settings::locations.title.locations')]));
    }
}
