<?php

namespace Modules\Settings\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Settings\Entities\ClientType;
use Modules\Settings\Http\Requests\CreateClientTypeRequest;
use Modules\Settings\Http\Requests\UpdateClientTypeRequest;
use Modules\Settings\Repositories\ClientTypeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ClientTypeController extends AdminBaseController
{
    /**
     * @var ClientTypeRepository
     */
    private $clienttype;

    public function __construct(ClientTypeRepository $clienttype)
    {
        parent::__construct();

        $this->clienttype = $clienttype;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $clienttypes = $this->clienttype->all();

        return view('settings::admin.clienttypes.index', compact('clienttypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('settings::admin.clienttypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateClientTypeRequest $request
     * @return Response
     */
    public function store(CreateClientTypeRequest $request)
    {
        $this->clienttype->create($request->all());

        return redirect()->route('admin.settings.clienttype.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('settings::clienttypes.title.clienttypes')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  ClientType $clienttype
     * @return Response
     */
    public function edit(ClientType $clienttype)
    {
        return view('settings::admin.clienttypes.edit', compact('clienttype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ClientType $clienttype
     * @param  UpdateClientTypeRequest $request
     * @return Response
     */
    public function update(ClientType $clienttype, UpdateClientTypeRequest $request)
    {
        $this->clienttype->update($clienttype, $request->all());

        return redirect()->route('admin.settings.clienttype.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('settings::clienttypes.title.clienttypes')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ClientType $clienttype
     * @return Response
     */
    public function destroy(ClientType $clienttype)
    {
        $this->clienttype->destroy($clienttype);

        return redirect()->route('admin.settings.clienttype.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('settings::clienttypes.title.clienttypes')]));
    }
}
