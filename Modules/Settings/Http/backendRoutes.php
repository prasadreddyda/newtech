<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/settings'], function (Router $router) {
    $router->bind('standards', function ($id) {
        return app('Modules\Settings\Repositories\StandardsRepository')->find($id);
    });
    $router->get('standards', [
        'as' => 'admin.settings.standards.index',
        'uses' => 'StandardsController@index',
        'middleware' => 'can:settings.standards.index'
    ]);
    $router->get('standards/create', [
        'as' => 'admin.settings.standards.create',
        'uses' => 'StandardsController@create',
        'middleware' => 'can:settings.standards.create'
    ]);
    $router->post('standards', [
        'as' => 'admin.settings.standards.store',
        'uses' => 'StandardsController@store',
        'middleware' => 'can:settings.standards.create'
    ]);
    $router->get('standards/{standards}/edit', [
        'as' => 'admin.settings.standards.edit',
        'uses' => 'StandardsController@edit',
        'middleware' => 'can:settings.standards.edit'
    ]);
    $router->put('standards/{standards}', [
        'as' => 'admin.settings.standards.update',
        'uses' => 'StandardsController@update',
        'middleware' => 'can:settings.standards.edit'
    ]);
    $router->delete('standards/{standards}', [
        'as' => 'admin.settings.standards.destroy',
        'uses' => 'StandardsController@destroy',
        'middleware' => 'can:settings.standards.destroy'
    ]);
    $router->bind('nace', function ($id) {
        return app('Modules\Settings\Repositories\NaceRepository')->find($id);
    });
    $router->get('naces', [
        'as' => 'admin.settings.nace.index',
        'uses' => 'NaceController@index',
        'middleware' => 'can:settings.naces.index'
    ]);
    $router->get('naces/create', [
        'as' => 'admin.settings.nace.create',
        'uses' => 'NaceController@create',
        'middleware' => 'can:settings.naces.create'
    ]);
    $router->post('naces', [
        'as' => 'admin.settings.nace.store',
        'uses' => 'NaceController@store',
        'middleware' => 'can:settings.naces.create'
    ]);
    $router->get('naces/{nace}/edit', [
        'as' => 'admin.settings.nace.edit',
        'uses' => 'NaceController@edit',
        'middleware' => 'can:settings.naces.edit'
    ]);
    $router->put('naces/{nace}', [
        'as' => 'admin.settings.nace.update',
        'uses' => 'NaceController@update',
        'middleware' => 'can:settings.naces.edit'
    ]);
    $router->delete('naces/{nace}', [
        'as' => 'admin.settings.nace.destroy',
        'uses' => 'NaceController@destroy',
        'middleware' => 'can:settings.naces.destroy'
    ]);
    $router->bind('clienttype', function ($id) {
        return app('Modules\Settings\Repositories\ClientTypeRepository')->find($id);
    });
    $router->get('clienttypes', [
        'as' => 'admin.settings.clienttype.index',
        'uses' => 'ClientTypeController@index',
        'middleware' => 'can:settings.clienttypes.index'
    ]);
    $router->get('clienttypes/create', [
        'as' => 'admin.settings.clienttype.create',
        'uses' => 'ClientTypeController@create',
        'middleware' => 'can:settings.clienttypes.create'
    ]);
    $router->post('clienttypes', [
        'as' => 'admin.settings.clienttype.store',
        'uses' => 'ClientTypeController@store',
        'middleware' => 'can:settings.clienttypes.create'
    ]);
    $router->get('clienttypes/{clienttype}/edit', [
        'as' => 'admin.settings.clienttype.edit',
        'uses' => 'ClientTypeController@edit',
        'middleware' => 'can:settings.clienttypes.edit'
    ]);
    $router->put('clienttypes/{clienttype}', [
        'as' => 'admin.settings.clienttype.update',
        'uses' => 'ClientTypeController@update',
        'middleware' => 'can:settings.clienttypes.edit'
    ]);
    $router->delete('clienttypes/{clienttype}', [
        'as' => 'admin.settings.clienttype.destroy',
        'uses' => 'ClientTypeController@destroy',
        'middleware' => 'can:settings.clienttypes.destroy'
    ]);
    $router->bind('countries', function ($id) {
        return app('Modules\Settings\Repositories\CountriesRepository')->find($id);
    });
    $router->get('countries', [
        'as' => 'admin.settings.countries.index',
        'uses' => 'CountriesController@index',
        'middleware' => 'can:settings.countries.index'
    ]);
    $router->get('countries/create', [
        'as' => 'admin.settings.countries.create',
        'uses' => 'CountriesController@create',
        'middleware' => 'can:settings.countries.create'
    ]);
    $router->post('countries', [
        'as' => 'admin.settings.countries.store',
        'uses' => 'CountriesController@store',
        'middleware' => 'can:settings.countries.create'
    ]);
    $router->get('countries/{countries}/edit', [
        'as' => 'admin.settings.countries.edit',
        'uses' => 'CountriesController@edit',
        'middleware' => 'can:settings.countries.edit'
    ]);
    $router->put('countries/{countries}', [
        'as' => 'admin.settings.countries.update',
        'uses' => 'CountriesController@update',
        'middleware' => 'can:settings.countries.edit'
    ]);
    $router->delete('countries/{countries}', [
        'as' => 'admin.settings.countries.destroy',
        'uses' => 'CountriesController@destroy',
        'middleware' => 'can:settings.countries.destroy'
    ]);
    $router->bind('locations', function ($id) {
        return app('Modules\Settings\Repositories\LocationsRepository')->find($id);
    });
    $router->get('locations', [
        'as' => 'admin.settings.locations.index',
        'uses' => 'LocationsController@index',
        'middleware' => 'can:settings.locations.index'
    ]);
    $router->get('locations/create', [
        'as' => 'admin.settings.locations.create',
        'uses' => 'LocationsController@create',
        'middleware' => 'can:settings.locations.create'
    ]);
    $router->post('locations', [
        'as' => 'admin.settings.locations.store',
        'uses' => 'LocationsController@store',
        'middleware' => 'can:settings.locations.create'
    ]);
    $router->get('locations/{locations}/edit', [
        'as' => 'admin.settings.locations.edit',
        'uses' => 'LocationsController@edit',
        'middleware' => 'can:settings.locations.edit'
    ]);
    $router->put('locations/{locations}', [
        'as' => 'admin.settings.locations.update',
        'uses' => 'LocationsController@update',
        'middleware' => 'can:settings.locations.edit'
    ]);
    $router->delete('locations/{locations}', [
        'as' => 'admin.settings.locations.destroy',
        'uses' => 'LocationsController@destroy',
        'middleware' => 'can:settings.locations.destroy'
    ]);
// append





});
