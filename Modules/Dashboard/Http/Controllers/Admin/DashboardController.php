<?php

namespace Modules\Dashboard\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Dashboard\Repositories\WidgetRepository;
use Modules\User\Contracts\Authentication;
use Nwidart\Modules\Contracts\RepositoryInterface;
use Modules\Clients\Repositories\ClientdataRepository;
use Modules\Settings\Repositories\StandardsRepository;
use Modules\Settings\Repositories\ClientTypeRepository;
use Modules\Settings\Repositories\CountriesRepository;
use Modules\Clients\Repositories\ClientofferRepository;
use Modules\Surveillance\Repositories\SurdataRepository;
use Modules\Scheme\Repositories\DataRepository;

class DashboardController extends AdminBaseController
{
    /**
     * @var WidgetRepository
     */
    private $widget;
    /**
     * @var Authentication
     */
    private $auth;

    /**
     * @param RepositoryInterface $modules
     * @param WidgetRepository $widget
     * @param Authentication $auth
     */
    public function __construct(RepositoryInterface $modules, WidgetRepository $widget, Authentication $auth,ClientdataRepository $clientdata,StandardsRepository $standards,ClientTypeRepository $clienttype,CountriesRepository $countries,ClientofferRepository $clientoffer,SurdataRepository $surdata, DataRepository $data)
    {
        parent::__construct();
        $this->widget = $widget;
        $this->auth = $auth;
        $this->clientdata = $clientdata;
        $this->standards = $standards;
        $this->clienttype = $clienttype;
        $this->countries = $countries;
        $this->surdata = $surdata;
        $this->clientoffer = $clientoffer;
        $this->data = $data;
        $this->auth = $auth;

        $this->middleware(function ($request, $next) use ($modules) {
            $this->bootWidgets($modules);

            return $next($request);
        });
    }

    /**
     * Display the dashboard with its widgets
     * @return \Illuminate\View\View
     */
    public function index()
    {

        $this->requireAssets();

        $widget = $this->widget->findForUser($this->auth->id());

        $customWidgets = json_encode(null);
        if ($widget) {
            $customWidgets = $widget->widgets;
        }
        $log = $this->auth->user();
        // print_r($log->roles[0]->slug);
        // die();
        if(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'admin')
        {
           $role = "Admin";
            $clientdatas = $this->clientdata->all()->count();
            $clientoffers = $this->clientoffer->all()->count();
            $surdatas = $this->data->all()->count();
            $datas = $this->data->all()->count();

            return view('dashboard::admin.dashboard', compact('customWidgets','clientdatas','role','clientoffers','surdatas','datas'));
        }

        elseif(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'marketing')
        {
            $role = "marketing";
           
            $clientdatas = $this->clientdata->getByAttributes(['user_id' => $log->id ])->count();
            $clientoffers = $this->clientoffer->getByAttributes(['user_id' => $log->id ])->count();
            $surdatas = $this->surdata->getByAttributes(['user_id' => $log->id ])->count();

            return view('dashboard::admin.dashboard', compact('customWidgets','clientdatas','role','clientoffers','surdatas'));
            
        }
        elseif(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'surveillance')
        {
            $role = "surveillance";
            $clientdatas = $this->clientdata->getByAttributes(['user_id' => $log->id ])->count();
            $clientoffers = $this->clientoffer->getByAttributes(['user_id' => $log->id ])->count();
            
            $surdatas = $this->data->all()->count();

            return view('dashboard::admin.dashboard', compact('customWidgets','clientdatas','role','clientoffers','surdatas'));

        } elseif(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'schema') {
            $role = "schema";

            $total_records = $this->data->getByAttributes(['user_id' => $log->id])->count();

            return view('dashboard::admin.dashboard', compact('customWidgets','total_records','role'));

        }elseif(isset($log->roles[0]->slug) && $log->roles[0]->slug == 'schemeadm') {
            $role = "schemeadm";

            // $total_records = $this->data->getByAttributes(['user_id' => $log->id])->count();
            $total_records = $this->data->all()->count();


            return view('dashboard::admin.dashboard', compact('customWidgets','total_records','role'));

        }
    }

    /**
     * Save the current state of the widgets
     * @param Request $request
     * @return mixed
     */
    public function save(Request $request)
    {
        $widgets = $request->get('grid');

        if (empty($widgets)) {
            return Response::json([false]);
        }

        $this->widget->updateOrCreateForUser($widgets, $this->auth->id());

        return Response::json([true]);
    }

    /**
     * Reset the grid for the current user
     */
    public function reset()
    {
        $widget = $this->widget->findForUser($this->auth->id());

        if (!$widget) {
            return redirect()->route('dashboard.index')->with('warning', trans('dashboard::dashboard.reset not needed'));
        }

        $this->widget->destroy($widget);

        return redirect()->route('dashboard.index')->with('success', trans('dashboard::dashboard.dashboard reset'));
    }

    /**
     * Boot widgets for all enabled modules
     * @param RepositoryInterface $modules
     */
    private function bootWidgets(RepositoryInterface $modules)
    {
        foreach ($modules->allEnabled() as $module) {
            if (! isset($module->widgets)) {
                continue;
            }
            foreach ($module->widgets as $widgetClass) {
                app($widgetClass)->boot();
            }
        }
    }

    /**
     * Require necessary assets
     */
    private function requireAssets()
    {
        $this->assetPipeline->requireJs('lodash.js');
        $this->assetPipeline->requireJs('jquery-ui-core.js');
        $this->assetPipeline->requireJs('jquery-ui-widget.js');
        $this->assetPipeline->requireJs('jquery-ui-mouse.js');
        $this->assetPipeline->requireJs('jquery-ui-draggable.js');
        $this->assetPipeline->requireJs('jquery-ui-resizable.js');
        $this->assetPipeline->requireJs('gridstack.js');
        $this->assetPipeline->requireJs('chart.js');
        $this->assetPipeline->requireCss('gridstack.css')->before('asgard.css');
    }
}
