@extends('layouts.master')

@section('content-header')
@if(isset($role) && $role == "Admin")  
    <h1 style="color:green">
        {{ trans(' Admin Dashboard') }}
    </h1>
@elseif(isset($role) && $role == "marketing")
<h1 style="color:green">
        {{ trans(' Marketing Dashboard') }}
</h1>
@elseif(isset($role) && $role == "surveillance")
<h1 style="color:green">
        {{ trans(' Surveillance Dashboard') }}
</h1>
@else 
  <h1 style="color:green" >Scheme Dashboard</h1>
@endif

@stop

@section('styles')

@stop

@section('content')

<?php if(isset($role) && $role == "Admin") { ?>


<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $clientdatas }}</h3>

              <p>Client Data Created</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-tie"></i>
            </div>
            <a href="{{ route('admin.clients.clientdata.index')}}" class="small-box-footer">View Details <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $clientoffers }}</h3>

              <p> Offers Created</p>
            </div>
            <div class="icon">
              <i class="fa fa-file"></i>
            </div>
            <a href="{{ route('admin.clients.clientoffer.index')}}" class="small-box-footer">View Details  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $datas }} </h3>

              <p>Scheme Data</p>
            </div>
            <div class="icon">
              
              <i class="fa fa-eye"></i>
            </div>
            
            <a href="{{ route('admin.scheme.data.index')}}" class="small-box-footer">View Details  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $surdatas }} </h3>

              <p>Surveillance Data</p>
            </div>
            <div class="icon">
              
              <i class="fa fa-eye"></i>
            </div>
            
            <a href="{{ route('admin.surveillance.surdata.index')}}" class="small-box-footer">View Details  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
       
        <!-- ./col -->
      </div>
<?php } elseif (isset($role) && $role == "marketing") 



{ ?>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $clientdatas }}</h3>

              <p>Client Data</p>
            </div>
            <div class="icon">
              <i class="fa fa-home"></i>
            </div>
            <a href="{{ route('admin.clients.clientdata.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $clientoffers }}</h3>

              <p>Client Offers</p>
            </div>
            <div class="icon">
              <i class="fa fa-question"></i>
            </div>
            <a href="{{ route('admin.clients.clientoffer.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        
    <?php
}



elseif (isset($role) && $role == "surveillance") 
{ ?>


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $surdatas }} </h3>

              <p>Surveillance </p>
            </div>
            <div class="icon">
              <i class="fa fa-pie-chart"></i>
            </div>
            
            <a href="{{ route('admin.surveillance.surdata.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>{{ $clientdatas }}</h3>

              <p>Client Data</p>
            </div>
            <div class="icon">
              <i class="fa fa-home"></i>
            </div>
            <a href="{{ route('admin.clients.clientdata.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $clientoffers }}</h3>

              <p>Client Offers</p>
            </div>
            <div class="icon">
              <i class="fa fa-question"></i>
            </div>
            <a href="{{ route('admin.clients.clientoffer.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        
        
  <?php
} elseif (isset($role) && $role == "schema")  { ?>

   <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $total_records }}</h3>

              <p>Total Schemda Data</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-tie"></i>
            </div>
            <a href="{{ route('admin.scheme.data.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

<?php } elseif (isset($role) && $role == "schemeadm")  { ?>

<div class="col-lg-3 col-xs-6">
       <!-- small box -->
       <div class="small-box bg-green">
         <div class="inner">
           <h3>{{ $total_records }}</h3>

           <p>Total Scheme Data</p>
         </div>
         <div class="icon">
           <i class="fa fa-user-tie"></i>
         </div>
         <a href="{{ route('admin.scheme.data.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
     </div>

<?php } ?>




@stop
