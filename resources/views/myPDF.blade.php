<!DOCTYPE html>
<html>
	
	<style>
html,body{
    width: 100%;
    height: 100%;
  
    border: solid #3c4352;
    border-width: thin;
    overflow:hidden;
    display:block;
	border: 0.5em;
}
table,
    th,
    td {
      border: 1px solid black;
      border-collapse: collapse;
      text-align: center;
      width=100%;
       border="0";
        cellspacing="0";
        cellpadding="0";
    }
.column {
  float: left;
  width: 50%;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}
.border-full {
    border-right: 2px solid #3c4352;
	border-top: 2px solid #3c4352;
	border-left: 2px solid #3c4352;
	border-bottom: 2px solid #3c4352;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

.sign{
    border: solid #3c4352;
    
	border: 0.1em;
}


.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}

.left {
  left: 0;
  background-color: #111;
}

.right {
  right: 0;
  background-color: red;
}

.centered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}

.centered img {
  width: 150px;
  border-radius: 50%;
}
.li{
	color:#2f4f82;
	font-weight:bold;
}
.first {
        margin-left: 3%;
 }

 .firstnew{
	font-family:"Times New Roman", Times, serif;
	font-weight:bold;
 }

 .firstlineheight{
	line-height:2;
 }

.lii{
	color:#415680;
	font-weight:bold;
	border: solid #8a9dc2;
    border-width: thin;
    overflow:hidden;
    display:block;
	border: 0.2em;
	background:#c8d2e6;
	font-size:17px;
	margin-left: 3%;
	margin-top: 10%;
}


.h4{
	text-align:center;
	color:#2f4f82;
	font-weight:bold;
	font-size:25px;
}

.h3{
	text-align:center;
	color:#2f4f82;
	font-weight:bold;
	font-size:18px;
}

.company{
	text-align: center;
	color:#2f4f82;
	font-weight:bold;
	font-size:18px;
}

.pre{
	text-align: center;
	color:#2f4f82;
	font-weight:bold;
	font-family:"Times New Roman", Times, serif;
	font-size:14px;
}
</style>
<head>
    <title>Hi</title>
</head>
<body>
					<h4 class="h4">ISO CERTIFICATION AGREEMENT</h4>
       
	            	<h3 class="h3">FOR</h3>

					<h4 class="company"	>Logitech group of companies</h4>
					<pre class="pre">
						INTEGRATED MANAGEMENT SYSTEM 						
						Bangalore
					</pre>
					<!-- <h5 style="text-align: center;color:#2f4f82;font-weight:bold;"> </h5>
					<h5 style="text-align: center;color:#2f4f82;font-weight:bold;"></h5>
					<h5 style="text-align: center;color:#2f4f82;font-weight:bold;"></h5> -->

					<img src="../resources/frontpage.png" alt="Newtech-LOGO" width="750" height="600">
					<h1 style="text-align: center;color:#2f4f82;font-weight:bold;font-size:20px;">AL TAIR QUALITY SERVICES LLC</h1>
					<h3 style="text-align: center;color:#2f4f82;font-weight:bold;font-size:16px;">P.O.Box: 108967, Abu Dhabi, UAE. -Tel: 02- 6713980 - Fax - 02-6713982</h3>
<br/>
		<!-- ********************PAGE 2**************************** -->
		
		<br/>
		<br/>	
		<br/>
		<br/>	

		<br/>
		<br/>	
	<h2 style="text-align:center;color:#2f4f82;">CONTEXT OF AGREEMENT</h2>
		<br/>
			<ol class="li">
				<li >INTRODUCTION LETTER</li>
				<br/>
				<li>CERTIFICATION PROCESS/ GUIDELINES</li>
				<br/>
				<li>ANNEXURE 1- APPLICATION FORM/AGREEMENT SHEET</li>
				<br/>
				<li>ANNEXURE 2- DURATION</li>
				<br/>
				<li>ANNEXURE 3- CONFIDENTIALITY</li>
				<br/>
				<li>ANNEXURE 4- NON-DISCLOSURE</li>
				<br/>
				<li>ANNEXURE 5- TERMINATION</li>
				<br/>
				<li>ANNEXURE 6- VALIDITY</li>
				<br/>
				<li>ANNEXURE 7- PAYMENT TERMS</li>
				<br/>
				<li>ANNEXURE 7- PAYMENT TERMS</li>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>


			</ol>
<br/><br/>
		
			<!-- *****************************************PAGE 3 ***************************************************** -->

			<ol class="lii">
				<li>INTRODUCTION LETTER </li>
				
		    </ol>
				<h3 class="first">To,</h3>
				<h3 class="first">M/S: SWIFT TECH ELECTROMECHANICAL CONTRACTING SOLE PROPRIETORSHIP L.L.C, ABU DHABI, UNITED ARAB EMIRATES</h3>
				<h3 class="first">ATTN: MR. VINU.U.V</h3>
				<p class="first">Sub: Certification AGREEMENT (CA) for <strong> 9001:2015, ISO 14001:2015 & ISO 45001:2018</strong></p>

				<p class="first">Thank you for your interest in AL TAIR QUALITY SERVICES LLC, we have the pleasure to provide our services to your esteemed company</p>
				<p class="first">We take this opportunity to wish you success and it will be our privilege to work with you on the proposed agreement. We are confident of delivering our services up to your expectations and look forward to a continuing the business relationship.</p>

				<h4 class="first">Enclosed, please find the following</h4>
				<ul class="first">
					<li >
					ISO AGREEMENT for certification services
					</li>
					<li>The Standard Terms & Conditions.</li>
				</ul>

				<p class="first">The standards certificates, which we offer are ISO 9001:2015, ISO 14001:2015, OHSAS 18001:2007, ISO 45001:2018, ISO 22000, HACCP, ISO 27001, ISO 50001 and others</p>
				<p class="first">Based on the requirements Expressed <strong>TECH ELECTROMECHANICAL CONTRACTING SOLE PROPRIETORSHIP L.L.C</strong></p>

				<p class="first">We would like to formally submit to you our agreement<strong>9001:2015, ISO 14001:2015 & ISO 45001:2018</strong></p>

				<p class="first"><strong>for the ISO certification requirement of your company</strong></p>

				<h4 class="first" style="background:#51fc70;">To confirm this AGREEMENT, along with the stamp and sign</h4>

				<p class="first">Thanking you and assuring you of our best services / attention at all times</p>

				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>

		<!-- ************************************************************** PAGE 4 *********************************************************** -->

			<ol class="lii">
			
				
				<li value="2">CERTIFICATION PROCESS/ GUIDELINES</li>
				
		    </ol>
			<h4 class="first">Accredited Management System Certification Rules</h4>
			<h4 class="first">Purpose</h4>
			<p class="first firstlineheight">The purpose of this description of the AL TAIR QUALITY SERVICES LLC Certification Rules
				is to provide relevant information regarding AL TAIR QUALITY SERVICES LLC services for
				conducting our impartial and competent assessment of a company’s management system
				for issue and maintenance of an accredited certification Standards.</p>

			<h4 class="first">Purpose</h4>
			<p class="first">It covers the following scope</p>
			<ul class="first">
					<li >Preliminary meeting to establish scope of registration and the applicable standard.</li>
					<br/>
					<li>Conduct of stage 1 & stage 2 audits for certification.</li>
					<br/>
					<li>Issue of accredited certifications as per accredited scope sectors.</li>
					<br/>
					<li>Surveillance visits for verification of conformance of quality systems to certification standard.</li>
					<br/>
					<li>The organization need to develop a system in respect to ISO standards as per the applicable management system for which they need the certification.</li>
			</ul>
		
		
			<div style="width: 100%;" >
   				<div class="first" style="float:left; width:60%;">
				   <h4 class="first">Certification Procedure</h4>
				   <p class="first "><strong>Enquiry and Fee Quotation:</strong>Upon receipt of an enquiry,
					The AL TAIR QUALITY SERVICES LLC Questionnaire is
					Required to be completed by the applicant company.
					Based upon the information provided, a detailed offer
					Is submitted for client’s consideration also check M/D’s
					Acceptance.</p>

					<p class="first "><strong>Application:</strong>Upon confirmation of acceptance of
					AL TAIR QUALITY SERVICES LLC fee offer and the
					Receipt of client’s application together with the
					Application fee, the process of certification commences
					With scheduling of audits on mutually agreeable dates.</p>

					<p class="first"><strong>Extension of Certification:</strong>Whenever the clients apply For the extension of Scope/ addition of sites, facilities etc.
					The same is verified during a special visit or at the
					Next surveillance audit and based upon the recommendations
					Of Lead Assessor and verification of audit reports a decision is
					Taken for issue of amended certification for scope extension</p>

   				</div>
				   <br/><br/><br/>
   				<div style="float:right;background:red;">
				   <img src="../resources/qrsimage.png" alt="QRS-LOGO" width="300" height="300">
   				</div>
			</div>
			<div style="clear:both"></div>
			<br/><br/><br/>
	<!-- ************************************************** PAGE 5 ********************************************************** -->
			<h4 class="first">Audit Process</h4>			
				<p class="first firstlineheight"><strong>Documentation Review and Stage 1 Audit:</strong>The purpose of this description of the AL TAIR QUALITY SERVICES LLC Certification Rules
				is to provide relevant information regarding AL TAIR QUALITY SERVICES LLC services for
				conducting our impartial and competent assessment of a company’s management system
				for issue and maintenance of an accredited certification Standards.</p>

				<p class="first firstlineheight"><strong>Stage 2 Audit: </strong>Following the Stage-I audit, AL TAIR QUALITY SERVICES LLC will conduct a
				Stage-II certification audit to assess conformity with the requirements of the applicable ISO
				standard.</p>

				<p class="first firstlineheight"><strong>Corrective Actions and Follow-up: </strong>The Company is required to submit a Corrective Action
				Plan addressing the non-conformities within a given time frame. </p>

				<h4 class="first">Issue of Certificate</h4>	
				<p class="first firstlineheight">Upon completion of the review of all audit documentation and corrective actions being
				closed out, AL TAIR QUALITY SERVICES LLC will proceed the Certificate.</p>


				<p class="first firstlineheight"><strong>VALIDITY AND RENEWAL OF CERTIFICATE: </strong>Certificates issued by AL TAIR QUALITY
				SERVICES LLC remain valid for three years subject to found satisfactory during periodical
				surveillance audits. </p>

				<h4 class="first">Surveillance Audit</h4>	
				<p class="first firstlineheight" >Surveillance audits shall be conducted at periodic interact at least once a year during the
				three-year term of validity of the Certificate followed by a re-assessment of the quality
				systems for renewal of the certification prior to its expiry. The frequency of the
				surveillance audits has to be at least once in 12 months from the date of closing meeting of
				the certification audit i.e., two surveillance audits to be conducted during the three-year
				period of validity at annual interact.</p>

				<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

	<!-- ******************************************************* PAGE 6 *********************************************************** -->
	<ol class="lii">
		<li value="3">APPLICATION FORM/AGREEMENT SHEET</li>
	   </ol>
	<table>
            <tr>
            <th rowspan="2">CONTROL NO</th>
              <th rowspan="2">SEC/IMS/100</th>
              <td>TQS - ASSISTANCE</td>
              <td colspan="2">PRASAD REDDY</td>
            </tr>
            <tr>
              <td>POSITION</td>
              <td colspan="2">SURVEILLNACE/RE-CERTIFICATION DEPARTMENT</td>
            </tr>
            <tr>
              <th>COMPANY</th>
              <td colspan="4">SWIFT TECH ELECTROMECHANICAL CONTRACTING SOLE PROPRIETORSHIP L.L.C</td>
            </tr>
            <tr>
                <th>ADDRESS</th>
                <td>Bangalore, India</td>
                <th>CONTACT NUMBER</th>
                <td colspan="2">06654540-96254-3022</td>
            </tr>
            <tr>
                <th>CONTACT PERSON</th>
                <td>MR. VINU.U.V</td>
                <th>DESIGNATION</th>
                <td colspan="2">Project Engineer</td>
            </tr>
            <tr>
                <th>NO. OF EMPLOYEES</th>
                <td>0-100 (Estimated)</td>
                <th>TYPE OF APPLICATION</th>
                <th colspan="2">Initial, Re- assessment, Surveillance/
                    Transfer Assessment</th>
            </tr>
            <tr>
                <th rowspan="3">STANDARD’S</th>
                <td rowspan="3">ISO 9001:2015 Quality Management System,<br> 
                    ISO 14001:2015 Environmental Management System,<br>
                    ISO 45001:2018 Occupational Health & Safety Management System</td>  
                    <th rowspan="3">Number of Sites</th>
                    <th>Head Office</th>
                    <td>Abu Dhabi</td>
                    <tr>
                        <th>Sites</th>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>Branch Office</td>
                        <td>Dubai</td>
                    </tr>
                    <tr>
                        <th>ACCREDITATION</th>
                        <td>IAS</td>
                        <td>ASCB-EUROPE</td>
                        <td>JAS-ANZ</td>
                        <td>EIAC</td>
                    </tr>
                    <tr>
                        <th colspan="3">CERTIFICATION STEP</th>
                        <th colspan="2">COST IN AED</th>
                    </tr>
                    <tr>
                        <td colspan="3">Stage 1 : Advance for ISO Renewal Registration :</td>
                        <td colspan="2">1,500</td>
                    </tr>
                    <tr>
                        <td colspan="3">Stage3: Issuance of Certificate & Logo</td>
                        <td colspan="2">2,000</td>
                    </tr>
                    <tr>
                        <td colspan="3">Total Contract Price : (Exclusive of 5% VAT)</td>
                        <td colspan="2">3,500/</td>
                    </tr>
                       <tr>
                        <td colspan="5">Note: Surveillance Audit --- Regardless of price it is mandatory requirement with minimum once a year surveillance audit must be performed by the certification body.</td>
                       </tr>
                       <tr>
                           <td colspan="3">Surveillance Audit – 2nd Year (Exclusive of 5% VAT)</td>
                           <td colspan="2">Free of Cost</td>
                       </tr>
                       <tr>
                           <td colspan="3">Surveillance Audit – 3rd Year (Exclusive of 5% VAT)</td>
                           <td colspan="2">Free of Cost</td>
                       </tr>
            </tr>
            
           
           

    </table>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<!-- ************************************************** PAGE 7 ********************************************************** -->
		<ol class="lii">
		<li value="4">ANNEXURE 2-DURATION</li>
	   </ol>
	   <p class="first firstlineheight">This contract is valid for a period of one month from the date of submission this contract or
		earlier up on satisfactory completion of services set out to deliver.</p>

		<ol class="lii">
		<li value="5">ANNEXURE 3- CONFIDENTIALITY</li>
	   </ol>
	   <p class="first firstlineheight">The terms and conditions of this Agreement are absolutely confidential between the parties
		and shall not be disclosed to anyone else, except as shall be necessary to effectuate its terms.
		Any disclosure in violation of this section shall be deemed a material breach of this
		Agreement and information security and data protection security of TQS & ISO certification
		body.</p>

		<ol class="lii">
		<li value="6">ANNEXURE 4- NON-DISCLOSURE</li>
	   </ol>
	   <p class="first firstlineheight">All information /data exposed to M/s AL TAIR QUALITY SERVICES LLC during the handling
		of assignment and later shall be kept confidential. Any misusage of logo, certificate need to
		be informed. </p>

		<ol class="lii">
		<li value="7">ANNEXURE 5- TERMINATION</li>
	   </ol>
	   <p class="first firstlineheight">This Valid Agreement can be terminated by any of the parties involved by a one-week
		advance written notice to affect the same. On termination must handover all the documents
		/working notes to the client. Any changes in Agreement or terms and condition of relevant
		documents will be terminated  </p>

		<ol class="lii">
		<li value="8">ANNEXURE 6-VALIDITY OF THIS CONTRACT</li>
	   </ol>
	   <p class="first firstlineheight">This Agreement is valid up to 1 month  </p>

	   <br/><br/><br/>

	<!-- ************************************************** PAGE 8 ********************************************************** -->
	
	   <ol class="lii">
		<li value="9">ANNEXURE 7-PAYMENT TERMS</li>
	   </ol>
	   <p class="first firstlineheight">Payment mode can be CASH / BANK TRANSFER /CHEQUE “cheque in favor of “Al TAIR
		QUALITY SERVICES LLC”.  </p>

	   <p class="first firstlineheight"><strong>Bank Details with Swift Code:</strong></p>
	   <pre class="first firstnew firstlineheight" style="text-align:left;" >
	   <b>Account Title : <strong style="background:yellow;">Newtech</strong></b>
	   <b>Bank Name 	: HDFC</b>
	   <b>Address       : P.O. Box 2681, Bangalore</b>
	   <b>IBAN          : AE4577 0290 59082553 1050 0596679</b>
	   <b>Account No.   : 0254-03345-05-45120311-105-0596679</b>
	   <b>SWIFT ID      : 44KH4BZUAEA45DXXX</b>
	   </pre>

	   <p class="first">Note:</p>
	   <ul class="first firstlineheight">
					<li >Payment should not be carried to any personal account if any such attempt mmade kindly update
						"Newtech Group Admin/Account Department "
					</li>
					<br/>
					<li >If any changes like Bank Change, Company Name Change or any legal changes both parties should inform each other
						for smooth processing.
					</li>
	  </ul>
	  <br/><br/><br/> <br/><br/><br/> <br/><br/><br/> <br/><br/><br/> <br/><br/><br/>

	<!-- ************************************************** PAGE 9 ********************************************************** -->
	<ol class="lii">
		<li value="10">ANNEXURE 8-CERTIFICATION STANDARD TERMS AND CONDITIONS</li>
	   </ol>
	   <ul class="first" >

	  			    <li value="1" style="font-weight:bold;">General Information</li>
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" >TQS audits and certifies management systems, employees
					of TQS will not involve in consulting activities.</li>
				

					<li value="2"  style="font-weight:bold;">Executing Orders</li>
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" >Orders accepted by TQS shall be ececuted-in so far as contrary agreements
					have not been made in writing the customary handling practiced by TQS</li>
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" ><strong>Extensions of Audit:</strong>Audits may need to be extended
					due to the nature of findings of Auditors.Such situations are not covered by the Agreement and may result in extra charges
					.The Lead Auditor will discuss these situations with the client before any action is taken.</li>
					

					<li value="3"  style="font-weight:bold;">Deadlines,Delay,Impossibility of Performance</li>
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" >The Order Periods Indicated by TQS shallnot be binding 
					unless they are expressly agreed upon in writing</li>				
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" ><strong>Scheduling and Re-Scheduling of Dates:</strong>
					Dates for audits must be scheduled atleast one (1) week prior to the audit.If the client re-schedules the audit without the 
					requisite advance notice then the client maybe liable to compensate AL TAIR QUALITY SERVICES LLC for any lost service charges.</li>
					
					<li value="1" style="font-weight:bold;">Complaints and Appeals</li>
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" ><strong>Complaints:</strong>Any complaints can be addressed
					to  "The Complaint Manager, Bangalore Developers. OFFICE NO:3232, MG Road Street Bangalorer, INDIA or through mail on prasadreddy.da@gmail.com</li>
				


					<li value="3"  style="font-weight:bold;">Terms Of Payment and Prices</li>
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" ><strong>Validity:</strong>Agreement is valid for 60 days continuing validity of a Agreement,
					execution of the work must begin with two months of order acceptance unless a specific waiver is granted by TQS </li>				
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" >Appropriate advances on costs may be required and/or partial
					invoices may be submitted in accordance with services already furnished.</li>
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" ><strong>Travel & Sojourn:</strong>Agreement includes travel,boarding and lodging for any onshore locations.Offshore locations 
					organization has to arrange the pass,trainings,adhering to the regulations and commercial implications towards offshore visit.
					The travel arrangement to sites and another audit location to be organized by by the client during the audit. </li>
					<li style="color:#6184ab;margin-left:5%;font-size:15px;" ><strong>Payment Terms:</strong>Invoices will be raised upon completion of each stage in the Agreement.
					Invoices must be settled within 7 days of receipt to assure uninterrupted service.All payments must be made by cheque/through wire Transfer to our
					account as stated in the invoice.  </li>

					<br/><br/><br/>

				

				

		</ul>
		<div class="row" >
						<div class="column border-full" style="">
							<h2 class="border-full">Al Tair Quality Services LLC</h2>
							<br/><br/><br/><br/><br/><br/><br/><br/><br/>
							
							<p class="border-full">Senior Marketing Manager</p>
						</div>
						<div class="column border-full" style="">
						<h2 class="border-full">Al Tair Quality Services LLC</h2>
							<br/><br/><br/><br/><br/><br/><br/><br/><br/>
							
							<p class="border-full">Company Representative Signature and Stamp</p>
						</div>
					</div>
	   
</body>
</html>